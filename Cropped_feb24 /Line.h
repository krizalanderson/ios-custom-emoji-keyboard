//
//  Line.h
//  bezierpathDemo
//
//  Created by Hupp Technologies on 12/04/17.
//  Copyright © 2017 Hupp Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Line : UIView

@property (nonatomic, assign) CGPoint pointA;
@property (nonatomic, assign) CGPoint pointB;

@end
