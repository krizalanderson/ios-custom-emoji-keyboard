//
//  CameraViewController.h
//  CameraWithAVFoundation
//
//  Created by Gabriel Alvarado on 4/16/14.
//  Copyright (c) 2014 Gabriel Alvarado. All rights reserved.
//
#import <UIKit/UIKit.h>

///Protocol Definition
@protocol CACameraCropDelegate <NSObject>

@optional - (void)didCroppedImage:(UIImage *)image;

@end

@interface CameraViewController : UIViewController

@property (nonatomic, weak) id <CACameraCropDelegate> delegate;

-(void)SetupCamera;
@end
