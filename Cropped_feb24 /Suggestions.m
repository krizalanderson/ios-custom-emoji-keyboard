//
//  Suggestions.m
//  Cropped
//
//  Created by webastral on 03/10/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#import "Suggestions.h"

#import "MZAppDelegate.h"
#import "Organise.h"
#import "Help.h"
#import "HomeScreen.h"
#import "Animator.h"
#import <MessageUI/MessageUI.h>
#import <MBProgressHUD.h>

@interface Suggestions ()<UINavigationControllerDelegate,UITextViewDelegate,MFMailComposeViewControllerDelegate,UITextFieldDelegate>
{
    BOOL isComment;
    NSString *comment;
    NSString *email;
    NSMutableString *code;
    NSMutableArray *alphabets;
}
@end

@implementation Suggestions
NSString *const kUser_Mail = @"user_email";

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    // configure send button and top view
    self.sendBtn.layer.borderWidth = 1.0f;
    self.sendBtn.layer.borderColor = [UIColor colorWithRed:0.71 green:0.72 blue:0.73 alpha:1.0].CGColor;
    self.sendBtn.clipsToBounds = YES;
    
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, self.topView.frame.size.height - 1.0f, self.topView.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithRed:0.71 green:0.72 blue:0.73 alpha:1.0].CGColor;
    
    
    [self.topView.layer addSublayer:bottomBorder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    _txtField_Captcha.frame = CGRectMake(_txtField_Captcha.frame.origin.x, _txtField_Captcha.frame.origin.y, _txtField_Captcha.frame.size.width, _txtField_Captcha.frame.size.height+50);
//    _txtField_Captcha.translatesAutoresizingMaskIntoConstraints = YES;
    self.navigationController.delegate = self;
    comment = @"";
    email = @"";
    isComment = NO;
    
    _txtView_Comments.text = comment;
    _txtView_Comments.textColor = [UIColor lightGrayColor];
    _txtView_Comments.delegate = self;
    _txtView_Comments.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _txtView_Comments.layer.borderWidth = 1.0;
    
    //_txtField_email.textColor = [UIColor lightGrayColor];
    _txtField_email.delegate = self;
    _txtField_email.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _txtField_email.layer.borderWidth = 1.0;
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kUser_Mail]){
        email = [[NSUserDefaults standardUserDefaults] valueForKey:kUser_Mail];
        _txtField_email.text = email;
    }else{
        _txtField_email.text = email;
    }
    
    alphabets = [[NSMutableArray alloc] initWithArray:[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    //Remove the last object (extra), '#' from the array.
    [alphabets removeLastObject];
    NSArray *lowercaseArray = [alphabets valueForKey:@"lowercaseString"];
    [alphabets addObjectsFromArray:lowercaseArray];  //52 elements
    self.bottomView.userInteractionEnabled = YES;
    [self changeCaptcha];
    //[self topViewButtonsAppearance];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        for (UIView *tmp in self.view.subviews){
            if ([tmp isKindOfClass:[MBProgressHUD class]])
                [tmp removeFromSuperview];
        }
    });
    
}

#pragma mark Top View Buttons Setting
-(void) topViewButtonsAppearance
{
    int width = self.view.frame.size.width;
    NSLog(@"%f %f",_btn_Organise.frame.size.width,_btn_Organise.frame.size.height);
    int totalSpace = width-240;
    int spaceBetweenButton = totalSpace/5;
    _btn_Organise.frame = CGRectMake(spaceBetweenButton, _btn_Organise.frame.origin.y, _btn_Organise.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Create.frame = CGRectMake(spaceBetweenButton+_btn_Organise.frame.origin.x+_btn_Organise.frame.size.width, _btn_Organise.frame.origin.y, _btn_Create.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Suggestion.frame = CGRectMake(spaceBetweenButton+_btn_Create.frame.origin.x+_btn_Create.frame.size.width, _btn_Organise.frame.origin.y, _btn_Suggestion.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Help.frame = CGRectMake(spaceBetweenButton+_btn_Suggestion.frame.origin.x+_btn_Suggestion.frame.size.width, _btn_Organise.frame.origin.y, _btn_Help.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Organise.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Create.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Suggestion.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Help.translatesAutoresizingMaskIntoConstraints = YES;
    
    [_btn_Suggestion.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [_btn_Help.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_btn_Organise.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_btn_Create.titleLabel setFont:[UIFont systemFontOfSize:15]];

}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Goto View Controllers

- (IBAction)Goto_Suggestion:(UIButton *)sender {
    
    // [MZAppDelegate GotoSuggestion];
//    Suggestions *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Suggestions"];
//    [self gotoSpecificController:object];
    
}

- (IBAction)goto_Help:(UIButton *)sender {

        Help *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    [self gotoSpecificController:object];
}



- (IBAction)goto_Create:(UIButton *)sender {
     HomeScreen *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeScreen"];
    [self gotoSpecificController:object];
  
//
//    [object RetunToSpecificViewController:object];
//    [self.navigationController pushViewController:object animated:NO];
}

- (IBAction)goto_Organise:(UIButton *)sender {
//     [self showAlert:@"View is in under progress and will be available soon"];
    Organise *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Organise"];
     [self gotoSpecificController:object];
}


#pragma mark Goto Specific Controller
-(void) gotoSpecificController:(UIViewController *)controller
{
    BOOL isExist = NO;
    for (UIViewController *object in self.navigationController.viewControllers)
    {
        if ([object isKindOfClass:[controller class]])
        {
            NSLog(@"POP");
            //Do not forget to import AnOldViewController.h
            isExist = YES;
            [self.navigationController popToViewController:object  animated:NO];
            
            break;
        }
        
    }
    if (!isExist) {
        NSLog(@"PUSH");
        [self.navigationController pushViewController:controller animated:NO];
    }
}

#pragma mark Navigation Controller Delegate Methods
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController*)fromVC
                                                 toViewController:(UIViewController*)toVC
{
    if (operation == UINavigationControllerOperationPush)
        return [[PushAnimator alloc] init];
    
    if (operation == UINavigationControllerOperationPop)
        return [[PopAnimator alloc] init];
    
    return nil;
}

#pragma mark Text Field Delegate Methods
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
//    if ([textField.text isEqualToString:email]) {
//        textField.text = @"";
//    }
    textField.textColor = [UIColor blackColor];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if ([textField.text isEqualToString:@""]) {
        textField.text = comment;
        textField.textColor = [UIColor lightGrayColor];
    }
}

#pragma mark Text View Delegate Methods
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:comment]) {
        textView.text = @"";
    }
    textView.textColor = [UIColor blackColor];
    return YES;
}


//- (BOOL)textViewShouldEndEditing:(UITextView *)textView
//{
//
//
//    return YES;
//}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = comment;
        textView.textColor = [UIColor lightGrayColor];
    }
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(textView.text.length == 0){
        textView.textColor = [UIColor lightGrayColor];
        textView.text = comment;
        [textView resignFirstResponder];
        isComment = NO;
    }
    else
    {
        isComment = YES;
    }
}

#pragma mark Random Number Generator
-(NSInteger)randomIntBetween:(NSInteger)min and:(NSInteger)max
{
    return (NSInteger)(min + arc4random_uniform(max + 1 - min));
}

-(void) changeCaptcha
{
    
    code = [[NSMutableString alloc]init];
    for (int i=0; i<6; i++) {
        NSInteger random = [self randomIntBetween:0 and:alphabets.count-1];
        [code appendString:[alphabets objectAtIndex:random]];
    }
    
    _lbl_Captcha.text = code;
    _txtField_Captcha.text = @"";

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Submit:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    
    if ([_txtView_Comments.text isEqualToString:comment])
    {
        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please Insert some Suggestions to post" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
    }
//    else if (_txtField_Captcha.text.length == 0) {
//        [[[UIAlertView alloc]initWithTitle:@"Caution !!" message:@"Please enter security text first" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
//    }
//   else if (![_lbl_Captcha.text isEqualToString: _txtField_Captcha.text] ) {
//        NSLog(@"Error");
//        [self changeCaptcha];
//       _txtField_Captcha.text = @"";
//       [[[UIAlertView alloc]initWithTitle:@"Wrong !!" message:@"Please enter correct security text" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
//    }
    else
    {
        NSLog(@"Proceed");
        [self showAlert:@"Suggestion has been sent"];
//        _txtField_Captcha.text = @"";
        _txtView_Comments.text = @"";
        //[self reload_Captcha:nil];
    }
}

- (IBAction)reload_Captcha:(UIButton *)sender
{
    [self changeCaptcha];
}

#pragma mark Alert View
-(void) showAlert:(NSString *)message
{
    [[[UIAlertView alloc]initWithTitle:@"Message" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
    //[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hideAlertView) userInfo:nil repeats:NO];
}

- (void)didPresentAlertView:(UIAlertView *)alertView
{
    if ([alertView.message containsString:@"Enter"] || [alertView.message containsString:@"select"] || [alertView.message containsString:@"progress"]) {
        [self performSelector:@selector(hideAlertView:) withObject:alertView afterDelay:2];
    }
    
    
}
-(void)hideAlertView:(UIAlertView *)alertView
{
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
    
}
- (IBAction)sendBtn:(id)sender {
    
    
    if ([_txtField_email.text isEqualToString:@""])
    {
        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please Insert email to post." delegate:nil
                         cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
        return;
    }
    
    if ([self validateEmailWithString:_txtField_email.text] == NO){
        
        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please Insert correct email to post Suggestions." delegate:nil
                         cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
        return;
        
    }
    
    if ([_txtView_Comments.text isEqualToString:comment])
    {
        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please Insert some Suggestions to post." delegate:nil
                         cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
        return;
    }
    
    
    
    email = _txtField_email.text;
    [[NSUserDefaults standardUserDefaults] setValue:email forKey:kUser_Mail];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *url = [NSURL URLWithString:@"http://demo.hupp.in/sample_html_email/sendmail.php"];
    NSDictionary *dictionary = @{ @"mail_id" : _txtField_email.text, @"content": _txtView_Comments.text};
    
    NSData *JSONData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:0
                                                         error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = JSONData;
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    // Create a task.
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (!error)
        {
            //NSLog(@"Status code: %i", ((NSHTTPURLResponse *)response).statusCode);
            NSError *error = nil;
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            NSLog(@"%@",dictionary);
            NSInteger success = [dictionary[@"success"] integerValue];
            
            if (success == 1) {
                //success! do stuff with the jsonData dictionary here
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Suggestions successfully send." delegate:nil
                                     cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
                    
                });
            }
            else {
                //handle ERROR from server
            }
        }
        else
        {
            NSLog(@"Error: %@", error.localizedDescription);
        }
    
        dispatch_async(dispatch_get_main_queue(), ^{
            self.txtField_email.text = email;
            self.txtView_Comments.text = comment;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            for (UIView *tmp in self.view.subviews){
                if ([tmp isKindOfClass:[MBProgressHUD class]])
                    [tmp removeFromSuperview];
            }
        });
    }];
    
    // Start the task.
    [task resume];
    
    
    
//    if (_txtView_Comments.text.length<=0 || [_txtView_Comments.text isEqual:comment]) {
//        NSLog(@"textview empty");
//    } else {
//
//        NSString *emailTitle = @"SUGGESTIONS ";
//        NSString *emailBody = _txtView_Comments.text;
//
//        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
//        mc.mailComposeDelegate = self;
//        NSArray *toRecipents = [NSArray arrayWithObject:@"praveenajilles@gmail.com"];
//
//        if ([MFMailComposeViewController canSendMail] && mc)
//        {
//            [mc setSubject:emailTitle];
//            [mc setMessageBody:emailBody isHTML:NO];
//            [mc setToRecipients:toRecipents];
//
//            //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
//
//            // [mc setToRecipients:toRecipents];
//            [self presentViewController:mc animated:YES completion:NULL];
//        }
//
//    }
    
    
    
    
}


- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
