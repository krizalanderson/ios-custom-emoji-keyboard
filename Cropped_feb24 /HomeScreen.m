//
//  HomeScreen.m
//  Cropped
//
//  Created by webastral on 03/10/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
#import "HomeScreen.h"
#import "MZViewController.h"
#import "MZResultViewController.h"
#import "MZCroppableView.h"
#import "Suggestions.h"
#import "Organise.h"
#import "Help.h"
#import "FCColorPickerViewController.h"
#import "Animator.h"
#import "NYXImagesKit.h"
#import "MBProgressHUD.h"
#import "MZAppDelegate.h"
#import "DTLoupeView.h"
#import "UIImage+ThreeD.h"
#import "SelectText.h"
#import "UIView+ColorOfPoint.h"
#import <AVFoundation/AVFoundation.h>
#import "AUIAutoGrowingTextView.h"
#import "CameraViewController.h"
#import "TOCropViewController.h"

@import Photos;
@import AVFoundation;

@interface HomeScreen ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate,HideViewDelegate,UITextViewDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CACameraCropDelegate,TOCropViewControllerDelegate>
{
    MZCroppableView *mzCroppableView;
    BOOL viewLoaded;
    UIImage *originalImage,*croppedImage,*borderLayout,*ImageWithBorder;
    UITableView *UserList;
    NSArray *records,*size,*colorNames,*colorsArray,*fontStyle;
    UIColor *selectedColor;
    UITapGestureRecognizer *singleFingerTap;
    UICollectionView *_collectionView;
    NSString *selectedSize,*selectedFontStyle;
    UIImageView *View,*textImage;
    UILabel *label;
    
    CGFloat firstX;
    CGFloat firstY,angle;
    CGPoint *firstLocation;
    // variables for multiple gestures on single label
    CGFloat tx; // x translation
    CGFloat ty; // y translation
    CGFloat scale; // zoom scale
    CGFloat theta; // rotation angle
    BOOL checkWidth, imageClipped;
    int staticpoint,heightOfLabel;
    UIButton *doneTitle;
    UIImage *NoBorder, *thickBorder, *mediumBorder, *thinBorder;
    UIView *viewForLabel;
    float unit, lastDegree;
    BOOL saveVideo, isTextImage, allowEndTextEditing;
    NSString* ImageName;
    MZAppDelegate *delegate;
    UIVisualEffectView *effectView;
    NSInteger index;
    CGRect initialFrame;
    NSString *FontName;
    NSArray *ForeGroundColor;
    NSString *TextViewString;
    UITextView *view;
    UIImageView *colorPicker;
    NSMutableArray *TextViewValues;
    int indexOfDict;
    CGFloat previousScale;
    UIPinchGestureRecognizer * pinch;
    UIRotationGestureRecognizer *rotate;
    UIPanGestureRecognizer *pan;
    UITapGestureRecognizer *singleTapOnImage_View;
    UIImageView *imageView123;
    
    CGAffineTransform rotationDegree;
    
    NSString *datestr;
    
    BOOL checkImageRoteted;
    
    CGPoint imageViewCenter;
    BOOL isTextAdded;
    CGPoint isTextAddedCenterPoint;
    
}

@end

@implementation HomeScreen
@synthesize isImageSelectcted,image_View,btn_Camera,btn_Gallery,isFromPreview,btn_Crop,imagePreview,btn_editText,darkView,top_View,baseView;
int flag=0,j=0;

//MARK:- VIEW CYCLE METHODS
- (void)viewDidLoad {
    [super viewDidLoad];
    darkView.hidden = YES;
    imagePreview.hidden = YES;
    self.navigationController.delegate = self;
    //    CGRect rect1 = image_View.frame;
    //    CGRect rect2 = image_View.frame;
    //    [image_View setFrame:[MZCroppableView scaleRespectAspectFromRect1:rect1 toRect2:rect2]];
    
    image_View.clipsToBounds = YES;
    //image_View.backgroundColor = [UIColor brownColor];
    
    image_View.contentMode = UIViewContentModeScaleAspectFit;
    
    isFromPreview = NO;
    [self colorArray];
    imageClipped = NO;
    singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [darkView addGestureRecognizer:singleFingerTap];
    indexOfDict = -1;
    doneTitle = [UIButton buttonWithType:UIButtonTypeSystem];
    [doneTitle setTitle:@"Done" forState:UIControlStateNormal];
    
    
    //    fontStyle = @[@"3Dumb",@"Alphabet Fantasie",@"Amazon Palafita",@"Bullpen3D",@"ChocolateDropsNF",@"From Cartoon Blocks",@"FunSized",@"Good Choice",@"GreatPoints",@"Guestservice",@"Gunplay3D",@"House-Paint-Shadow",@"ka1",@"KarmaFuture",@"KGPartyOnTheRooftop",@"LateClub",@"Ming in Bling",@"NextLevel",@"NineteenOhFive",@"Perspect",@"planetbe",@"PlayAlong",@"PonyMaker",@"RaceFlow",@"Sans Serif Shaded",@"Scribble Table",@"SignPaintersGothicShaded",@"SolidBrand",@"TacoTruckMilitia",@"TakeTwo",@"ThirdHand-solid",@"ThirdHand",@"Tonight",@"Trouble",@"VegasNeon",@"Wedgie Regular",@"White Bold"];
    
    angle = 0;
    [self setCornerRadius];
    btn_Crop.hidden = YES;
    _btn_Border.hidden = YES;
    btn_editText.hidden = YES;
    _btn_saveImage.hidden = YES;
    _btn_Cancel.hidden = YES;
    
    
    TextViewValues = [[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(cropImage) name:@"CutTheImage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveTestNotification:)name:@"TextImage" object:nil];
    delegate  = (MZAppDelegate *)[[UIApplication sharedApplication]delegate];
    isTextImage = NO;
    [delegate preferredStatusBarStyle];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(DoneKeyboard:)
                                                 name:@"Done"
                                               object:nil];
    // [self topViewBubtn_BorderttonsAppearance];
    
    
    checkImageRoteted = NO;
    //[self photoLibraryAvailabilityCheck];
    [self requestCameraPermissionsIfNeeded];
    
    
    pinch = [[UIPinchGestureRecognizer alloc] init];
    pan = [[UIPanGestureRecognizer alloc] init];
    singleTapOnImage_View = [[UITapGestureRecognizer alloc] init];
    rotate = [[UIRotationGestureRecognizer alloc] init];
    
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    // configure send button and top view
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, self.top_View.frame.size.height - 1.0f, self.top_View.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithRed:0.71 green:0.72 blue:0.73 alpha:1.0].CGColor;
    
    [self.top_View.layer addSublayer:bottomBorder];
    [self.view bringSubviewToFront:self.top_View];
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    //    if (image_View.image)
    //        [self AddCroppableView];
    if (isTextImage) {
        // show text view for add text to create image to add on image.
        [self addTextEditingComponents:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSLog(@"%f",self.image_View.frame);
    
}
//MARK:- GALLERY ACCESS
-(void)photoLibraryAvailabilityCheck{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized:
                break;
            case PHAuthorizationStatusRestricted:
                [self requestPhotosLibraryAccess];
                break;
            case PHAuthorizationStatusDenied:
                [self requestPhotosLibraryAccess];
                break;
            case PHAuthorizationStatusNotDetermined:
                [self requestPhotosLibraryAccess];
                break;
            default:
                [self requestPhotosLibraryAccess];
                break;
        }
    }];
}

-(void)requestPhotosLibraryAccess{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized:
                NSLog(@"Authorization done");
                break;
            default:
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self notifyUserOfPhotosAccessDenial];
                });
                
                
                break;
        }
    }];
}


#pragma mark Top View Buttons Setting
-(void) topViewButtonsAppearance
{
    int width = self.view.frame.size.width;
    NSLog(@"%f %f",_btn_Organise.frame.size.width,_btn_Organise.frame.size.height);
    int totalSpace = width-240;
    int spaceBetweenButton = totalSpace/5;
    _btn_Organise.frame = CGRectMake(spaceBetweenButton, _btn_Organise.frame.origin.y, _btn_Organise.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Create.frame = CGRectMake(spaceBetweenButton+_btn_Organise.frame.origin.x+_btn_Organise.frame.size.width, _btn_Organise.frame.origin.y, _btn_Create.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Suggestion.frame = CGRectMake(spaceBetweenButton+_btn_Create.frame.origin.x+_btn_Create.frame.size.width, _btn_Organise.frame.origin.y, _btn_Suggestion.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Help.frame = CGRectMake(spaceBetweenButton+_btn_Suggestion.frame.origin.x+_btn_Suggestion.frame.size.width, _btn_Organise.frame.origin.y, _btn_Help.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Organise.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Create.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Suggestion.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Help.translatesAutoresizingMaskIntoConstraints = YES;
    
    [_btn_Create.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [_btn_Organise.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_btn_Suggestion.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_btn_Help.titleLabel setFont:[UIFont systemFontOfSize:15]];
    //    [infoLabel setFont:[UIFont boldSystemFontOfSize:16]];
}



- (BOOL)prefersStatusBarHidden {
    return NO;
}

-(void) addTextEditingComponents:(UIImageView *)ImageView
{
    [self addVisualEffectView];
    [self addTextView:ImageView];
}


-(void) showImage
{
    CGFloat size1 = 180.0;
    UIFont *font = [UIFont fontWithName:@"MarkerFelt-Wide" size:size1];
    UIFont *systemFont = [UIFont systemFontOfSize:250];
    int depth = 8;
    BOOL shine = YES;
    UIColor *ForegroundColor,*ShadowColor,*outlineColor;
    ForegroundColor = [UIColor greenColor];
    ShadowColor = [UIColor orangeColor];
    outlineColor = [UIColor blueColor];
    UIColor *color = [UIColor colorWithRed:(200/255.f) green:(200/255.f) blue:(200/255.f) alpha:1.0];
    UIImage *my3dImage = [UIImage create3DImageWithText:@"Virat Kohli" Font:systemFont ForegroundColor:ForegroundColor ShadowColor:ShadowColor outlineColor:outlineColor depth:depth useShine:shine];
    UIImage *my3dImage2 = [UIImage create3DImageWithText:@"Joga Singh" Font:font ForegroundColor:ForegroundColor ShadowColor:ShadowColor outlineColor:outlineColor depth:depth useShine:shine];
    //image_View.image = image;
    image_View.hidden = NO;
}

-(void) showControls
{
    // btn_Crop.hidden = NO;
    _btn_Border.hidden = NO;
    btn_editText.hidden = NO;
}

-(void) setCornerRadius
{
    int radius = self.view.frame.size.width/16;
    btn_Crop.layer.cornerRadius = radius;
    btn_Crop.clipsToBounds = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark ShowIndicator
-(void)showIndicator
{
    UIView *whiteView = [[UIView alloc]initWithFrame:self.view.frame];
    whiteView.backgroundColor = [UIColor whiteColor];
    whiteView.alpha = 0.8;
    whiteView.tag = 999;
    [self.view addSubview:whiteView];
    
    //    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.mode = MBProgressHUDModeIndeterminate;
    //    hud.label.text = @"Please Wait...";
    //    hud.animationType = MBProgressHUDAnimationZoom;
    //    hud.bezelView.backgroundColor = [UIColor blackColor];
    //    hud.contentColor = [UIColor whiteColor];
}

-(void) HideIndicator
{
    for (UIView *whiteView in self.view.subviews) {
        if ([whiteView isKindOfClass:[UIView class]] && whiteView.tag == 999) {
            [whiteView removeFromSuperview];
        }
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark array of colors
-(void) colorArray
{
    UIColor *Red = [UIColor redColor];
    UIColor *Black = [UIColor blackColor];
    UIColor *Green = [UIColor greenColor];
    UIColor *Blue = [UIColor blueColor];
    UIColor *Yellow = [UIColor yellowColor];
    UIColor *Orange = [UIColor orangeColor];
    UIColor *Brown = [UIColor brownColor];
    UIColor *Gray = [UIColor grayColor];
    
    
    colorsArray = @[Red,Black,Green,Blue,Yellow,Orange,Brown,Gray];
    colorNames = @[@"Red",@"Black",@"Green",@"Blue",@"Yellow",@"Orange",@"Brown",@"Gray"];
    size = @[@"9",@"10",@"11",@"12",@"13",@"14",@"18",@"24",@"36",@"48"];
}

#pragma mark Add Table View
-(void)tableView:(UIButton *)sender
{
    for (UITableView *table in _view_Prompt.subviews) {
        if ([table isKindOfClass:[UITableView class]]) {
            [table removeFromSuperview];
        }
    }
    if (sender.tag == 100) {
        UserList = [[UITableView alloc]initWithFrame:CGRectMake(0,20,self.view.frame.size.width,self.view.frame.size.height-20) style:UITableViewStylePlain];
    }
    else
    {
        // for Size
        UserList = [[UITableView alloc]initWithFrame:CGRectMake(sender.frame.origin.x, sender.frame.origin.y-200, sender.frame.size.width, 200) style:UITableViewStylePlain];
    }
    
    
    
    [UserList registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    UserList.tag = sender.tag;
    UserList.backgroundColor = [UIColor clearColor];
    UserList.delegate = self;
    UserList.dataSource = self;
    [self.view addSubview:UserList];
    [self popUpZoomIn:UserList];
    
}

#pragma mark Table View Delegate Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 100) {
        // color
        records = fontStyle;
        return records.count;
    }
    records = fontStyle;
    return  records.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    //    cell.backgroundColor = [UIColor redColor];
    //    return cell;
    
    for (UILabel *lbl in cell.contentView.subviews) {
        [lbl removeFromSuperview];
    }
    static NSString *simpleTableIdentifier = @"cell";
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    UILabel *text = [[UILabel alloc]initWithFrame:cell.bounds];
    text.text = [records objectAtIndex:indexPath.row];
    text.textAlignment = NSTextAlignmentCenter;
    [text setFont:[UIFont fontWithName:[fontStyle objectAtIndex:indexPath.row] size:20]];
    //text.font = [UIFont systemFontOfSize:13];
    [cell.contentView addSubview:text];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // txt_username.text = [records objectAtIndex:indexPath.row];
    if (tableView.tag == 100) {
        [_btn_FontStyle setTitle:[records objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        [_btn_FontStyle.titleLabel setFont:[UIFont fontWithName:[records objectAtIndex:indexPath.row] size:15.0]];
        selectedFontStyle = [records objectAtIndex:indexPath.row];
        UserList.hidden=YES;
    }
    else
    {
        [_btn_Size setTitle:[records objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        UserList.hidden=YES;
    }
    //[darkView addGestureRecognizer:singleFingerTap];
    [self popZoomOut:UserList];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @" ";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGRect frame = tableView.frame;
    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-70, 0, 60, 26)];
    [addButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, 30)];
    title.text = @"Please Select:";
    title.textColor = [UIColor whiteColor];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    headerView.backgroundColor = [UIColor blackColor];
    [headerView addSubview:title];
    [headerView addSubview:addButton];
    
    return headerView;
}


#pragma mark Add Collection View
-(void) collectionView:(UIButton *)sender
{
    
    for (UICollectionView *collection in _view_Prompt.subviews) {
        if ([collection isKindOfClass:[UICollectionView class]]) {
            [collection removeFromSuperview];
        }
    }
    CGRect frame = CGRectMake(sender.frame.origin.x, sender.frame.origin.y+sender.frame.size.height, sender.frame.size.width, 30);
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _collectionView=[[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    _collectionView.tag = sender.tag;
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor lightGrayColor]];
    [self.view_Prompt addSubview:_collectionView];
}

#pragma mark Collection View Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (collectionView.tag == 100) {
        // color
        records = fontStyle;
        return records.count;
    }
    records = size;
    return  records.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UILabel *lbl in cell.contentView.subviews)
    {
        if ([lbl isKindOfClass:[UILabel class]])
        {
            [lbl removeFromSuperview];
        }
    }
    
    if (collectionView.tag == 100) {
        
        UILabel *label = [[UILabel alloc]initWithFrame:cell.bounds];
        label.text = @"AB";
        label.textColor = [UIColor whiteColor];
        [label setFont:[UIFont fontWithName:[fontStyle objectAtIndex:indexPath.row] size:20]];
        label.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:label];
        cell.backgroundColor = [UIColor blackColor];
    }
    else
    {
        UILabel *label = [[UILabel alloc]initWithFrame:cell.bounds];
        label.text = [size objectAtIndex:indexPath.row];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:label];
        cell.backgroundColor = [UIColor blackColor];
        
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (collectionView.tag == 100) {
        [_btn_FontStyle setTitle:[records objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        //btn_editText settitl
        [_btn_FontStyle.titleLabel setFont:[UIFont fontWithName:[records objectAtIndex:indexPath.row] size:15.0]];
        selectedFontStyle = [records objectAtIndex:indexPath.row];
    }
    else
    {
        [_btn_Size setTitle:[records objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        selectedSize = _btn_Size.titleLabel.text;
    }
    [darkView addGestureRecognizer:singleFingerTap];
    // collectionView.hidden = YES;
    [collectionView removeFromSuperview];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(30, 30);
}


# pragma mark Crop Button
- (IBAction)crop_Image:(UIButton *)sender {
    [self cropImage];
}


# pragma mark NSNotification Methods
-(void) cropImage
{
    croppedImage = nil;
    croppedImage = [mzCroppableView deleteBackgroundOfImage:image_View];
    if (croppedImage == nil) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Please Draw Pattern to crop image" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString( @"OK", @"Alert OK button" ) style:UIAlertActionStyleCancel handler:^( UIAlertAction *action ) {
            [self cancel];
        }];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        //[[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Draw Pattern to crop image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
    }
    else
    {
        borderLayout = nil;
        borderLayout = [self imageWithView:mzCroppableView];
        UIImageView *temp = [[UIImageView alloc]init];
        temp.frame = image_View.frame;
        temp.image = borderLayout;
        //  UIImage *croppedImage2 = [mzCroppableView deleteBackgroundOfImage:temp];
        ImageWithBorder = nil;
        thickBorder = nil;
        mediumBorder = nil;
        thinBorder = nil;
        NoBorder = nil;
        imageClipped = YES;
        [self showIndicator];
        image_View.backgroundColor = [UIColor clearColor];
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_DEFAULT,0), ^{
            
           [self callmethod:croppedImage];
            dispatch_async(dispatch_get_main_queue(), ^{
                //[self callmethod:croppedImage];
                [self showIcon:croppedImage];
                
            });
        });

        
        //[self performSelectorInBackground:@selector(callmethod:)withObject:croppedImage];
        
        pinch = [[UIPinchGestureRecognizer alloc] init];
        pan = [[UIPanGestureRecognizer alloc] init];
        singleTapOnImage_View = [[UITapGestureRecognizer alloc] init];
        rotate = [[UIRotationGestureRecognizer alloc] init];
        
        [pinch addTarget:self action:@selector(twoFingerPinch:)];
        [pan addTarget:self action:@selector(moveMyObject:)];
        [singleTapOnImage_View addTarget:self action:@selector(handleSingleTapOnImage_View:)];
        [rotate addTarget:self action:@selector(rotation:)];
        
//        [image_View addGestureRecognizer:pan];
//        [image_View addGestureRecognizer:singleTapOnImage_View];
//        [image_View addGestureRecognizer:pinch];
//        [image_View addGestureRecognizer:rotate];
        
        image_View.userInteractionEnabled = YES;
        
        
       
        
        image_View.gestureRecognizers = @[pinch, rotate, pan, singleTapOnImage_View];
        
        for (UIGestureRecognizer *recognizer in image_View.gestureRecognizers)
            recognizer.delegate = self;

    }
}
- (void)handleSingleTapOnImage_View:(UIPanGestureRecognizer *)recognizer
{
    [image_View addGestureRecognizer:pan];
    [image_View addGestureRecognizer:pinch];
    [image_View addGestureRecognizer:rotate];
}

- (void)moveMyObject:(UIPanGestureRecognizer *)recognizer
{
    

    CGPoint translation = [recognizer translationInView:baseView];
    
    
    
    CGRect recognizerFrame = recognizer.view.frame;
    recognizerFrame.origin.x += translation.x;
    recognizerFrame.origin.y += translation.y;

    
//    if (CGRectContainsRect(self.baseView.bounds, recognizerFrame)) {
//
//    }else{
//        return;
//    }

    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:baseView];

    if(recognizer.state == UIGestureRecognizerStateEnded) {

        CGPoint velocity = [recognizer velocityInView:baseView];
        CGFloat magnitude = sqrtf((velocity.x * velocity.x) + (velocity.y * velocity.y));
        CGFloat slideMult = magnitude / 200;
        NSLog(@"magnitude: %f, slideMult: %f", magnitude, slideMult);

        float slideFactor = 0.1 * slideMult; // Increase for more of a slide
        CGPoint finalPoint = CGPointMake(recognizer.view.center.x + (velocity.x * slideFactor),
                                         recognizer.view.center.y + (velocity.y * slideFactor));
        finalPoint.x = MIN(MAX(finalPoint.x, 0), self.baseView.bounds.size.width);
        finalPoint.y = MIN(MAX(finalPoint.y, 0), self.baseView.bounds.size.height);
        [UIView animateWithDuration:slideFactor*2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            recognizer.view.center = finalPoint;
            imageViewCenter = finalPoint;
        } completion:nil];
    }
    
    
    
  
    
//    if(recognizer.state == UIGestureRecognizerStateEnded) {
//        NSLog(@"panGest ended");
//        //baseView.b = recognizer.view.frame;
//        baseView.bounds = recognizer.view.bounds;
//        //recognizer.view.center = touchLocation;
//    }
//
//        //[image_View bringSubviewToFront:imageView123];
//        CGPoint pointLocation = [recognizer locationInView:baseView];
//        recognizer.view.center = pointLocation;
}
- (void)twoFingerPinch:(UIPinchGestureRecognizer *)recognizer
{
    CGSize scale1 = recognizer.view.bounds.size;
    NSLog(@"==================scale=========1%f",scale1.height);
    if (scale1.height > self.baseView.bounds.size.height){
        
        return;
    }
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1.0;
    
    recognizer.scale = 1.0;
    
    
    
    //rotationDegree = self.image_View.transform;
    rotationDegree = self.baseView.transform;
    
    
    
//        if (recognizer.scale >1.0f && recognizer.scale < 2.5f)
//        {
//            CGAffineTransform transform = CGAffineTransformMakeScale(recognizer.scale, recognizer.scale);
//            image_View.transform = transform;
//
//        }
}
-(void) rotation:(UIRotationGestureRecognizer *) sender
{
    sender.view.transform = CGAffineTransformRotate(sender.view.transform, sender.rotation);
    sender.rotation = 0.0;
    checkImageRoteted = YES;
    //[image_View updateConstraints];
    //[image_View snapshotViewAfterScreenUpdates:YES];
    //    if ([sender state] == UIGestureRecognizerStateBegan || [sender state] == UIGestureRecognizerStateChanged)
    //    {
    //        [sender view].transform = CGAffineTransformRotate([[sender view] transform], [(UIRotationGestureRecognizer *)sender rotation]);
    //        [(UIRotationGestureRecognizer *)sender setRotation:0];
    //        //[image_View updateConstraints];
    //        //[image_View snapshotViewAfterScreenUpdates:YES];
    //    }
}

- (void) DoneKeyboard:(NSNotification *) notification
{
    NSLog(@"DoneKeyboard tapped");
    allowEndTextEditing = YES;
}
- (void) receiveTestNotification:(NSNotification *) notification
{
    
    NSDictionary *dictionary = (NSDictionary *)notification.object;
    NSString  *number = [dictionary valueForKey:@"Index"];
    index = [number integerValue];
    //    UIImage *image = [self createText_String:@"123" Index:(int)index];
    //[self addImageOnImage:image String:@"000"];
    isTextImage = YES;
    
    NSMutableArray *textFormats = [[NSMutableArray alloc]initWithArray:delegate.textFormats];
    NSDictionary *dictionary2 = [textFormats objectAtIndex:index];
    FontName = [dictionary2 valueForKey:@"FontName"];
    ForeGroundColor = [dictionary2 valueForKey:@"ForeGroundColor"];
}


#pragma mark Burning Images
-(void) burnTextinImageView
{
    
    //image_View.backgroundColor = [UIColor clearColor];
    //UIImage *labelImage = [HomeScreen imageWithView:image_View];
    self.btn_Cancel.hidden = YES;
    self.btn_editText.hidden = YES;
    self.btn_saveImage.hidden = YES;
    self.top_View.hidden = YES;
    self.image_View.backgroundColor = [UIColor clearColor];
    self.baseView.backgroundColor = [UIColor clearColor];
    
    UIImage *labelImage = [self captureView];
    
    image_View.image = labelImage;
    [self setUpMZCroppableView];
    
    originalImage = labelImage;
    btn_editText.hidden = NO;
    top_View.userInteractionEnabled = YES;
}

#pragma mark Image Border Methods

-(UIImage *)changeWhiteColorTransparent: (UIImage *)image
{
    CGImageRef rawImageRef=image.CGImage;
    
    const CGFloat colorMasking[6] = {222, 255, 222, 255, 222, 255};
    //  NSArray *color = @[@"222",@"255",@"222",@"255",@"222",@"255"];
    UIGraphicsBeginImageContext(image.size);
    CGImageRef maskedImageRef=CGImageCreateWithMaskingColors(rawImageRef, colorMasking);
    {
        //if in iphone
        CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, image.size.height);
        CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    }
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, image.size.width, image.size.height), maskedImageRef);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(maskedImageRef);
    UIGraphicsEndImageContext();
    return result;
}

- (UIImage*) replaceColor:(UIColor*)color inImage:(UIImage*)image withTolerance:(float)tolerance {
    CGImageRef imageRef = [image CGImage];
    
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    NSUInteger bitmapByteCount = bytesPerRow * height;
    
    unsigned char *rawData = (unsigned char*) calloc(bitmapByteCount, sizeof(unsigned char));
    
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    
    CGColorRef cgColor = [color CGColor];
    const CGFloat *components = CGColorGetComponents(cgColor);
    float r = components[0];
    float g = components[1];
    float b = components[2];
    //float a = components[3]; // not needed
    
    r = r * 255.0;
    g = g * 255.0;
    b = b * 255.0;
    
    const float redRange[2] = {
        MAX(r - (tolerance / 2.0), 0.0),
        MIN(r + (tolerance / 2.0), 255.0)
    };
    
    const float greenRange[2] = {
        MAX(g - (tolerance / 2.0), 0.0),
        MIN(g + (tolerance / 2.0), 255.0)
    };
    
    const float blueRange[2] = {
        MAX(b - (tolerance / 2.0), 0.0),
        MIN(b + (tolerance / 2.0), 255.0)
    };
    
    int byteIndex = 0;
    
    while (byteIndex < bitmapByteCount) {
        unsigned char red   = rawData[byteIndex];
        unsigned char green = rawData[byteIndex + 1];
        unsigned char blue  = rawData[byteIndex + 2];
        
        if (((red >= redRange[0]) && (red <= redRange[1])) &&
            ((green >= greenRange[0]) && (green <= greenRange[1])) &&
            ((blue >= blueRange[0]) && (blue <= blueRange[1]))) {
            // make the pixel transparent
            //
            rawData[byteIndex] = 0;
            rawData[byteIndex + 1] = 0;
            rawData[byteIndex + 2] = 0;
            rawData[byteIndex + 3] = 0;
        }
        byteIndex += 4;
    }
    UIImage *result = [UIImage imageWithCGImage:CGBitmapContextCreateImage(context)];
    CGContextRelease(context);
    free(rawData);
    return result;
}

-(UIImage *) imageWithView:(UIView *)view1
{
    
    UIGraphicsBeginImageContextWithOptions(view1.frame.size, NO, 0.0);
    [view1.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
    
}

-(UIImage *) imagefromImageView:(UIView *)view1
{
    UIGraphicsBeginImageContextWithOptions(view1.bounds.size, view.opaque, 4.0);
    [view1.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

-(UIImage *) mergeImage:(UIImage *)background withClipped:(UIImage *)clipped
{

    UIImage *image = nil;
    CGSize newSize = clipped.size;
    UIGraphicsBeginImageContext( newSize );
    [clipped drawInRect:CGRectMake(newSize.width/3.1,newSize.height/3.3,newSize.width/3.1,newSize.height/2.9) blendMode:kCGBlendModeNormal alpha:1.0];
    [background drawInRect:CGRectMake(newSize.width/3.1,newSize.height/3.3,newSize.width/3.1,newSize.height/2.9) blendMode:kCGBlendModeNormal alpha:1.0];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
    

// Uncomment code get croped image zoomed
    
//    UIImage *image = nil;
//    CGSize newSize = clipped.size;
//    UIGraphicsBeginImageContext( newSize );
//    [clipped drawInRect:CGRectMake(1,1,newSize.width-2,newSize.height-2) blendMode:kCGBlendModeNormal alpha:1.0];
//    [background drawInRect:CGRectMake(0,0,newSize.width,newSize.height) blendMode:kCGBlendModeNormal alpha:1.0];
//    
//    image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return image;
    
}



- (UIImage*)imageByCombiningImage:(UIImage*)clippedImage withClearMask:(UIImage*)ClearMask andBorderMask:(UIImage *)borderMask Case:(int)choice {
    
    UIImage *image = nil;
    CGSize newSize = CGSizeMake(clippedImage.size.width, clippedImage.size.height);
    UIGraphicsBeginImageContext( newSize );
    
    //[clippedImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    if (choice) {
        [borderMask drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    }
    
    switch (choice)
    {
        case 0:
            // if no border is selected
            [ClearMask drawInRect:CGRectMake(2,2,newSize.width-4,newSize.height-4) blendMode:kCGBlendModeNormal alpha:1.0];
            break;
            
        case 1:
            
            [ClearMask drawInRect:CGRectMake(2,2,newSize.width-4,newSize.height-4) blendMode:kCGBlendModeNormal alpha:1.0];
            
            break;
        case 2:
            
            [ClearMask drawInRect:CGRectMake(6,6,newSize.width-12,newSize.height-12) blendMode:kCGBlendModeNormal alpha:1.0];
            break;
        case 3:
            
            [ClearMask drawInRect:CGRectMake(10,10,newSize.width-20,newSize.height-20) blendMode:kCGBlendModeNormal alpha:1.0];
            break;
    }
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
    
}

- (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage {
    UIImage *image = nil;
    CGSize newSize = CGSizeMake(firstImage.size.width+80, firstImage.size.height+80);
    UIGraphicsBeginImageContext( newSize );
    [firstImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    [secondImage drawInRect:CGRectMake(30,30,newSize.width-60,newSize.height-60) blendMode:kCGBlendModeNormal alpha:1.0];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
-(void) showIcon:(UIImage *)croppedImage
{
    // when cut out show in screen
    //imagePreview.hidden = NO;
    imagePreview.alpha = 0;
    imagePreview.image = croppedImage;
    imagePreview.layer.borderWidth = 1;
    [mzCroppableView removeFromSuperview];
    // [btn_Crop setTitle:@"Save" forState:UIControlStateNormal];
    btn_Crop.tag = 100;
    image_View.image = croppedImage;
    originalImage = croppedImage;
    //    _btn_Border.hidden = NO;
    _btn_Border.userInteractionEnabled = YES;
    //    [UIView animateWithDuration:0.5 animations:^{
    //        image_View.alpha = 0.0;
    //        imagePreview.alpha = 1.0;
    //    }completion:^(BOOL finished){
    //        image_View.hidden = YES;
    //        top_View.userInteractionEnabled = NO;
    //        [self HideIndicator];
    //    }];
    [self HideIndicator];
    btn_Crop.userInteractionEnabled = NO;
    btn_Crop.userInteractionEnabled = NO;
    mzCroppableView.croppingPath = nil;
    // image_View.backgroundColor = [UIColor whiteColor];
    
    self.bottomView.hidden = NO;
    _btn_Border.hidden = NO;
    btn_editText.hidden = NO;
    _btn_saveImage.hidden = NO;
    _btn_Cancel.hidden = NO;
    
}
# pragma mark Camera Button

- (IBAction)openCamera:(UIButton *)sender {
    
    
    
    
    
    //    if ([sender.titleLabel.text isEqualToString:@"Camera"]) {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        
        image_View.transform = CGAffineTransformIdentity;
//        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//        picker.delegate = self;
//        [picker setAllowsEditing:YES];
//        //[picker setShowsCameraControls:YES];
//        [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
        
        CameraViewController *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"CameraViewController"];
        object.delegate = self;
        [self presentViewController:object animated:NO completion:nil];
        
        //[self presentViewController:self.homeVC animated:YES completion:NULL];
    }
}

//MARK:- CAMERA VIEW DELEGATE

-(void)didCroppedImage:(UIImage *)image{
     [self SetupChoosenImage:image];
    
}

- (void)requestCameraPermissionsIfNeeded {
    
    // check camera authorization status
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    switch (authStatus) {
        case AVAuthorizationStatusAuthorized: { // camera authorized
            // do camera intensive stuff
        }
            break;
        case AVAuthorizationStatusNotDetermined: { // request authorization
            
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if(granted) {
                        // do camera intensive stuff
                    } else {
                        [self notifyUserOfCameraAccessDenial];
                    }
                });
            }];
        }
            break;
        case AVAuthorizationStatusRestricted:
        case AVAuthorizationStatusDenied: {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if(granted) {
                        // do camera intensive stuff
                    } else {
                        [self notifyUserOfCameraAccessDenial];
                    }
                });
            }];
        }
            break;
        default:
            break;
    }
}

- (void)notifyUserOfCameraAccessDenial {
    // display a useful message asking the user to grant permissions from within Settings > Privacy > Camera
    
    NSString *message = NSLocalizedString( @"My App doesn't have permission to use the camera, please change privacy settings", @"Alert message when the user has denied access to the camera" );
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"meBoard" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString( @"OK", @"Alert OK button" ) style:UIAlertActionStyleCancel handler:^( UIAlertAction *action ) {
       //[self photoLibraryAvailabilityCheck];
    }];
    [alertController addAction:cancelAction];
    
    // Provide quick access to Settings.
    UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:NSLocalizedString( @"Settings", @"Alert button to open Settings" ) style:UIAlertActionStyleDefault handler:^( UIAlertAction *action ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    [alertController addAction:settingsAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)notifyUserOfPhotosAccessDenial {
    // display a useful message asking the user to grant permissions from within Settings > Privacy > Camera
    
    NSString *message = NSLocalizedString( @"My App doesn't have permission to use the Photos, please change privacy settings", @"Alert message when the user has denied access to the Photos" );
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"meBoard" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString( @"OK", @"Alert OK button" ) style:UIAlertActionStyleCancel handler:^( UIAlertAction *action ) {
        
    }];
    [alertController addAction:cancelAction];
    // Provide quick access to Settings.
    UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:NSLocalizedString( @"Settings", @"Alert button to open Settings" ) style:UIAlertActionStyleDefault handler:^( UIAlertAction *action ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    [alertController addAction:settingsAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void) cancel
{
    // cancel method
    image_View.transform = CGAffineTransformIdentity;
    croppedImage = nil;
    
    [self.btn_Camera setHidden:NO];
    [self.btn_Gallery setHidden:NO];
    
    [label removeFromSuperview];
    [viewForLabel removeFromSuperview];
    
    [image_View removeGestureRecognizer:pan];
    [image_View removeGestureRecognizer:pinch];
    [image_View removeGestureRecognizer:rotate];
    [image_View removeGestureRecognizer:singleTapOnImage_View];
    
    
    
    isImageSelectcted = NO;
    //image_View.alpha=0;
    //imageView123.alpha=0;
    [mzCroppableView removeFromSuperview];
    
    image_View.image =  nil;//[UIImage imageNamed:@""];
    [imageView123 removeFromSuperview];
    imageView123.image = nil;
    imageView123 = nil;
    [imageView123 setUserInteractionEnabled:NO];
    
    
    imagePreview.hidden = YES;
    image_View.hidden = NO;
    //    _btn_Border.hidden = YES;
    //  image_View.backgroundColor = [UIColor whiteColor];
    _btn_Border.userInteractionEnabled = NO;
    selectedColor = nil;
    //viewForLabel = nil;
    [viewForLabel removeFromSuperview];
    [top_View setUserInteractionEnabled:YES];
//    [UIView animateWithDuration:1.0
//                     animations:^{
//                         image_View.alpha = 1.0;
//                     }
//                     completion:^(BOOL finished){
//                         //                             NSLog(@"completion block");
//                         //                             image_View.alpha = 1.0;
//                     }];
    
    for (UIImageView *view1 in image_View.subviews) {
        if ([view1 isKindOfClass:[UIImageView class]]) {
            [view1 removeFromSuperview];
        }
    }
    for (UIImageView *view1 in baseView.subviews) {
        if (view1.tag == 101){
            [view1 removeFromSuperview];
        }
    }
    [textImage removeFromSuperview];
    [viewForLabel removeFromSuperview];
    btn_editText.hidden = YES;
    _btn_Border.hidden = YES;
    _btn_saveImage.hidden = YES;
    btn_Crop.hidden = YES;
    _btn_Cancel.hidden = YES;
    _bottomView.hidden  = NO;
    
    
    [TextViewValues removeAllObjects];
    
    [self updateTitle];
}
# pragma mark Gallery Button
- (IBAction)open_Gallery:(UIButton *)sender {
    //    if ([sender.titleLabel.text isEqualToString:@"Gallery"]) {
    //[self setUpMZCroppableView];
    image_View.transform = CGAffineTransformIdentity;
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
    
//    UIViewController *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"TestVC"];
//    [self presentViewController:object animated:NO completion:nil];
    
}
//    else if([sender.titleLabel.text isEqualToString:@"Reset"])
//    {
// Reset Method
//
//    }
//    else
//    {
//        // Clear
//        [label removeFromSuperview];
//        [viewForLabel removeFromSuperview];
//        [self setUpMZCroppableView];
//
//
//    }
//}


-(void) resetMethod
{
    image_View.hidden = NO;
    imagePreview.hidden = YES;
    //    _btn_Border.hidden = YES;
    _btn_Border.userInteractionEnabled = NO;
    image_View.image = originalImage;
    //viewForLabel = nil;
    [viewForLabel removeFromSuperview];
    [self setUpMZCroppableView];
    [label removeFromSuperview];
    btn_editText.hidden = NO;
    [btn_Crop setTitle:@"Crop" forState:UIControlStateNormal];
    image_View.alpha = 1.0;
    top_View.userInteractionEnabled = YES;
    selectedColor = nil;
}
#pragma mark Update Titles of buttons
-(void)updateTitle
{
    if (!isImageSelectcted) {
        // [btn_Gallery setTitle:@"Gallery" forState:UIControlStateNormal];
        // [btn_Camera setTitle:@"Camera" forState:UIControlStateNormal];
        // btn_Crop.hidden = YES;
        btn_editText.hidden = YES;
    }
    else
    {
        // [btn_Gallery setTitle:@"Reset" forState:UIControlStateNormal];
        // [btn_Camera setTitle:@"Cancel" forState:UIControlStateNormal];
        // btn_Crop.hidden = NO;
        btn_editText.hidden = NO;
        
    }
}

# pragma mark Image Picker Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
//    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//    NSLog(@"%@",chosenImage);
    //    originalImage = chosenImage;
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    //self.image_View.image = image;
     //[self SetupChoosenImage:image];
    

    [picker dismissViewControllerAnimated:NO completion:^{
        
        TOCropViewController *cropController = [[TOCropViewController alloc] initWithImage:image];
        cropController.delegate = self;
        cropController.toolbar.cancelTextButtonTitle = @"Cancel";
        cropController.toolbar.clampButtonHidden = YES;
        cropController.toolbar.rotateClockwiseButtonHidden = YES;
        cropController.toolbar.rotateCounterclockwiseButtonHidden = YES;
        [cropController setImageCropFrame:CGRectMake(image.size.width/2, image.size.height/2, image.size.width, image.size.width)];
        [cropController setMinimumAspectRatio:image.size.width];
        //[[cropController cropView] setAspectRatio:image_View.frame.size];
//        cropController.toolbar.resetButtonTapped = ^{
//            NSLog(@"reset btn tapped");
//        };
        [[cropController cropView] setResetAspectRatioEnabled:NO];
        //cropController.toolbar.resetButton.titleLabel.text = @"Full";
        
        [self presentViewController:cropController animated:NO completion:nil];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    isImageSelectcted = NO;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


-(void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle{
    //self.image_View.image = image;
    [self SetupChoosenImage:image];
    [cropViewController dismissViewControllerAnimated:NO completion:nil];
    
}


-(void)SetupChoosenImage:(UIImage *)chosenImage{
    
    NSLog(@"%@",chosenImage);
    
    [label removeFromSuperview];
    image_View.image = chosenImage;
    image_View.backgroundColor = [UIColor clearColor];
    btn_editText.hidden = NO;
    _btn_Border.hidden = NO;
    _btn_saveImage.hidden = NO;
    _btn_Cancel.hidden = NO;
    
    [self.btn_Camera setHidden:YES];
    [self.btn_Gallery setHidden:YES];
    
    imageClipped = NO;
    NoBorder = nil;
    selectedColor = nil;
    isImageSelectcted = YES;
    //[self updateTitle];
    mzCroppableView.croppingPath = nil;
    
    btn_Crop.userInteractionEnabled = YES;
    btn_Crop.userInteractionEnabled = YES;
    //  [_btn_Border setUserInteractionEnabled:NO];
    originalImage = [self imageWithView:image_View];
    //originalImage = [[self class] imageWithImage:chosenImage scaledToMaxWidth:1024.0 maxHeight:1024.0];
    
    image_View.image = originalImage;
    isFromPreview = YES;
    [self showControls];
    //int height = [self aspectratio:originalImage.size Width:image_View.frame.size.width];
    //    image_View.frame = CGRectMake(0, 0, image_View.frame.size.width, height);
    // image_View.translatesAutoresizingMaskIntoConstraints = YES;
    //image_View.frame = CGRectMake(0, baseView.frame.size.height/2-height/2, image_View.frame.size.width, height);
    [self setUpMZCroppableView];
    // image_View.frame = CGRectMake(0, baseView.frame.size.height/2-height/2, image_View.frame.size.width, height);
    NSLog(@"%@",chosenImage);
    
    //    image_View.center = baseView.center;
    //  baseView.translatesAutoresizingMaskIntoConstraints = YES;
    
    // image_View.backgroundColor = [UIColor redColor];
    // image_View.backgroundColor = [UIColor greenColor];
    //[btn_Crop setTitle:@"Crop" forState:UIControlStateNormal];
    //image_View.contentMode = UIViewContentModeScaleAspectFit;
    
}



#pragma mark Go to View Controllers
- (IBAction)Goto_Suggestion:(UIButton *)sender {
    
    
    // [MZAppDelegate GotoSuggestion];
    Suggestions *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Suggestions"];
    [self gotoSpecificController:object];
    
}

- (IBAction)goto_Help:(UIButton *)sender {
    //    [self showAlert:@"View is in under progress and will be available soon"];
    Help *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    [self gotoSpecificController:object];
}

- (IBAction)goto_Create:(UIButton *)sender {
    //    HomeScreen *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeScreen"];
    //    [self gotoSpecificController:object];;
}

- (IBAction)goto_Organise:(UIButton *)sender {
    //     [self showAlert:@"View is in under progress and will be available soon"];
    Organise *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Organise"];
    [self gotoSpecificController:object];;
}



#pragma mark ImageView
- (void)setUpMZCroppableView {
    //[_view_Prompt removeFromSuperview];
    [mzCroppableView removeFromSuperview];
    mzCroppableView = [[MZCroppableView alloc] initWithImageView:image_View];
    mzCroppableView.delegate = self;
    [self.baseView addSubview:mzCroppableView];
    [self.baseView addSubview:btn_editText];
    [self.baseView addSubview:btn_Crop];
    [self.baseView addSubview:_btn_Border];
    [self.baseView addSubview:_btn_saveImage];
    [self.baseView addSubview:_btn_Cancel];
    //[_btn_Border setUserInteractionEnabled:NO];
    // ç≈      [self.view addSubview:_view_Prompt];
    
    //testing croppable view
    //mzCroppableView.backgroundColor = [UIColor redColor];
    //self.image_View.backgroundColor = [UIColor blueColor];
    //self.imagePreview.backgroundColor = [UIColor yellowColor];
}



#pragma mark Goto Specific Controller
-(void) gotoSpecificController:(UIViewController *)controller
{
    BOOL isExist = NO;
    for (UIViewController *object in self.navigationController.viewControllers)
    {
        if ([object isKindOfClass:[controller class]])
        {
            NSLog(@"POP");
            //Do not forget to import AnOldViewController.h
            isExist = YES;
            [self.navigationController popToViewController:object  animated:NO];
            
            break;
        }
        
    }
    if (!isExist) {
        NSLog(@"PUSH");
        [self.navigationController pushViewController:controller animated:NO];
    }
}

#pragma mark Navigation Controller Delegate Methods
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController*)fromVC
                                                 toViewController:(UIViewController*)toVC
{
    if (operation == UINavigationControllerOperationPush)
        return [[PushAnimator alloc] init];
    
    if (operation == UINavigationControllerOperationPop)
        return [[PopAnimator alloc] init];
    
    return nil;
}

#pragma mark Add Text into Image Method
- (UIImage *)burnTextIntoImage:(NSString *)text  color:(UIColor *)color size:(NSString *)size{
    UIImage *img = [UIImage imageNamed:@"Transparent_Image"];
    
    CGRect aRectangle = [text
                         boundingRectWithSize:img.size
                         options:NSStringDrawingUsesLineFragmentOrigin
                         attributes:@{
                                      NSFontAttributeName : [UIFont systemFontOfSize:[size floatValue]]
                                      }
                         context:nil];
    // CGRect aRectangle = CGRectMake(0,0, img.size.width, img.size.height);
    //  CGRect bRectangle = CGRectMake(0,0, 100, 100);
    CGSize tempSize = CGSizeMake(aRectangle.size.width, aRectangle.size.height);
    img = [self resizeImage:img Size:tempSize];
    UIGraphicsBeginImageContext(img.size);
    [img drawInRect:aRectangle];
    
    [color set];           // set text color
    NSInteger fontSize = (int)[size intValue];
    //    if ( [text length] > 200 ) {
    //        fontSize = 10;
    //    }
    UIFont *font = [UIFont boldSystemFontOfSize: fontSize];     // set text font
    
    [ text drawInRect : aRectangle                      // render the text
             withFont : font
        lineBreakMode : UILineBreakModeTailTruncation  // clip overflow from end of last line
            alignment : UITextAlignmentLeft ];
    
    UIImage *theImage=UIGraphicsGetImageFromCurrentImageContext();
    // extract the image
    UIGraphicsEndImageContext();     // clean  up the context.
    return theImage;
}

-(UIImage *) resizeImage:(UIImage *)image Size:(CGSize)tempSize
{
    UIImage *tempImage = nil;
    UIGraphicsBeginImageContext(tempSize);
    
    CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
    thumbnailRect.origin = CGPointMake(0.0,0.0);
    thumbnailRect.size.width  = tempSize.width;
    thumbnailRect.size.height = tempSize.height;
    
    [image drawInRect:thumbnailRect];
    tempImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tempImage;
}

#pragma mark Edit text Button
- (IBAction)edit_Text:(UIButton *)sender {
    

    UIImage *croppedImage = [mzCroppableView deleteBackgroundOfImage:image_View];
    if (croppedImage != nil) {
        
        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Changes will not be saved" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil]show];
    }
    else{
        [self EnterText];
    }
}


-(void)EnterText
{
    // MZAppDelegate s
    _bottomView.hidden = NO;
    UIImage *image = [self imagefromImageView:self.view];
    SelectText *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"SelectText"];
    object.backgroundImage = image;
    object.isFromHomeScreen = YES;
    [self presentViewController:object animated:NO completion:nil];
}

#pragma mark Prompt View Controls Methods
-(void) showPrompt
{
    _view_Prompt.hidden = NO;
    
    if (!selectedColor)
    {
        _txtField_text.text = @"";
        [_btn_Size setTitle:@"Please Select" forState:UIControlStateNormal];
        [_btn_Color setTitle:@"Please Select" forState:UIControlStateNormal];
        [_btn_FontStyle setTitle:@"Please Select" forState:UIControlStateNormal];
        [_btn_FontStyle.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_btn_Color setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_btn_Color setBackgroundColor:[UIColor whiteColor]];
    }
    baseView.userInteractionEnabled = NO;
    top_View.userInteractionEnabled = NO;
    //   _view_Prompt.alpha = 0.0;
    [self popUpZoomIn:_view_Prompt];
    darkView.hidden = NO;
    darkView.alpha = 0.0;
    [UIView animateWithDuration:0.3
                     animations:^{
                         darkView.alpha =0.7;
                     }
                     completion:^(BOOL finished){
                     }];
}

-(void) hidePrompt
{
    darkView.hidden = YES;
    UserList.hidden = YES;
    btn_editText.hidden = NO;
    baseView.userInteractionEnabled = YES;
    top_View.userInteractionEnabled = YES;
    [self.view endEditing:YES];
    [self popZoomOut:_view_Prompt];
    _view_Prompt.hidden = YES;
    if (!imageClipped) {
        [self setUpMZCroppableView];
    }
    
    //    if (!image_View.hidden) {
    //         [self setUpMZCroppableView];
    //    }
    
}

#pragma Prompt Options Method
- (IBAction)select_Color:(UIButton *)sender {
    [self pickColor:self];
    // [self tableView:sender];
    
}

- (IBAction)select_Size:(UIButton *)sender {
    // [self tableView:sender];
    [self collectionView:sender];
    [self.view endEditing:YES];
}

- (IBAction)select_Font:(UIButton *)sender
{
    [self tableView:sender];
    // [self collectionView:sender];
    [self.view endEditing:YES];
    
}

- (IBAction)done_Prompt:(UIButton *)sender {
    
    NSString *text = _txtField_text.text;
    if ([_txtField_text.text  isEqualToString: @""]) {
        [self showAlert:@"Please Enter Text"];
    }
    else  if ([_btn_Color.titleLabel.text containsString:@"Please Select"]) {
        [self showAlert:@"Please select color"];
    }
    //    else if([_btn_Size.titleLabel.text containsString:@"Please Select"])
    //    {
    //        [self showAlert:@"Please select size"];
    //    }
    else if([_btn_FontStyle.titleLabel.text containsString:@"Please Select"])
    {
        [self showAlert:@"Please select Font Style"];
    }
    else
    {
        [self hidePrompt];
        NSString *sizeValue,*title;
        UIColor *color;
        sizeValue = @"26";
        title = _txtField_text.text;
        NSInteger index = [colorNames indexOfObject:_btn_Color.titleLabel.text];
        color = selectedColor;
        
        
        // selectedFontStyle
        //  [self newImageView:[self burnTextIntoImage:title color:color size:sizeValue]];
        [self addLabelOnImage:[self newLabel:title color:color size:sizeValue FontStyle:selectedFontStyle]];
        // baseView.userInteractionEnabled = NO;
        //  [btn_Gallery setTitle:@"Clear" forState:UIControlStateNormal];
        top_View.userInteractionEnabled = NO;
        //        btn_Gallery.userInteractionEnabled = NO;
        // btn_Crop.userInteractionEnabled = NO;
        [btn_Crop setTitle:@"Add" forState:UIControlStateNormal];
        btn_Crop.tag = 100;
    }
}

#pragma mark New Label
-(UILabel *) newLabel:(NSString *)text  color:(UIColor *)color size:(NSString *)size1 FontStyle:(NSString *)style
{
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    //gettingSizeLabel.font = [UIFont fontWithName:@"YOUR FONT's NAME" size:16];
    gettingSizeLabel.text = text;
    gettingSizeLabel.numberOfLines = 1;
    gettingSizeLabel.textColor = color;
    [gettingSizeLabel setFont:[UIFont fontWithName:style size:[size1 floatValue]]];
    gettingSizeLabel.frame = CGRectMake(0, 0, 320, 200);
    [gettingSizeLabel sizeToFit];
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(600, CGFLOAT_MAX);
    
    CGSize expectSize = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    CGRect aRectangle = [text
                         boundingRectWithSize:CGSizeMake(600, 100)
                         options:NSStringDrawingUsesLineFragmentOrigin
                         attributes:@{
                                      NSFontAttributeName : [UIFont systemFontOfSize:[size1 floatValue]]
                                      }
                         context:nil];
    
    CGRect textRect = [text boundingRectWithSize:maximumLabelSize
                                         options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                      attributes:@{NSFontAttributeName:gettingSizeLabel.font}
                                         context:nil];
    
    
    
    //NSLog(@"aRectangle:\n%f %f",aRectangle.size.width,aRectangle.size.height);
    //gettingSizeLabel.frame=textRect;
    // gettingSizeLabel.frame=CGRectMake(image_View.frame.origin.x, image_View.frame.origin.y, textRect.size.width, textRect.size.height);
    
    return gettingSizeLabel;
    
}

-(void)addLabelOnImage:(UILabel *)label1
{
    [label removeFromSuperview];
    [viewForLabel removeFromSuperview];
    label = [[UILabel alloc]init];
    label = label1;
    heightOfLabel = label.frame.size.height;
    viewForLabel= [[UIView alloc]initWithFrame:CGRectMake(0, 0, label.frame.size.width, label.frame.size.height)];
    viewForLabel.userInteractionEnabled = YES;
    viewForLabel.backgroundColor = [UIColor redColor];
    label.center = viewForLabel.center;
    [viewForLabel addSubview:label];
    
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveViewWithGestureRecognizer:)];
    panGestureRecognizer.delegate = self;
    
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchWithGestureRecognizer:)];
    pinchGestureRecognizer.delegate = self;
    
    UIRotationGestureRecognizer *rotationGestureRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotationWithGestureRecognizer:)];
    rotationGestureRecognizer.delegate = self;
    
    
    label.gestureRecognizers = @[panGestureRecognizer, pinchGestureRecognizer, rotationGestureRecognizer];
    //viewForLabel.gestureRecognizers = @[panGestureRecognizer, pinchGestureRecognizer, rotationGestureRecognizer];
    
    for (UIGestureRecognizer *recognizer in viewForLabel.gestureRecognizers)
        recognizer.delegate = self;
    
    [mzCroppableView removeFromSuperview];
    [label setUserInteractionEnabled:YES];
    //    label.backgroundColor = [UIColor yellowColor];
    NSLog(@"label:\n%f %f",label.frame.size.width,label.frame.size.height);
    
    if (!image_View.hidden) {
        
        [image_View addSubview:label];
        image_View.userInteractionEnabled = YES;
        //label.center = image_View.center;
    }
    else
    {
        [imagePreview addSubview:label];
        // imagePreview.layer.borderWidth = 1;
        imagePreview.userInteractionEnabled = YES;
        // label.center = imagePreview.center;
    }
    //[self calculations];
}


#pragma mark Calculation for dymanic view
-(void) calculations
{
    CGSize LabelSize = CGSizeMake(label.frame.size.width, label.frame.size.height);
    
    float diffrence = LabelSize.width-LabelSize.height;
    NSString *value = [NSString stringWithFormat:@"%.2f",diffrence/45];
    unit = [value floatValue];
    NSLog(@"Unit: %f",unit);
}
#pragma mark Gestures Methods


//#pragma mark - Loupe Methods

- (void)handleLongPress:(UILongPressGestureRecognizer *)gesture
{
    DTLoupeView *loupe = [DTLoupeView sharedLoupe];
    CGPoint touchPoint = [gesture locationInView:self.view];
    
    switch (gesture.state)
    {
        case UIGestureRecognizerStateBegan:
        {
            // Init loupe just once for performance
            // It should be removed/release etc somewhere else when
            // editing is complete or maybe in dealloc
            
            loupe.style = DTLoupeStyleCircle;
            loupe.targetView = self.view;
            
            // The Initial TouchPoint needs to be set before we set the style
            loupe.touchPoint = touchPoint;
            
            // Normally you would set the loupe that require
            //  i.e. _loupe.type = DTLoupeStyleRectangle;
            // In this project we using our UIControls Values
            
            // Default Magnification is 1.2
            loupe.magnification = 2.0;
            
            [loupe presentLoupeFromLocation:touchPoint];
            
            
            break;
        }
            
        case UIGestureRecognizerStateChanged:
        {
            // Show Cursor and position between glyphs
            loupe.touchPoint = touchPoint;
            
            break;
        }
            
        default:
        {
            [loupe dismissLoupeTowardsLocation:touchPoint];
            
            break;
        }
    }
}


// enable multiple gestures simultaneously
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:
(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


-(void)ChangeTextViewColor:(UIPanGestureRecognizer *)panGestureRecognizer
{
    NSLog(@"Gesture called");
    CGPoint touchPoint = [panGestureRecognizer locationInView: panGestureRecognizer.view];
    if (CGRectContainsPoint(panGestureRecognizer.view.bounds, touchPoint))
    {
        UIColor *color = [panGestureRecognizer.view colorOfPoint:touchPoint];
        if (touchPoint.x>0 && touchPoint.y>0 && touchPoint.x<panGestureRecognizer.view.frame.size.width && touchPoint.y<panGestureRecognizer.view.frame.size.height) {
            view.textColor = color;
        }
    }
    
    
}
-(void)moveViewWithGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer
{
    NSLog(@"text image moving");
    [image_View removeGestureRecognizer:pan];
    //[label removeFromSuperview];
    //[baseView addSubview:imageView123];
    //[baseView insertSubview:label atIndex:1];
    //[baseView addSubview:label];
    //CGPoint touchLocation = [panGestureRecognizer locationInView:image_View];
    //panGestureRecognizer.view.center = touchLocation;
    
    CGPoint translation = [panGestureRecognizer translationInView:baseView];
    panGestureRecognizer.view.center = CGPointMake(panGestureRecognizer.view.center.x + translation.x, panGestureRecognizer.view.center.y + translation.y);
    [panGestureRecognizer setTranslation:CGPointMake(0, 0) inView:baseView];
    
    if(panGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        
        CGPoint velocity = [panGestureRecognizer velocityInView:baseView];
        CGFloat magnitude = sqrtf((velocity.x * velocity.x) + (velocity.y * velocity.y));
        CGFloat slideMult = magnitude / 200;
        NSLog(@"magnitude: %f, slideMult: %f", magnitude, slideMult);
        
        float slideFactor = 0.1 * slideMult; // Increase for more of a slide
        CGPoint finalPoint = CGPointMake(panGestureRecognizer.view.center.x + (velocity.x * slideFactor),
                                         panGestureRecognizer.view.center.y + (velocity.y * slideFactor));
        finalPoint.x = MIN(MAX(finalPoint.x, 0), self.baseView.bounds.size.width);
        finalPoint.y = MIN(MAX(finalPoint.y, 0), self.baseView.bounds.size.height);
        [UIView animateWithDuration:slideFactor*2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            panGestureRecognizer.view.center = finalPoint;
            isTextAddedCenterPoint = finalPoint;
        } completion:nil];
    }
    
    
    
    if (panGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        [image_View addGestureRecognizer:pan];
        //[image_View addGestureRecognizer:pinch];
        //[image_View addGestureRecognizer:rotate];
        //[image_View addGestureRecognizer:singleTapOnImage_View];
    }
    
    //    CGPoint translationInView = [(UIPanGestureRecognizer *)panGestureRecognizer translationInView:panGestureRecognizer.view];
    //    NSLog(@"%f %f", translationInView.x,translationInView.y);
    //    CGRect firstRectFrame = panGestureRecognizer.view.frame;
    //    firstRectFrame.origin.x = firstRectFrame.origin.x + translationInView.x;
    //    firstRectFrame.origin.y = firstRectFrame.origin.y + translationInView.y;
    //    panGestureRecognizer.view.frame = firstRectFrame;
    //    if (!image_View.hidden) {
    //        if (firstRectFrame.origin.x>=0 && firstRectFrame.origin.y>=0 && firstRectFrame.origin.x<=image_View.frame.size.width-firstRectFrame.size.width && firstRectFrame.origin.y<=image_View.frame.size.height-firstRectFrame.size.height)
    //        {
    //
    //        }
    //    }
    //    else
    //    {
    //        if (firstRectFrame.origin.x>=0 && firstRectFrame.origin.y>=0 && firstRectFrame.origin.x<=imagePreview.frame.size.width-firstRectFrame.size.width && firstRectFrame.origin.y<=imagePreview.frame.size.height-firstRectFrame.size.height)
    //        {
    ////            panGestureRecognizer.view.frame = firstRectFrame;
    //            viewForLabel.frame = firstRectFrame;
    //        }
    //    }
    
    
    //    [(UIPanGestureRecognizer *)panGestureRecognizer setTranslation:CGPointZero inView:panGestureRecognizer.view];
    //    NSLog(@"x:%f y:%f",viewForLabel.frame.origin.x,viewForLabel.frame.origin.y);
    
}

-(void)handlePinchWithGestureRecognizer:(UIPinchGestureRecognizer *)pinchGestureRecognizer{
    
    
    [image_View removeGestureRecognizer:pinch];
    
    pinchGestureRecognizer.view.transform = CGAffineTransformScale(pinchGestureRecognizer.view.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
    pinchGestureRecognizer.scale = 1.0;
    
    if (pinchGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        //[image_View addGestureRecognizer:pan];
        [image_View addGestureRecognizer:pinch];
        //[image_View addGestureRecognizer:rotate];
        //[image_View addGestureRecognizer:singleTapOnImage_View];
    }
    //    NSLog(@"%f %f",pinchGestureRecognizer.view.frame.origin.x,pinchGestureRecognizer.view.frame.size.width);
    //    NSLog(@"%f",pinchGestureRecognizer.scale);
    //    if (pinchGestureRecognizer.scale>1) {
    //        NSLog(@"Zoom Out");
    //        if (pinchGestureRecognizer.view.frame.origin.x>=0 && pinchGestureRecognizer.view.frame.origin.x+pinchGestureRecognizer.view.frame.size.width<self.view.frame.size.width) {
    //            pinchGestureRecognizer.view.transform = CGAffineTransformScale(pinchGestureRecognizer.view.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
    //            }
    //        else
    //        {
    //         pinchGestureRecognizer.view.frame = CGRectMake(1, pinchGestureRecognizer.view.frame.origin.y, pinchGestureRecognizer.view.frame.size.width, pinchGestureRecognizer.view.frame.size.height);
    //        }
    //    }
    //    else
    //    {
    //        NSLog(@"Zoom In");
    //        pinchGestureRecognizer.view.transform = CGAffineTransformScale(pinchGestureRecognizer.view.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
    //    }
    
    pinchGestureRecognizer.scale = 1.0;
    
}

-(void)handleRotationWithGestureRecognizer:(UIRotationGestureRecognizer *)rotationGestureRecognizer{
    //  NSLog(@"Rotstion: %f",rotationGestureRecognizer.rotation);
    
    [image_View removeGestureRecognizer:rotate];
    rotationGestureRecognizer.view.transform = CGAffineTransformRotate(rotationGestureRecognizer.view.transform, rotationGestureRecognizer.rotation);
    rotationGestureRecognizer.rotation = 0.0;
    if (rotationGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        //[image_View addGestureRecognizer:pan];
        //[image_View addGestureRecognizer:pinch];
        [image_View addGestureRecognizer:rotate];
        //[image_View addGestureRecognizer:singleTapOnImage_View];
    }
}

-(void) changeFrame:(CGFloat)degree
{
    // NSLog(@"Degree: %f",degree);
    // Case 1
    if (degree>0 && degree<45) {
        if (!lastDegree) {
            lastDegree = degree;
        }
        if (degree>lastDegree) {
            // NSLog(@"Incr");
            lastDegree = degree;
            viewForLabel.frame = CGRectMake(viewForLabel.frame.origin.x, viewForLabel.frame.origin.y-unit, viewForLabel.frame.size.width, viewForLabel.frame.size.height+unit);
        }
        else
        {
            //NSLog(@"Desc");
            lastDegree = degree;
            viewForLabel.frame = CGRectMake(viewForLabel.frame.origin.x, viewForLabel.frame.origin.y+unit, viewForLabel.frame.size.width, viewForLabel.frame.size.height-unit);
        }
        // NSLog(@"D: %f",degree);
    }
    
    // case 2
    else if (degree>45 && degree<90) {
        if (!lastDegree) {
            lastDegree = degree;
        }
        if (degree>lastDegree) {
            // NSLog(@"Incr");
            lastDegree = degree;
            viewForLabel.frame = CGRectMake(viewForLabel.frame.origin.x+unit, viewForLabel.frame.origin.y, viewForLabel.frame.size.width-unit, viewForLabel.frame.size.height);
        }
        else
        {
            // NSLog(@"Desc");
            lastDegree = degree;
            viewForLabel.frame = CGRectMake(viewForLabel.frame.origin.x-unit, viewForLabel.frame.origin.y, viewForLabel.frame.size.width+unit, viewForLabel.frame.size.height);
        }
        //  NSLog(@"D: %f",degree);
    }
    // case 3
    else if (degree>90 && degree<135) {
        if (!lastDegree) {
            lastDegree = degree;
        }
        if (degree>lastDegree) {
            //    NSLog(@"Incr");
            lastDegree = degree;
            viewForLabel.frame = CGRectMake(viewForLabel.frame.origin.x-unit, viewForLabel.frame.origin.y, viewForLabel.frame.size.width+unit, viewForLabel.frame.size.height);
        }
        else
        {
            //   NSLog(@"Desc");
            lastDegree = degree;
            viewForLabel.frame = CGRectMake(viewForLabel.frame.origin.x+unit, viewForLabel.frame.origin.y, viewForLabel.frame.size.width-unit, viewForLabel.frame.size.height);
        }
        // NSLog(@"D: %f",degree);
    }
    // case 4
    else if (degree>135 && degree<180) {
        if (!lastDegree) {
            lastDegree = degree;
        }
        if (degree>lastDegree) {
            //    NSLog(@"Incr");
            lastDegree = degree;
            viewForLabel.frame = CGRectMake(viewForLabel.frame.origin.x, viewForLabel.frame.origin.y+unit, viewForLabel.frame.size.width, viewForLabel.frame.size.height-unit);
        }
        else
        {
            //   NSLog(@"Desc");
            lastDegree = degree;
            viewForLabel.frame = CGRectMake(viewForLabel.frame.origin.x, viewForLabel.frame.origin.y-unit, viewForLabel.frame.size.width, viewForLabel.frame.size.height+unit);
        }
        // NSLog(@"D: %f",degree);
    }
    NSLog(@"X:%f Y:%f W:%f H:%f",viewForLabel.frame.origin.x,viewForLabel.frame.origin.y,viewForLabel.frame.size.width,viewForLabel.frame.size.height);
    
    // label.center = viewForLabel.center;
}

#pragma mark Alert View
-(void) showAlert:(NSString *)message
{
    [[[UIAlertView alloc]initWithTitle:@"Message" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
}

-(void)hideAlertView:(UIAlertView *)alertView
{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
    
}

//- (void)didPresentAlertView:(UIAlertView *)alertView
//{
//    if ([alertView.message containsString:@"Enter"] || [alertView.message containsString:@"select"] || [alertView.message containsString:@"progress"]) {
//        [self performSelector:@selector(hideAlertView:) withObject:alertView afterDelay:2];
//    }
//
//
//}

#pragma mark Zoom pop in out animations
- (void)popUpZoomIn:(UIView*)img{
    // UIView *img = _view_Prompt;
    [UIView animateWithDuration:0.3
                     animations:^{
                         img.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                     } completion:^(BOOL finished) {
                         [UIImageView animateWithDuration:0.3/2 animations:^{
                             img.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                         } completion:^(BOOL finished) {
                             [UIImageView animateWithDuration:0.3/2 animations:^{
                                 img.transform = CGAffineTransformIdentity;
                             }];
                         }];
                     }];
}


- (void)popZoomOut:(UIView *)img{
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         img.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
                     } completion:^(BOOL finished) {
                         //img.hidden = TRUE;
                     }];
}

#pragma mark Touch Event
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[self.view endEditing:YES];// this will do the trick
   
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    //[image_View sendSubviewToBack:imageView123];
    //[image_View addGestureRecognizer:pan];
}

#pragma mark Tap Gesture
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    NSLog(@"handling single tap event");
    [self hidePrompt];
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    // if (CGRectContainsPoint(self.menuView.bounds, [touch locationInView:self.menuView]))
    //     return NO;
    
    return YES;
}

- (void)showOptions:(UITapGestureRecognizer *)recognizer {
    NSLog(@"Show Options");
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"Options" message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction* deleteAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        UIImageView *imageView = (UIImageView *)recognizer.view;
        UIImage *Image = imageView.image;
        NSLog(@"Img");
        for (NSDictionary *dict in TextViewValues) {
            UIImage *imageInDict = [dict valueForKey:@"Image"];
            if ([imageInDict isEqual:Image]) {
                [TextViewValues removeObject:dict];
                break;
            }
        }
        // NSDictionary *dict = @{@"FontName":font,@"FontColor":color,@"Title":TextViewString,@"Image":img};
        NSLog(@"ImageFound");
        [recognizer.view removeFromSuperview];
       
        if ([TextViewValues count] == 0){
            [imageView123 removeFromSuperview];
            imageView123.image = nil;
            imageView123 = nil;
            [imageView123 setUserInteractionEnabled:NO];
        }
        [self.btn_editText setHidden:NO];
        
    }];
    
    UIAlertAction* EditAction = [UIAlertAction actionWithTitle:@"Edit" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImageView *imageView = (UIImageView *)recognizer.view;
    
        [self addTextEditingComponents:imageView];
        [recognizer.view removeFromSuperview];
//        [imageView123 removeFromSuperview];
//        imageView123.image = nil;
//        imageView123 = nil;
//        [imageView123 setUserInteractionEnabled:NO];
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:deleteAction];
    [alertController addAction:EditAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void) AddCroppableView
{
    BOOL imageExist = NO;
    for (UIImageView *view in image_View.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            imageExist = YES;
            break;
        }
    }
    if (!imageExist) {
        [self setUpMZCroppableView];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.message containsString:@"Changes"]) {
        if (buttonIndex == 1) {
            [mzCroppableView removeFromSuperview];
            mzCroppableView.croppingPath = nil;
            [self EnterText];
        }
    }
    else if([alertView.message containsString:@"Successfully"]) {
        [self cancel];
    }
}

#pragma mark Combine two images
-(UIImage *) addImageToImage:(UIImage *)image withImage2:(UILabel *)label
{
    
    //UIImage *image1 = [UIImage imageNamed:@"image1.png"];
    //UIImage *image2 = [UIImage imageNamed:@"image2.png"];
    
    UIImage *view = [[UIImage alloc]init];
    
    // [view addSubview:label];
    CGSize size1 = CGSizeMake(image.size.width, image.size.height);
    
    UIGraphicsBeginImageContext(size1);
    
    [image drawInRect:CGRectMake(0,0,size1.width, image.size.height)];
    //[view drawInRect:CGRectMake(view.frame.origin.x,label.frame.origin.y,label.frame.size.width, label.frame.size.height)];
    
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return finalImage;
    //set finalImage to IBOulet UIImageView
}

#pragma mark Combine two images
+ (UIImage *) imageWithView:(UIImageView *)imageView
{
//    UIGraphicsBeginImageContext(imageView.bounds.size);
//    [imageView.layer renderInContext:UIGraphicsGetCurrentContext()];
//    imageView.opaque = NO;
//    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return viewImage;
    
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0, imageView.image.size.width, imageView.image.size.height)];
    
    UIGraphicsBeginImageContextWithOptions(imageView.frame.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(bitmap, imageView.frame.size.width / 2, imageView.frame.size.height / 2);
    
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-imageView.image.size.width / 2, -imageView.image.size.height / 2 , imageView.image.size.width, imageView.image.size.height), imageView.image.CGImage );
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark Color Picker Methods
- (IBAction)pickColor:(id)sender {
    FCColorPickerViewController *colorPicker = [FCColorPickerViewController colorPickerWithColor:selectedColor
                                                                                        delegate:self];
    colorPicker.tintColor = [UIColor whiteColor];
    [colorPicker setModalPresentationStyle:UIModalPresentationFormSheet];
    [self presentViewController:colorPicker
                       animated:YES
                     completion:nil];
}

- (void)colorPickerViewController:(FCColorPickerViewController *)colorPicker
                   didSelectColor:(UIColor *)color
{
    if (color) {
        self.color = color;
        selectedColor = color;
        [_btn_Color setTitleColor:selectedColor forState:UIControlStateNormal];
        [_btn_Color setTitle:@"Selected" forState:UIControlStateNormal];
        [_btn_Color setBackgroundColor:color];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        NSAlertView(@"Message", @"Color Not Selected yet");
    }
    
}

- (void)colorPickerViewControllerDidCancel:(FCColorPickerViewController *)colorPicker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setColor:(UIColor *)color
{
    selectedColor = [color copy];
    //[self.view setBackgroundColor:selectedColor];
}

# pragma mark Border Methods
- (IBAction)add_Border:(UIButton *)sender {
    
    if (!NoBorder)
    {
        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please clip image first to add Border" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
    }
    else
    {
        [self changeBorderWidth];
    }
}

//    if (imagePreview.image == NoBorder) {
//        imagePreview.image = thinBorder;
//    }
//    else if(imagePreview.image == thinBorder)
//    {
//    imagePreview.image = mediumBorder;
//    }
//    else if(imagePreview.image == mediumBorder)
//    {
//        imagePreview.image = thickBorder;
//    }
//    else
//    {
//        imagePreview.image = NoBorder;
//    }




-(void) changeBorderWidth
{
    if (image_View.image == NoBorder) {
        image_View.image = thickBorder;
        originalImage = thickBorder;
    }
    else if(image_View.image == thickBorder)
    {
        image_View.image = mediumBorder;
        originalImage =mediumBorder;
    }
    else if(image_View.image == mediumBorder)
    {
        image_View.image = thinBorder;
        originalImage =thinBorder;
    }
    else
    {
        image_View.image = NoBorder;
        originalImage =NoBorder;
    }
}

#pragma mark Insert Border On Image Methods
-(void) callmethod:(UIImage *)image
{
    int selection;
    UIImage *imageBlackBorder,*imageClearBorder;
    for (int i=0; i<4; i++) {
        selection = i;
        if (selection) {
            if (!imageClearBorder) {
                imageClearBorder = [self insert_Border_On_Image:croppedImage Color:[UIColor whiteColor]];
            }
        }
        else
        {
            imageClearBorder = [self insert_Border_On_Image:croppedImage Color:[UIColor clearColor]];
        }
        //  to create mask of images (with black or white color)
        // uncommect below code to get border on cropped image
        
        //        if (!imageBlackBorder) {
        //            imageBlackBorder = [self insert_Border_On_Image:croppedImage Color:[UIColor blackColor]];
        //        }
        
        
        UIImage *result = [self resizeImage:imageClearBorder ratio:selection];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImageView *viewBottom = [[UIImageView alloc]initWithImage:imageBlackBorder];
            viewBottom.frame = CGRectMake(0, 0, imageBlackBorder.size.width, imageBlackBorder.size.height);
            UIImageView *viewTop = [[UIImageView alloc]initWithImage:result];
            viewTop.frame = CGRectMake(0, 0, result.size.width, result.size.height);
            viewTop.center = viewBottom.center;
            [viewBottom addSubview:viewTop];
        });
        //UIImage *ing =  [self imagefromImageView:viewBottom];
        
        UIImage *image =  [self imageByCombiningImage:croppedImage withClearMask:imageClearBorder andBorderMask:imageBlackBorder Case:selection];
        
        if (!selection) {
            imageClearBorder = nil;
        }
        
        image = [MZAppDelegate changeWhiteColorTransparent:image];
        
        image  = [self mergeImage:image withClipped:croppedImage];
        if (selection == 0) {
            NoBorder = image;
        }
        
        // uncommect below code to get border on cropped image
        else if (selection == 1) {
            thickBorder = image;
        }
        else if (selection == 2) {
            mediumBorder = image;
        }
        else if (selection == 3) {
            thinBorder = image;
        }
    }
    
    
   // [self performSelectorOnMainThread:@selector(showIcon:) withObject:thickBorder waitUntilDone:NO];
}
-(UIImage *) insert_Border_On_Image:(UIImage *)maskImage Color:(UIColor *)color
{
    
    UIGraphicsBeginImageContextWithOptions(maskImage.size, NO, maskImage.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    
    CGContextFillRect(context, CGRectMake(0.f, 0.f, maskImage.size.width, maskImage.size.height));
    
    
    UIImage *whiteImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //    return whiteImage; square box sent as image
    // Use CoreImage to mask the colored background to the mask (the entire opaque region of the mask)
    CIContext *ciContext = [CIContext contextWithOptions: nil];
    CIFilter *filter = [CIFilter filterWithName: @"CIBlendWithAlphaMask"];
    [filter setValue: [CIImage imageWithCGImage: whiteImage.CGImage]
              forKey: kCIInputImageKey];
    [filter setValue: [CIImage imageWithCGImage: maskImage.CGImage]
              forKey: kCIInputMaskImageKey];
    CIImage *whiteBackground = filter.outputImage;
    
    CGSize size1 = [self doubleSizeOfFrame:maskImage];
    [whiteImage scaleToSize:CGSizeMake(size1.width * maskImage.scale, size1.height * maskImage.scale)
                  usingMode: NYXResizeModeAspectFill];
    
    filter = [CIFilter filterWithName: @"CIBlendWithMask"];
    [filter setValue: [CIImage imageWithCGImage: whiteImage.CGImage]
              forKey: kCIInputImageKey];
    [filter setValue: whiteBackground
              forKey: kCIInputBackgroundImageKey];
    [filter setValue: [CIImage imageWithCGImage: maskImage.CGImage]
              forKey: kCIInputMaskImageKey];
    
    UIImage  *image  = [UIImage imageWithCGImage: [ciContext createCGImage: filter.outputImage
                                                                  fromRect: [filter.outputImage extent]]];
    return image;
}

- (CGSize)doubleSizeOfFrame:(UIImage *)view{
    CGSize size = view.size;
    return CGSizeMake(size.width*1, size.height*1);
}

- (IBAction)saveImageIntoDocumentDirectory:(UIButton *)sender
{
    /* commented on 13/12/2017
     
    [btn_Gallery setHidden:YES];
    [_btn_saveImage setHidden:YES];
    [btn_editText setHidden:YES];
    [_btn_Cancel setHidden:YES];
    [_bottomView setHidden:YES];
    [top_View setHidden:YES];*/
    
    NSDate *now = [NSDate date];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:NSLocaleIdentifier]];
//    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
//    [dateFormatter  setDateStyle:NSDateFormatterShortStyle];
//
//    datestr = [dateFormatter stringFromDate:date];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd_HH-mm-ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    datestr = [dateFormatter stringFromDate:now];
    
    NSLog(@"%@",datestr);
    
    
//    NSString *dateString = datestr;
//    NSDate *dateFromString = [[NSDate alloc] init];
//    dateFromString = [dateFormatter dateFromString:dateString];
//    NSTimeInterval timeInMiliseconds = [dateFromString timeIntervalSince1970]*1000;
//    NSInteger time = timeInMiliseconds;
//    NSLog(@"Time is as %ld",(long)time);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    ImageName = [NSString stringWithFormat:@"%@.png",datestr];
    NSLog(@"%@",ImageName);
    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    
    //    NSString *str;
    //    for(str in filePathsArray)
    //    {
    ////        if([str isEqualToString:ImageName])
    //        {
    
    j=0;
    saveVideo=YES;
    image_View.backgroundColor = [UIColor clearColor];
    
    
    [self burnTextinImageView];
    
    //[self saveImage];
    [self saveImage2];
    // NSLog(@"Saved Successfully: %@",str);
    //}
    //}
    // myView.transform = CGAffineTransformRotate(myView.transform, -currentRotationAngle);
    self.top_View.hidden = NO;
    [self cancel];
}

- (UIImage *)captureView {
    
    //hide controls if needed
    CGRect rect = [baseView bounds];
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.baseView.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
    
}

-(void) saveImage2
{
//    NSURL *groupContainerURL = [[NSFileManager defaultManager]containerURLForSecurityApplicationGroupIdentifier:@"group.com.summerinc.MeBoard"];
//    NSString *sharedDirectory = [groupContainerURL path];
    
    
    
    if (checkImageRoteted) {
        NSURL *groupContainerURL = [[NSFileManager defaultManager]containerURLForSecurityApplicationGroupIdentifier:@"group.com.summerinc.MeBoard"];
        NSString *sharedDirectory = [groupContainerURL path];
        
        //CGFloat radians = (250 * M_PI / 360);
        
        UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0, originalImage.size.width, originalImage.size.height)];
        //UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height)];
        CGAffineTransform t = rotationDegree; //CGAffineTransformMakeRotation(radians);
        rotatedViewBox.transform = t;
        CGSize rotatedSize = rotatedViewBox.frame.size;
        
        UIGraphicsBeginImageContextWithOptions(rotatedSize, NO, [[UIScreen mainScreen] scale]);
        CGContextRef bitmap = UIGraphicsGetCurrentContext();
        
        CGContextTranslateCTM(bitmap, rotatedSize.width / 2, rotatedSize.height / 2);
        
        CGContextConcatCTM(bitmap, rotationDegree);
        //CGContextRotateCTM(bitmap, radians);
        
        CGContextScaleCTM(bitmap, 1.0, -1.0);
        CGContextDrawImage(bitmap, CGRectMake(-originalImage.size.width / 2, -originalImage.size.height / 2 , originalImage.size.width, originalImage.size.height), originalImage.CGImage );
        
        
//        UIImage *newImage = [self captureView];
//        NSData *imageData = UIImagePNGRepresentation(newImage);
        
//        NSString *filePath = [sharedDirectory stringByAppendingPathComponent:ImageName];
//        [imageData writeToFile:filePath atomically:YES];
        
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *imageData = UIImagePNGRepresentation(newImage);
        
        NSString *filePath = [sharedDirectory stringByAppendingPathComponent:ImageName];
        [imageData writeToFile:filePath atomically:YES];

    }
    else{
        
        NSURL *groupContainerURL = [[NSFileManager defaultManager]containerURLForSecurityApplicationGroupIdentifier:@"group.com.summerinc.MeBoard"];
        NSString *sharedDirectory = [groupContainerURL path];
        
        
        UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0, originalImage.size.width, originalImage.size.height)];
        
        UIGraphicsBeginImageContextWithOptions(rotatedViewBox.frame.size, NO, [[UIScreen mainScreen] scale]);
        CGContextRef bitmap = UIGraphicsGetCurrentContext();
        
        CGContextTranslateCTM(bitmap, rotatedViewBox.frame.size.width / 2, rotatedViewBox.frame.size.height / 2);
        
        CGContextScaleCTM(bitmap, 1.0, -1.0);
        CGContextDrawImage(bitmap, CGRectMake(-originalImage.size.width / 2, -originalImage.size.height / 2 , originalImage.size.width, originalImage.size.height), originalImage.CGImage );
        
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *imageData = UIImagePNGRepresentation(newImage);
        
        NSString *filePath = [sharedDirectory stringByAppendingPathComponent:ImageName];
        [imageData writeToFile:filePath atomically:YES];
        
//        CGRect rect = CGRectMake(0,0,349,349);
//        UIGraphicsBeginImageContext( rect.size );
//        [originalImage drawInRect:rect];
//        UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//
//        NSData *imageData = UIImagePNGRepresentation(picture1);
//
//
//        // NSData *imageData = UIImagePNGRepresentation(originalImage);
//        NSString *filePath = [sharedDirectory stringByAppendingPathComponent:ImageName];
//        [imageData writeToFile:filePath atomically:YES];
    }
}



- (void)saveImage
{
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:ImageName];
//
//    UIImage *newImage = [self captureView];
//
//    NSData *imageData = UIImagePNGRepresentation(newImage);
//
//    [imageData writeToFile:savedImagePath atomically:NO];
    
    
    if (checkImageRoteted) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:ImageName];
        
        //CGFloat radians = (250 * M_PI / 360);
        
        UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0, originalImage.size.width, originalImage.size.height)];
        CGAffineTransform t = rotationDegree; //CGAffineTransformMakeRotation(radians);
        rotatedViewBox.transform = t;
        CGSize rotatedSize = rotatedViewBox.frame.size;
        
        UIGraphicsBeginImageContextWithOptions(rotatedSize, NO, [[UIScreen mainScreen] scale]);
        CGContextRef bitmap = UIGraphicsGetCurrentContext();
        
        CGContextTranslateCTM(bitmap, rotatedSize.width / 2, rotatedSize.height / 2);
        
        CGContextConcatCTM(bitmap, rotationDegree);
        
        CGContextScaleCTM(bitmap, 1.0, -1.0);
        CGContextDrawImage(bitmap, CGRectMake(-originalImage.size.width / 2, -originalImage.size.height / 2 , originalImage.size.width, originalImage.size.height), originalImage.CGImage );
        
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *imageData = UIImagePNGRepresentation(newImage);
        
        [imageData writeToFile:savedImagePath atomically:NO];
        NSLog(@"Image Saved At: \n%@",savedImagePath);
        UIImage* pngImage = [UIImage imageWithData:imageData];
        //UIImageWriteToSavedPhotosAlbum(pngImage, nil, nil, nil);
        
//        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
//            PHAssetChangeRequest *changeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:pngImage];
//            changeRequest.creationDate          = [NSDate date];
//        } completionHandler:^(BOOL success, NSError *error) {
//            if (success) {
//                NSLog(@"successfully saved");
//            }
//            else {
//                NSLog(@"error saving to photos: %@", error);
//            }
//        }];
        
        //NSAlertView2(@"Message", @"Image Saved to Photos Successfully");
        
    }
    else{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:ImageName];
        
        UIImage *image = originalImage; // imageView is my image from camera
        CGRect rect = CGRectMake(0,0,248,248);
        UIGraphicsBeginImageContext( rect.size );
        [image drawInRect:rect];
        UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *imageData = UIImagePNGRepresentation(picture1);
        
        // NSData *imageData = UIImagePNGRepresentation(image);
        [imageData writeToFile:savedImagePath atomically:NO];
        NSLog(@"Image Saved At: \n%@",savedImagePath);
        UIImage* pngImage = [UIImage imageWithData:imageData];
        //UIImageWriteToSavedPhotosAlbum(pngImage, nil, nil, nil);
//        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
//            PHAssetChangeRequest *changeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:pngImage];
//            changeRequest.creationDate          = [NSDate date];
//        } completionHandler:^(BOOL success, NSError *error) {
//            if (success) {
//                NSLog(@"successfully saved");
//            }
//            else {
//                NSLog(@"error saving to photos: %@", error);
//            }
//        }];
        //NSAlertView2(@"Message", @"Image Saved to Photos Successfully");
    }
}
- (IBAction)resetView:(UIButton *)sender {
    //  image_View.backgroundColor = [UIColor whiteColor];
    [self cancel];
}

#pragma mark Aspect Ratio Formula
-(int) aspectratio:(CGSize)sizeOfView Width:(int)width
{
    return (sizeOfView.height/sizeOfView.width)*width;
}

#pragma mark Protocol Method
-(void) hideView
{
    self.bottomView.hidden = YES;
    _btn_Border.hidden = YES;
    btn_editText.hidden = YES;
    _btn_saveImage.hidden = YES;
    //_btn_Cancel.hidden = YES;
}

#pragma mark Add Text Image
-(UIImageView *)addImageOnImage:(UIImage *)image String:(NSString *)string
{
    //    [self viewDidLoad];
    // [textImage removeFromSuperview];
    UIImageView *textImage = [[UIImageView alloc]init];
    // label = label1;
    textImage.image = image;
    int height = [self aspectratio:image.size Width:320];
    textImage.frame = CGRectMake(0, 0, 320, height);
    textImage.center = image_View.center;
    
    CGRect Rectangle = [string
                        boundingRectWithSize:CGSizeMake(600, 100)
                        options:NSStringDrawingUsesLineFragmentOrigin
                        attributes:@{
                                     NSFontAttributeName : [UIFont systemFontOfSize:100]
                                     }
                        context:nil];
    // textImage.frame = Rectangle;
    // heightOfLabel = label.frame.size.height;
    textImage.userInteractionEnabled = YES;
    //    textImage.backgroundColor = [UIColor redColor];
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveViewWithGestureRecognizer:)];
    //[label addGestureRecognizer:panGestureRecognizer];
    
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchWithGestureRecognizer:)];
    // [label addGestureRecognizer:pinchGestureRecognizer];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(showOptions:)];
    
    UIRotationGestureRecognizer *rotationGestureRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotationWithGestureRecognizer:)];
    rotationGestureRecognizer.delegate = self;
    // [label addGestureRecognizer:rotationGestureRecognizer];
    
    textImage.gestureRecognizers = @[panGestureRecognizer, pinchGestureRecognizer, rotationGestureRecognizer, singleFingerTap];
    
    for (UIGestureRecognizer *recognizer in textImage.gestureRecognizers)
        recognizer.delegate = self;
    [mzCroppableView removeFromSuperview];
    [textImage setUserInteractionEnabled:YES];
    //    label.backgroundColor = [UIColor yellowColor];
    NSLog(@"View: %f\n%f %f",textImage.frame.origin.y,textImage.frame.size.width,textImage.frame.size.height);
    return textImage;
}

#pragma mark Delete button method on image
-(void)deleteTextImage:(UIButton *)button
{
    UIImageView *img = (UIImageView *)[self.view viewWithTag:button.tag];
    [img removeFromSuperview];
}
-(void) hideDeleteButton:(UIButton *)button
{
    button.hidden = YES;
}

#pragma mark CreateText
-(UIImage *) createText_String:(NSString *)string Index:(int)index
{
    NSMutableArray *textFormats = [[NSMutableArray alloc]initWithArray:delegate.textFormats];
    NSDictionary *dictionary = [textFormats objectAtIndex:index];
    NSString *FontName = [dictionary valueForKey:@"FontName"];
    NSArray *ForeGroundColor = [dictionary valueForKey:@"ForeGroundColor"];
    NSArray *ShadowColor = [dictionary valueForKey:@"ShadowColor"];
    NSArray *OutlineColor = [dictionary valueForKey:@"OutlineColor"];
    int depth =[[dictionary valueForKey:@"depth"] intValue];
    UIImage *image =
    [self create3DImageWithText:string Font:[UIFont fontWithName:FontName size:100] ForegroundColor:[UIColor colorWithRed:[ForeGroundColor[0] floatValue]/255.f green:[ForeGroundColor[1] floatValue]/255.f blue:[ForeGroundColor[2] floatValue]/255.f alpha:[ForeGroundColor[3] floatValue]] ShadowColor:[delegate colorFromArray:ShadowColor] outlineColor:[delegate colorFromArray:OutlineColor] depth:(int)depth useShine:YES];
    return image;
}


#pragma mark 3D Image Text Method
- (UIImage *)create3DImageWithText:(NSString *)_text Font:(UIFont*)_font ForegroundColor:(UIColor*)_foregroundColor ShadowColor:(UIColor*)_shadowColor outlineColor:(UIColor*)_outlineColor depth:(int)_depth useShine:(BOOL)_shine {
    //calculate the size we will need for our text
    CGSize expectedSize = [_text sizeWithFont:_font constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
    
    
    //  CGSize expectedSize = CGSizeMake(70.0, 070.0);
    //increase our size, as we will draw in 3d, so we need extra space for 3d depth + shadow with blur
    expectedSize.height+=_depth+5;
    expectedSize.width+=_depth+5;
    
    UIColor *_newColor;
    
    UIGraphicsBeginImageContextWithOptions(expectedSize, NO, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //because we want to do a 3d depth effect, we are going to slightly decrease the color as we move back
    //so here we are going to create a color array that we will use with required depth levels
    NSMutableArray *_colorsArray = [[NSMutableArray alloc] initWithCapacity:_depth];
    
    CGFloat *components =  (CGFloat *)CGColorGetComponents(_foregroundColor.CGColor);
    
    //add as a first color in our array the original color
    [_colorsArray insertObject:_foregroundColor atIndex:0];
    
    //create a gradient of our color (darkening in the depth)
    int _colorStepSize = floor(100/_depth);
    
    for (int i=0; i<_depth; i++) {
        
        for (int k=0; k<3; k++) {
            if (components[k]>(_colorStepSize/255.f)) {
                components[k]-=(_colorStepSize/255.f);
            }
        }
        _newColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:CGColorGetAlpha(_foregroundColor.CGColor)];
        
        //we are inserting always at first index as we want this array of colors to be reversed (darkest color being the last)
        [_colorsArray insertObject:_newColor atIndex:0];
    }
    
    //we will draw repeated copies of our text, with the outline color and foreground color, starting from the deepest
    for (int i=0; i<_depth; i++) {
        
        //change color
        _newColor = (UIColor*)[_colorsArray objectAtIndex:i];
        
        //draw the text
        CGContextSaveGState(context);
        
        CGContextSetShouldAntialias(context, YES);
        
        //draw outline if this is the last layer (front one)
        if (i+1==_depth) {
            CGContextSetLineWidth(context, 1);
            CGContextSetLineJoin(context, kCGLineJoinRound);
            
            CGContextSetTextDrawingMode(context, kCGTextStroke);
            [_outlineColor set];
            [_text drawAtPoint:CGPointMake(i, i) withFont:_font];
        }
        
        //draw filling
        [_newColor set];
        
        CGContextSetTextDrawingMode(context, kCGTextFill);
        
        //if this is the last layer (first one we draw), add the drop shadow too and the outline
        if (i==0) {
            CGContextSetShadowWithColor(context, CGSizeMake(-2, -2), 4.0f, _shadowColor.CGColor);
        }
        else if (i+1!=_depth){
            //add glow like blur
            CGContextSetShadowWithColor(context, CGSizeMake(-1, -1), 3.0f, _newColor.CGColor);
        }
        
        [_text drawAtPoint:CGPointMake(i, i) withFont:_font];
        CGContextRestoreGState(context);
    }
    
    //if we need to apply the shine
    if (_shine) {
        //create an alpha mask from the top most layer of the image, so we can add a shine effect over it
        CGColorSpaceRef genericRGBColorspace = CGColorSpaceCreateDeviceRGB();
        CGContextRef imageContext = CGBitmapContextCreate(NULL, (int)expectedSize.width, (int)expectedSize.height, 8, (int)expectedSize.width * 4, genericRGBColorspace,  kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
        UIGraphicsPushContext(imageContext);
        CGContextSetTextDrawingMode(imageContext, kCGTextFill);
        [_text drawAtPoint:CGPointMake(_depth-1, _depth-1) withFont:_font];
        CGImageRef alphaMask = CGBitmapContextCreateImage(imageContext);
        CGContextRelease(imageContext);
        UIGraphicsPopContext();
        
        //draw shine effect
        //clip context to the mask we created
        CGRect drawRect = CGRectZero;
        drawRect.size = expectedSize;
        CGContextSaveGState(context);
        CGContextClipToMask(context, drawRect, alphaMask);
        
        CGContextSetBlendMode(context, kCGBlendModeLuminosity);
        
        size_t num_locations = 4;
        CGFloat locations[4] = { 0.0, 0.4, 0.6, 1};
        CGFloat gradientComponents[16] = {
            0.0, 0.0, 0.0, 1.0,
            0.6, 0.6, 0.6, 1.0,
            0.8, 0.8, 0.8, 1.0,
            0.0, 0.0, 0.0, 1.0
        };
        
        CGGradientRef glossGradient = CGGradientCreateWithColorComponents(genericRGBColorspace, gradientComponents, locations, num_locations);
        CGPoint start = CGPointMake(0, 0);
        CGPoint end = CGPointMake(0, expectedSize.height);
        CGContextDrawLinearGradient(context, glossGradient, start, end, 0);
        
        CGColorSpaceRelease(genericRGBColorspace);
        CGGradientRelease(glossGradient);
        CGImageRelease(alphaMask);
    }
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return finalImage;
}

#pragma mark Visual Effect view
-(void) addVisualEffectView
{
    // create effect
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    // add effect to an effect view
    effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    // effectView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    effectView.frame = self.view.frame;
    //effectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    effectView.alpha = 1;
    effectView.userInteractionEnabled = YES;
    // add the effect view to the image view
    //  [self.view insert
    NSArray *views =  self.view.subviews;
    [self.view insertSubview:effectView atIndex:views.count];
    
    //    UIImageView *ColorImage = [[UIImageView alloc]initWithFrame:CGRectMake(effectView.frame.size.width-40, 40, 40, 80)];
    //    ColorImage.image = [UIImage imageNamed:@""];
    
    //[effectView removeFromSuperview];
}

-(void) addTextView:(UIImageView *)ImageView
{
    isTextAdded = NO;
    
    view = [[UITextView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-60, self.view.frame.size.width, 50)];
    view.backgroundColor = [UIColor clearColor];
    view.delegate = self;
    [view setFont:[UIFont fontWithName:FontName size:50]];
    view.textColor = [self colorFromArray:ForeGroundColor];
    view.textAlignment = NSTextAlignmentCenter;
    view.tintColor = [UIColor yellowColor];
    view.autocorrectionType = UITextAutocorrectionTypeNo;
    //    view.backgroundColor = [UIColor lightGrayColor];
    [[effectView contentView] addSubview:view];
    
   // [view setScrollEnabled:NO];
    UIPinchGestureRecognizer *pinchGestRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scaleTextView:)];
    UITapGestureRecognizer *TapGestRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TextViewCancel:)];
    [effectView addGestureRecognizer:TapGestRecognizer];
    pinchGestRecognizer.delegate = self;
    [view addGestureRecognizer:pinchGestRecognizer];
    if (ImageView) {
        UIImage *Image = ImageView.image;
        NSLog(@"Img");
        for (NSDictionary *dict in TextViewValues) {
            UIImage *imageInDict = [dict valueForKey:@"Image"];
            if ([imageInDict isEqual:Image]) {
                // NSDictionary *dict = @{@"FontName":font,@"FontColor":color,@"Title":TextViewString,@"Image":img};
                NSLog(@"ImageFound");
                isTextAdded = YES;
                UIFont *font = [dict valueForKey:@"FontName"];
                UIColor *color = [dict valueForKey:@"FontColor"];
                NSString *Title = [dict valueForKey:@"Title"];
                [view setFont:[UIFont fontWithName:font.fontName size:50]];
                view.textColor = color;
                view.text = Title;
                indexOfDict = [TextViewValues indexOfObject:dict];
                break;
            }
        }
    }
    [view becomeFirstResponder];
    
}
#pragma mark Color From Array
-(UIColor *) colorFromArray:(NSArray *)array
{
    return [UIColor colorWithRed:[array[0] floatValue]/255.f green:[array[1] floatValue]/255.f blue:[array[2] floatValue]/255.f alpha:[array[3] floatValue]];
}

#pragma mark TextView Delegate Methods
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    //  textView.scrollEnabled = NO;
    [self updateFrame_TextView:textView endediting:NO];
    if (!colorPicker) {
        colorPicker = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"colormap"]];
        colorPicker.tag = 255;
        colorPicker.frame = CGRectMake(self.view.frame.size.width-40, textView.frame.origin.y-100, 20, 90);
        colorPicker.layer.cornerRadius = 10;
        colorPicker.layer.masksToBounds = YES;
        colorPicker.userInteractionEnabled = YES;
        
        UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(ChangeTextViewColor:)];
        
        [colorPicker addGestureRecognizer:panGestureRecognizer];
        [self.view insertSubview:colorPicker aboveSubview:textView];
    }
    
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString* parameterStr=@"";
    NSString *str = [textView text];
    NSLog(@"%@",text);
    if (text.length>0) {
        parameterStr=[str stringByAppendingString:text];
        [self updateFrame_TextView:textView endediting:NO];
    }
    //   int lines =  textView.contentSize.height/textView.font.lineHeight;
    NSLog(@"%f",textView.contentSize.height);
    //    else
    //    {
    //        NSString *newString = [str substringToIndex:[str length]-1];
    //        parameterStr=newString;
    //    }
    //
    //    UIImage *image = [self createText_String:parameterStr Index:(int)index];
    //    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, (self.view.frame.size.height-50)-image.size.height, image.size.width, image.size.height)];
    //    imageView.image = image;
    //    if (imageView.frame.size.width >=self.view.frame.size.width) {
    //     int height =  [self aspectratio:image.size Width:self.view.frame.size.width];
    //        imageView.frame = CGRectMake(imageView.frame.origin.x, imageView.frame.origin.y,  self.view.frame.size.width, image.size.height );
    //    }
    //    for (UIVisualEffectView *view in self.view.subviews) {
    //      if ([view isKindOfClass:[UIVisualEffectView class]]) {
    //            for (UIImageView *img in view.subviews) {
    //                if ([img isKindOfClass:[UIImageView class]]) {
    //                    [img removeFromSuperview];
    //                }
    //            }
    //            [view addSubview:imageView];
    //        }
    //    }
    //textView.scrollEnabled = YES;
    return YES;
}

-(void) updateFrame_TextView:(UITextView *)textView endediting:(BOOL)isEndEditing
{
    NSString *str = textView.text;
    if (isEndEditing) {
        if ([str hasSuffix:@"\n"]) {
            str = [str stringByReplacingCharactersInRange:NSMakeRange(str.length-3, 3) withString:@""];
        }
    }
    textView.text = str;
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    if (initialFrame.size.width == 0) {
        initialFrame = textView.frame;
    }
    CGRect newFrame;
    if (newSize.height != initialFrame.size.height) {
        int height = newSize.height-initialFrame.size.height;
        newFrame = CGRectMake(initialFrame.origin.x,initialFrame.origin.y-height-5, initialFrame.size.width, initialFrame.size.height+height);
    }

    NSLog(@"this is updating height%@",NSStringFromCGSize(newFrame.size));
//    [UIView animateWithDuration:0.2 animations:^{
//        textView.frame = newFrame;
//        colorPicker.frame = CGRectMake(self.view.frame.size.width-40, textView.frame.origin.y-100, 20, 90);
//    }];
    textView.frame = newFrame;
    colorPicker.frame = CGRectMake(self.view.frame.size.width-40, textView.frame.origin.y-100, 20, 90);
    [textView setContentOffset: CGPointMake(0,0) animated:NO];

}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length > 0){
        allowEndTextEditing = YES;
        [self createImageFromTextView];
    }else{
        [effectView removeFromSuperview];
        isTextImage = NO;
        allowEndTextEditing = NO;
        [colorPicker removeFromSuperview];
        colorPicker = nil;
    }
}

-(void) createImageFromTextView
{
    if (allowEndTextEditing) {
        //[self.btn_editText setHidden:YES];
        
       
        TextViewString = view.text;
        UIColor *color = view.textColor;
        UIFont *font = view.font;
        NSLog(@"End Editing... %@",TextViewString);
        // [self updateFrame_TextView:textView endediting:YES];
        UIImage *img = [self imageWithView:view];
        if (TextViewString.length>0) {
            imageView123 = [[UIImageView alloc]init]; //WithFrame: textImage.bounds];
            imageView123 =  [self addImageOnImage:img String:@""];
            [imageView123 setTag:101];
            //imageView.frame = CGRectMake(0, 200, 320, height);
            //
            if (isTextAdded){
                [imageView123 setCenter:isTextAddedCenterPoint];
            }else{
                imageView123.center = baseView.center;
            }
            //imageView123.backgroundColor = [UIColor redColor];
            //[image_View addSubview:imageView123];
            [baseView addSubview:imageView123];
            image_View.userInteractionEnabled = YES;
            imageView123.userInteractionEnabled = YES;
            NSDictionary *dict = @{@"FontName":font,@"FontColor":color,@"Title":TextViewString,@"Image":img};
            if (indexOfDict>=0) {
                [TextViewValues insertObject:dict atIndex:indexOfDict];
                indexOfDict = -1;
            }
            else
                [TextViewValues addObject:dict];
        }
        //[image_View addSubview:textView];
        // [textView removeFromSuperview];
        [effectView removeFromSuperview];
        isTextImage = NO;
        allowEndTextEditing = NO;
        [colorPicker removeFromSuperview];
        colorPicker = nil;
        
        //[self updateViewConstraints];
    }
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    [self updateFrame_TextView:textView endediting:NO];
}

- (void)scaleTextView:(UIPinchGestureRecognizer *)pinchGestRecognizer{
    CGFloat scale1 = pinchGestRecognizer.scale;
    UITextView *view = (UITextView *) pinchGestRecognizer.view;
    view.font = [UIFont fontWithName:FontName size:40*scale1];
    [self textViewDidChange:view];
}
- (void)TextViewCancel:(UITapGestureRecognizer *)GestRecognizer{
    NSLog(@"TextViewCancel tapped");
//    if (view.text.length > 0){
//        allowEndTextEditing = YES;
//        [self createImageFromTextView];
//    }else{
//        [effectView removeFromSuperview];
//        isTextImage = NO;
//        allowEndTextEditing = NO;
//        [colorPicker removeFromSuperview];
//        colorPicker = nil;
//    }
    [view resignFirstResponder];
}

#pragma mark Image Resizing Methods
-(UIImage *) resizeImage:(UIImage *)image ratio:(int)ratio
{
    CGSize targetSize =  [self setAspectRatio:image.size Percentage:ratio];
    UIImage *tempImage = nil;
    UIGraphicsBeginImageContext(targetSize);
    CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
    thumbnailRect.origin = CGPointMake(0.0,0.0);
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    [image drawInRect:thumbnailRect];
    tempImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tempImage;
}

-(CGSize) setAspectRatio:(CGSize )Size Percentage:(int)percentage
{
    CGSize size1 = Size;
    int deleteWidth = [self getPercentage:percentage Size:Size.width];
    int deleteHeight = [self getPercentage:percentage Size:Size.height];
    // (1200 / 1600) x 400 = 300
    int width = (size1.width/size1.height)*(size1.height-deleteHeight);
    int height = (size1.height/size1.width)*(size1.width-deleteWidth);
    size1 = CGSizeMake(width, height);
    return size1;
}

-(int) getPercentage:(int)percentage Size:(int)size1
{
    int result;
    return result = (size1*percentage)/100;
}


@end
