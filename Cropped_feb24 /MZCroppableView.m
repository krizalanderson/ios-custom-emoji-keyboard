//
//  MZCroppableView.m
//  MZCroppableView
//
//  Created by macbook on 30/10/2013.
//  Copyright (c) 2013 macbook. All rights reserved.
//

#import "MZCroppableView.h"
#import "UIBezierPath-MZPoints.h"
#import "DTLoupeView.h"
#import "CGPointExtension.h"

#define kEPSILON 1.0e-5

@implementation MZCroppableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        // Initialization code
    
    }
    return self;
}
- (id)initWithImageView:(UIImageView *)imageView
{
    //CGRect frame=CGRectMake(0, 0, imageView.image.size.width, imageView.image.size.height);
    self = [super initWithFrame:imageView.frame];
    if (self)
    {
        self.lineWidth = 5.0f;
        [self setBackgroundColor:[UIColor clearColor]];
        [self setClipsToBounds:YES];
        [self setMultipleTouchEnabled:NO];
        [self setUserInteractionEnabled:YES];
        self.croppingPath = [[UIBezierPath alloc] init];
        [self.croppingPath setLineWidth:self.lineWidth];
        self.croppingPath.miterLimit=-200;
        self.lineColor = [UIColor blackColor];
        pathArray=[[NSMutableArray alloc]init];
    }
    
    start = CGPointMake(-1,-1);
    i=0;
    return self;
}
#pragma mark - My Methods -
//+ (CGRect)scaleRespectAspectFromRect1:(CGRect)rect1 toRect2:(CGRect)rect2
//{
//    CGSize scaledSize = rect2.size;
//    
//    float scaleFactor = 1.0;
//    
//    CGFloat widthFactor  = rect2.size.width / rect1.size.width;
//    CGFloat heightFactor = rect2.size.height / rect1.size.width;
//    
//    if (widthFactor < heightFactor)
//        scaleFactor = widthFactor;
//    else
//        scaleFactor = heightFactor;
//    
//    scaledSize.height = rect1.size.height *scaleFactor;
//    scaledSize.width  = rect1.size.width  *scaleFactor;
//    
//    float y = (rect2.size.height - scaledSize.height)/2;
//       
//    return CGRectMake(0, y, scaledSize.width, scaledSize.height);
//}
+ (CGPoint)convertCGPoint:(CGPoint)point1 fromRect1:(CGSize)rect1 toRect2:(CGSize)rect2
{
    point1.y = rect1.height - point1.y;
    CGPoint result = CGPointMake((point1.x*rect2.width)/rect1.width, (point1.y*rect2.height)/rect1.height);
    return result;
}
+ (CGPoint)convertPoint:(CGPoint)point1 fromRect1:(CGSize)rect1 toRect2:(CGSize)rect2
{
    CGPoint result = CGPointMake((point1.x*rect2.width)/rect1.width, (point1.y*rect2.height)/rect1.height);
    return result;
}


- (void)drawRect:(CGRect)rect
{
    // Drawing code
//    [self.lineColor setStroke];
//    [self.croppingPath strokeWithBlendMode:kCGBlendModeNormal alpha:1.0f];
    
    [[UIColor blackColor] setStroke];
    [[UIColor blueColor] setFill];
    
    for (UIBezierPath *_path in pathArray) {
        //[_path fill];
        
        [_path strokeWithBlendMode:kCGBlendModeNormal alpha:1.0];
    }
    
}


- (UIImage *)deleteBackgroundOfImage:(UIImageView *)image
{
    NSLog(@"%@",self.croppingPath);
    NSArray *points = [self.croppingPath points];
    NSLog(@"Points count: %ld",points.count);
    if (points.count==0)
    {
        return Nil;
    }
    CGRect rect = CGRectZero;
    rect.size = image.image.size;
    
    UIBezierPath *aPath;
    {
        [[UIColor blackColor] setFill];
        UIRectFill(rect);
        [[UIColor whiteColor] setFill];
        UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0);
        aPath = [UIBezierPath bezierPath];
        
        // Set the starting point of the shape.
        NSLog(@"%@",[NSValue valueWithCGPoint:self.intersactPoint]);
        int tmpIndex = (int)[points indexOfObject:[NSValue valueWithCGPoint:self.intersactPoint]];
        
        if (tmpIndex<0)
            tmpIndex=0;
        
        CGPoint p1 = [MZCroppableView convertCGPoint:[[points objectAtIndex:tmpIndex] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
        [aPath moveToPoint:CGPointMake(p1.x, p1.y)];
        
        // without curve method
        
//        for (int j=tmpIndex; j<points.count; j++)
//        {
//            CGPoint p = [MZCroppableView convertCGPoint:[[points objectAtIndex:j] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
//            [aPath addLineToPoint:CGPointMake(p.x, p.y)];
//        }
        
        
        //curve method 1
    
//        float granularity = 100;
//
//        for (int index = tmpIndex + 1; index < points.count - 2 ; index++) {
//
//            CGPoint point0 = [MZCroppableView convertCGPoint:[[points objectAtIndex:index - 1] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
//            CGPoint point1 = [MZCroppableView convertCGPoint:[[points objectAtIndex:index] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
//            CGPoint point2 = [MZCroppableView convertCGPoint:[[points objectAtIndex:index + 1] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
//            CGPoint point3 = [MZCroppableView convertCGPoint:[[points objectAtIndex:index + 2] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
//
//            for (int i = 1; i < granularity ; i++) {
//                float t = (float) i * (1.0f / (float) granularity);
//                float tt = t * t;
//                float ttt = tt * t;
//
//                CGPoint pi;
//                pi.x = 0.5 * (2*point1.x+(point2.x-point0.x)*t + (2*point0.x-5*point1.x+4*point2.x-point3.x)*tt + (3*point1.x-point0.x-3*point2.x+point3.x)*ttt);
//                pi.y = 0.5 * (2*point1.y+(point2.y-point0.y)*t + (2*point0.y-5*point1.y+4*point2.y-point3.y)*tt + (3*point1.y-point0.y-3*point2.y+point3.y)*ttt);
//
////                if (pi.y > image.frame.size.height) {
////                    pi.y = image.frame.size.height;
////                }
////                else if (pi.y < 0){
////                    pi.y = 0;
////                }
//
//                if (pi.x > point0.x) {
//                    [aPath addLineToPoint:pi];
//                }
//            }
//
//            [aPath addLineToPoint:point2];
//        }
//        CGPoint tmp = [MZCroppableView convertCGPoint:[[points objectAtIndex:[points count] - 1] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
//        [aPath addLineToPoint:tmp];
        
         //curve method 2
        float alpha = 1.0;
        
        for (NSInteger ii=tmpIndex; ii < points.count - 2; ++ii) {
            
            CGPoint p0, p1, p2, p3;
            NSInteger nextii      = (ii+1)%[points count];
            NSInteger nextnextii  = (nextii+1)%[points count];
            NSInteger previi      = (ii-1 < 0 ? [points count]-1 : ii-1);
            
            p0 = [MZCroppableView convertCGPoint:[[points objectAtIndex:previi] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
            p1 = [MZCroppableView convertCGPoint:[[points objectAtIndex:ii] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
            p2 = [MZCroppableView convertCGPoint:[[points objectAtIndex:nextii] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
            p3 = [MZCroppableView convertCGPoint:[[points objectAtIndex:nextnextii] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
            
//            [pointsAsNSValues[ii] getValue:&p1];
//            [pointsAsNSValues[previi] getValue:&p0];
//            [pointsAsNSValues[nextii] getValue:&p2];
//            [pointsAsNSValues[nextnextii] getValue:&p3];
            
            CGFloat d1 = ccpLength(ccpSub(p1, p0));
            CGFloat d2 = ccpLength(ccpSub(p2, p1));
            CGFloat d3 = ccpLength(ccpSub(p3, p2));
            
            CGPoint b1, b2;
            if (fabs(d1) < kEPSILON) {
                b1 = p1;
            }
            else {
                b1 = ccpMult(p2, powf(d1, 2*alpha));
                b1 = ccpSub(b1, ccpMult(p0, powf(d2, 2*alpha)));
                b1 = ccpAdd(b1, ccpMult(p1,(2*powf(d1, 2*alpha) + 3*powf(d1, alpha)*powf(d2, alpha) + powf(d2, 2*alpha))));
                b1 = ccpMult(b1, 1.0 / (3*powf(d1, alpha)*(powf(d1, alpha)+powf(d2, alpha))));
            }
            
            if (fabs(d3) < kEPSILON) {
                b2 = p2;
            }
            else {
                b2 = ccpMult(p1, powf(d3, 2*alpha));
                b2 = ccpSub(b2, ccpMult(p3, powf(d2, 2*alpha)));
                b2 = ccpAdd(b2, ccpMult(p2,(2*powf(d3, 2*alpha) + 3*powf(d3, alpha)*powf(d2, alpha) + powf(d2, 2*alpha))));
                b2 = ccpMult(b2, 1.0 / (3*powf(d3, alpha)*(powf(d3, alpha)+powf(d2, alpha))));
            }
            
//            if (ii==startIndex)
//                [path moveToPoint:p1];
            
            [aPath addCurveToPoint:p2 controlPoint1:b1 controlPoint2:b2];
        }
        
         //curve method 3
        
//        NSInteger nCurves = [points count];
//        for (NSInteger ii=tmpIndex+1; ii < nCurves -2 ; ii++) {
//            NSValue *value  = points[ii];
//
//            //CGPoint curPt, prevPt, nextPt, endPt;
//
//            CGPoint curPt = [MZCroppableView convertCGPoint:[[points objectAtIndex:ii - 1] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
//            CGPoint prevPt = [MZCroppableView convertCGPoint:[[points objectAtIndex:ii] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
//            CGPoint nextPt = [MZCroppableView convertCGPoint:[[points objectAtIndex:ii + 1] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
//            CGPoint endPt = [MZCroppableView convertCGPoint:[[points objectAtIndex:ii + 2] CGPointValue] fromRect1:image.frame.size toRect2:image.image.size];
//
//
//           // [value getValue:&curPt];
////            if (ii == tmpIndex)
////                //[aPath moveToPoint:CGPointMake(p1.x, p1.y)];
////                continue;
//
//            NSInteger nextii = (ii+1)%[points count];
//            NSInteger previi = (ii-1 < 0 ? [points count]-1 : ii-1);
//
//            //[points[previi] getValue:&prevPt];
//            //[points[nextii] getValue:&nextPt];
//            endPt = nextPt;
//
//            CGFloat mx, my;
//            if ( ii > 0) {
//                mx = (nextPt.x - curPt.x)*0.5 + (curPt.x - prevPt.x)*0.5;
//                my = (nextPt.y - curPt.y)*0.5 + (curPt.y - prevPt.y)*0.5;
//            }
//            else {
//                mx = (nextPt.x - curPt.x)*0.5;
//                my = (nextPt.y - curPt.y)*0.5;
//            }
//
//            CGPoint ctrlPt1;
//            ctrlPt1.x = curPt.x + mx / 3.0;
//            ctrlPt1.y = curPt.y + my / 3.0;
//
//            //[points[nextii] getValue:&curPt];
//
//            nextii = (nextii+1)%[points count];
//            previi = ii;
//
//            //[points[previi] getValue:&prevPt];
//            //[points[nextii] getValue:&nextPt];
//
//            if (ii < nCurves - 1) {
//                mx = (nextPt.x - curPt.x)*0.5 + (curPt.x - prevPt.x)*0.5;
//                my = (nextPt.y - curPt.y)*0.5 + (curPt.y - prevPt.y)*0.5;
//            }
//            else {
//                mx = (curPt.x - prevPt.x)*0.5;
//                my = (curPt.y - prevPt.y)*0.5;
//            }
//
//            CGPoint ctrlPt2;
//            ctrlPt2.x = curPt.x - mx / 3.0;
//            ctrlPt2.y = curPt.y - my / 3.0;
//
//            [aPath addCurveToPoint:endPt controlPoint1:ctrlPt1 controlPoint2:ctrlPt2];
//        }
////
        [aPath closePath];
        [aPath fill];
    }
    
    UIImage *mask = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0);
    
    {
        CGContextClipToMask(UIGraphicsGetCurrentContext(), rect, mask.CGImage);
        [image.image drawAtPoint:CGPointZero];
    }
    
    UIImage *maskedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
   
    CGRect croppedRect = aPath.bounds;
    croppedRect.origin.y = rect.size.height - CGRectGetMaxY(aPath.bounds);//This because mask become inverse of the actual image;
    
    croppedRect.origin.x = croppedRect.origin.x*2;
    croppedRect.origin.y = croppedRect.origin.y*2;
    croppedRect.size.width = croppedRect.size.width*2;
    croppedRect.size.height = croppedRect.size.height*2;
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(maskedImage.CGImage, croppedRect);
    
    maskedImage = [UIImage imageWithCGImage:imageRef];
    
    // Release the imageRef to prevent a memory leak. CG classes don't use ARC
   // CGImageRelease(imageRef);
    imageRef = NULL;
   
    return maskedImage;
}
//-(UIImage *) drawBorder
//{
//UIBezierPath * path = [[UIBezierPath alloc] init];
//[path moveToPoint:CGPointMake(10.0, 10.0)];
//[path addLineToPoint:CGPointMake(290.0, 10.0)];
//[path setLineWidth:8.0];
//    
//CGFloat dashes[] = { path.lineWidth, path.lineWidth * 2 };
//[path setLineDash:dashes count:2 phase:0];
//[path setLineCapStyle:kCGLineCapRound];
//UIGraphicsBeginImageContextWithOptions(CGSizeMake(300, 20), false, 2);
//[path stroke];
//UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
//UIGraphicsEndImageContext();
//    return image;
//}


//-(UIImage *) addBorder:(UIImage *)image
//{
//
//    UIImage *_originalImage = image;// your image
//    
//    
//    
//    NSArray *aLine = [self.croppingPath points];
//    
//    CGRect rect = CGRectZero;
//    rect.size = image.size;
//    
//    
//    //UIBezierPath *aPath;
//    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0);
//
//    #define POINT(X) [[aLine objectAtIndex:X]CGPointValue]
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetStrokeColor(context, CGColorGetComponents([[UIColor blackColor] CGColor]));
//    for (int i = 0;i < (aLine.count-1);i++)
//    {
//        CGPoint pt1 = POINT(i);
//        CGPoint pt2 = POINT(i+1);
//        CGContextMoveToPoint(context,pt1.x,pt1.y);
//        CGContextAddLineToPoint(context,pt2.x,pt2.y);
//        CGContextStrokePath(context);
//    }
//    CGContextClosePath(context);
//    CGContextDrawPath(context, kCGPathStroke);
//    UIImage *_newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    
//    return _newImage;
//    
//    
//}
#pragma mark - Touch Methods -
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
//    UITouch *mytouch=[[touches allObjects] objectAtIndex:0];
//   // [self.croppingPath moveToPoint:[mytouch locationInView:self]];
//    
//    pointArr = [[NSMutableArray alloc] init];
//    if (start.x == -1 && start.y == -1)
//    {
//        start = [mytouch locationInView:self];
//        [self createRegion:start];
//        [self.delegate hideView];
//    }
//    
//     ctr = 0;
//     pts[0] = [mytouch locationInView:self];
//
//    DTLoupeView *loupe = [DTLoupeView sharedLoupe];
//    loupe.magnification = 1.5;
//    loupe.targetView = self;
//    loupe.touchPoint = [mytouch locationInView:self];
//   
//    [loupe presentLoupeFromLocation:[mytouch locationInView:self]];
    
    myPath = [[UIBezierPath alloc]init];
    lines = [[NSMutableArray alloc]init];
    myPath.lineWidth=4;
    pathArray=[[NSMutableArray alloc]init];
    
    UITouch *mytouch = [[event allTouches] anyObject];
    [myPath moveToPoint:[mytouch locationInView:mytouch.view]];
    
    if (start.x == -1 && start.y == -1)
    {
        start = [mytouch locationInView:self];
        [self createRegion:start];
        [self.delegate hideView];
    }

    [pathArray addObject:myPath];
}
- (void) createRegion:(CGPoint) point
{
    xAxis = [[NSMutableArray alloc]init];
    yAxis = [[NSMutableArray alloc]init];
    
    for (int j=-3; j<3; j++)
    {
        [xAxis addObject:[[NSNumber numberWithDouble:point.x+j] stringValue]];
        [yAxis addObject:[[NSNumber numberWithDouble:point.y+j] stringValue]];
        [xAxis addObject:[[NSNumber numberWithDouble:point.x+j+.5] stringValue]];
        [yAxis addObject:[[NSNumber numberWithDouble:point.y+j+.5] stringValue]];
        
    }
    
//    NSLog(@"X: %@",xAxis);
//    NSLog(@"Y: %@",yAxis);
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    
    UITouch *mytouch = [[event allTouches] anyObject];
    i++;
//    UITouch *mytouch=[[touches allObjects] objectAtIndex:0];
//    
//    if ([xAxis containsObject:[NSString stringWithFormat:@"%d",(int)[mytouch locationInView:self].x]])
//    {
//        NSLog(@"Same Point X");
//    }
//    
//    if ([yAxis containsObject:[NSString stringWithFormat:@"%d",(int)[mytouch locationInView:self].y]])
//    {
//        NSLog(@"Same Point Y");
//    }
//
    NSLog(@"pathArray count %lu",(unsigned long)pathArray.count);
    NSLog(@"myPath count %lu",(unsigned long)[myPath points].count);
    
    if ([yAxis containsObject:[NSString stringWithFormat:@"%d",(int)[mytouch locationInView:self].y]] && [xAxis containsObject:[NSString stringWithFormat:@"%d",(int)[mytouch locationInView:self].x]])
    {
        if ([myPath points].count < 10){
            return;
        }
        NSLog(@"Same Point both");
        NSLog(@"value of i --%d",i);
        
        if(i>8){
            
            
            [pathArray removeLastObject];
            [self.croppingPath setCGPath:myPath.CGPath];
            [myPath removeAllPoints];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"CutTheImage" object:nil];
            i = 0;
            return;
        }
    }
//
//    
//    if ([_croppingPath containsPoint:[mytouch locationInView:self]])
//    {
//        NSLog(@"Hurry");
//     
//    }
//
//    ctr++;
//    pts[ctr] = [mytouch locationInView:self];
//    
//    if (ctr == 4) // 4th point
//    {
//       
//        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0);
//        pts[0] = pts[3];
//        pts[1] = pts[4];
//        ctr = 1;
//    }
//    [self.croppingPath moveToPoint:pts[0]];
//    [self.croppingPath addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]];
//    [self setNeedsDisplay];
    
    if(myPath.isEmpty) {
        
    } else {
        
        
        [myPath addLineToPoint:[mytouch locationInView:mytouch.view]];
    
        [pathArray addObject:myPath];
        
        CGPoint pointA = [mytouch previousLocationInView:mytouch.view];
        CGPoint pointB = [mytouch locationInView:mytouch.view];
        
        line = [[Line alloc]init];
        [line setPointA:pointA];
        [line setPointB:pointB];
        
        [lines addObject:line];
        
        for(Line *l in lines) {
            
            CGPoint pa = l.pointA;
            CGPoint pb = l.pointB;
            
            //NSLog(@"Point A: %@", NSStringFromCGPoint(pa));
            //NSLog(@"Point B: %@", NSStringFromCGPoint(pb));
            
//            CGPoint tmp = [self intersection2:pointA u2:pointB v1:pa v2:pb];
//            NSLog(@"%@.....",[NSValue valueWithCGPoint:tmp]);
            
            if ([self checkLineIntersection:pointA :pointB :pa :pb])
            {
                if ([myPath points].count < 10){
                    return;
                }
                NSLog(@"%@",[NSValue valueWithCGPoint:[myPath currentPoint]]);
                self.intersactPoint = pb;
                [pathArray removeLastObject];
                [self.croppingPath setCGPath:myPath.CGPath];
                [myPath removeAllPoints];
                [self setNeedsDisplay];
                NSLog(@"Removed path!");
                [[NSNotificationCenter defaultCenter]postNotificationName:@"CutTheImage" object:nil];
                return;
            }
            
            
        }
    }
    [self setNeedsDisplay];

    
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *mytouch=[[touches allObjects] objectAtIndex:0];
    DTLoupeView *loupe = [DTLoupeView sharedLoupe];
    [loupe dismissLoupeTowardsLocation:[mytouch locationInView:self]];
    
    NSLog(@"pathArray count %lu",(unsigned long)pathArray.count);
    NSLog(@"myPath count %lu",(unsigned long)[myPath points].count);
    
    [pathArray removeAllObjects];
    [myPath removeAllPoints];
    
    [self setNeedsDisplay];
    NSLog(@"Removed path!");

}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}


-(void)checkPreviousPoint:(CGPoint)point1 {
    
    [pointArr addObject:[[NSNumber numberWithDouble:point1.x] stringValue]];
    [pointArr2 addObject:[[NSNumber numberWithDouble:point1.y] stringValue]];

}

-(CGPoint) intersection2:(CGPoint)u1 u2:(CGPoint)u2 v1:(CGPoint)v1 v2:(CGPoint)v2 {
    CGPoint ret=u1;
    double t=((u1.x-v1.x)*(v1.y-v2.y)-(u1.y-v1.y)*(v1.x-v2.x))
    /((u1.x-u2.x)*(v1.y-v2.y)-(u1.y-u2.y)*(v1.x-v2.x));
    ret.x+=(u2.x-u1.x)*t;
    ret.y+=(u2.y-u1.y)*t;
    return ret;
}

- (NSValue *)intersectionOfLineFrom:(CGPoint)p1 to:(CGPoint)p2 withLineFrom:(CGPoint)p3 to:(CGPoint)p4
{
    CGFloat d = (p2.x - p1.x)*(p4.y - p3.y) - (p2.y - p1.y)*(p4.x - p3.x);
    if (d == 0)
        return nil; // parallel lines
    CGFloat u = ((p3.x - p1.x)*(p4.y - p3.y) - (p3.y - p1.y)*(p4.x - p3.x))/d;
    CGFloat v = ((p3.x - p1.x)*(p2.y - p1.y) - (p3.y - p1.y)*(p2.x - p1.x))/d;
    if (u < 0.0 || u > 1.0)
        return nil; // intersection point not between p1 and p2
    if (v < 0.0 || v > 1.0)
        return nil; // intersection point not between p3 and p4
    CGPoint intersection;
    intersection.x = p1.x + u * (p2.x - p1.x);
    intersection.y = p1.y + u * (p2.y - p1.y);
    
    return [NSValue valueWithCGPoint:intersection];
}

-(BOOL)checkLineIntersection:(CGPoint)p1 :(CGPoint)p2 :(CGPoint)p3 :(CGPoint)p4
{
    //CGFloat denominator = (p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y);
    
    /*
     // In this case the lines are parallel so you assume they don't intersect
     if (denominator == 0.0f)
     return NO;
     */
    //    if (denominator == 0.0f) return NO;
    //
    //    CGFloat ua = ((p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x)) / denominator;
    //    CGFloat ub = ((p2.x - p1.x) * (p1.y - p3.y) - (p2.y - p1.y) * (p1.x - p3.x)) / denominator;
    //
    //    if (ua > 0.0 && ua <= 1.0 && ub > 0.0 && ub <= 1.0)
    //    {
    //        return YES;
    //    }
    //
    //    return NO;
    
    CGFloat denominator = (p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y);
    CGFloat ua = (p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x);
    CGFloat ub = (p2.x - p1.x) * (p1.y - p3.y) - (p2.y - p1.y) * (p1.x - p3.x);
    if (denominator < 0) {
        ua = -ua; ub = -ub; denominator = -denominator;
    }
    return (ua > 0.0 && ua <= denominator && ub > 0.0 && ub <= denominator);
}

#pragma mark Color of a pixel
- (UIColor*)pixelColorInImage:(UIImage*)image atX:(int)x atY:(int)y {
    
    CFDataRef pixelData = CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage));
    const UInt8* data = CFDataGetBytePtr(pixelData);
    
    int pixelInfo = ((image.size.width * y) + x ) * 4; // 4 bytes per pixel
    
    UInt8 red   = data[pixelInfo + 0];
    UInt8 green = data[pixelInfo + 1];
    UInt8 blue  = data[pixelInfo + 2];
    UInt8 alpha = data[pixelInfo + 3];
    CFRelease(pixelData);
    
    return [UIColor colorWithRed:red  /255.0f
                           green:green/255.0f
                            blue:blue /255.0f
                           alpha:alpha/255.0f];
}


- (void)drawBitmap
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 0.0);
    [[UIColor blackColor] setStroke];
   // if (!incrementalImage) // first time; paint background white
    {
        UIBezierPath *rectpath = [UIBezierPath bezierPathWithRect:self.bounds];
        [[UIColor whiteColor] setFill];
        [rectpath fill];
    }
   // [incrementalImage drawAtPoint:CGPointZero];
    [self.croppingPath stroke];
  
    UIGraphicsEndImageContext();
}
@end
