//
//  KeyboardViewController.m
//  PJKeyboard
//
//  Created by webastral on 22/12/16.

//

#import "KeyboardViewController.h"

@interface KeyboardViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSMutableArray *images;
   
    UIImageView *imgview;
    UILabel *stickerlbl;
    UILabel *stickerlbl_background;
    NSIndexPath *myIndexPath;
    UICollectionView *myCollectionView;
    NSMutableArray *indexNumber;
    
    BOOL isclicked;
    UIPasteboard *pasteboard;
    }
@property (nonatomic, strong) UIButton *nextKeyboardButton;

@end

@implementation KeyboardViewController
- (void)updateViewConstraints
{
    [super updateViewConstraints];
    // Add custom view sizing constraints here
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addCollectionView];
    [self addBottomView];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    indexNumber = [[NSMutableArray alloc] init];
    myIndexPath = [[NSIndexPath alloc] init];
    // Perform custom UI setup here
//    self.nextKeyboardButton = [UIButton buttonWithType:UIButtonTypeSystem];
//    
////    [self.nextKeyboardButton setTitle:NSLocalizedString(@"Next Keyboard", @"Title for 'Next Keyboard' button") forState:UIControlStateNormal];
//     [self.nextKeyboardButton setImage:[UIImage imageNamed:@"Globe3.png"] forState:UIControlStateNormal];
//    
//    [self.nextKeyboardButton sizeToFit];
//    self.nextKeyboardButton.translatesAutoresizingMaskIntoConstraints = NO;
//    
//    [self.nextKeyboardButton addTarget:self action:@selector(advanceToNextInputMode) forControlEvents:UIControlEventTouchUpInside];
//    
//    [self.view addSubview:self.nextKeyboardButton];
//    
//    [self.nextKeyboardButton.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
//    [self.nextKeyboardButton.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated
}

- (void)textWillChange:(id<UITextInput>)textInput
{
    // The app is about to change the document's contents. Perform any preparation here.
}

- (void)textDidChange:(id<UITextInput>)textInput
{
    // The app has just changed the document's contents, the document context has been updated.
    
    UIColor *textColor = nil;
    if (self.textDocumentProxy.keyboardAppearance == UIKeyboardAppearanceDark)
    {
        textColor = [UIColor whiteColor];
    } else
    {
        textColor = [UIColor blackColor];
    }
    [self.nextKeyboardButton setTitleColor:textColor forState:UIControlStateNormal];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    //[self addBottomView];
}
#pragma mark Add View To keyboard from here
-(void)viewDidAppear:(BOOL)animated
{
    [self addCollectionView];
    [self addBottomView];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self getData];
        [myCollectionView reloadData];
    });
}

#pragma mark Add Bottom View
-(void) addBottomView
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-30, self.view.frame.size.width, 30)];
    view.backgroundColor = [UIColor clearColor];
    //[self.view addSubview:view];
    [self.view insertSubview:view aboveSubview:myCollectionView];
    self.nextKeyboardButton = [[UIButton alloc]init];
    
    self.nextKeyboardButton.frame = CGRectMake(0,0, 60, view.frame.size.height);
    
//    CGPoint c_point =  self.nextKeyboardButton.center;
//    c_point.y = view.center.y;
//    self.nextKeyboardButton.center = c_point;
    
  // [self.nextKeyboardButton setImage:[UIImage imageNamed:@"worldwide_2"] forState:UIControlStateNormal];
   // [self.nextKeyboardButton.im];
    [self.nextKeyboardButton addTarget:self action:@selector(deleteCopiedImage) forControlEvents:UIControlEventTouchUpInside];
    //[UIPasteboard generalPasteboard].image = nil;
    
//    UITapGestureRecognizer *TapGestRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteCopiedImage)];
//    [self.nextKeyboardButton addGestureRecognizer:TapGestRecognizer];
    UIImageView *imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(20.0, 0.0, 25, 25)];
    imgView1.image = [UIImage imageNamed:@"worldwide_2_white"];
    [imgView1 setContentMode:UIViewContentModeScaleAspectFit];
    [imgView1 setUserInteractionEnabled:NO];
    [view addSubview:imgView1];
   [view addSubview:self.nextKeyboardButton];
    
    
    UIButton * backspace = [[UIButton alloc]init];
    backspace.frame = CGRectMake( view.frame.size.width -60, 0, 60, 30);
    //[backspace setImage:[UIImage imageNamed:@"left-arrow_2"] forState:UIControlStateNormal];
    [backspace addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIImageView *imgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width -40, 0.0, 25, 25)];
   
    imgView2.image = [UIImage imageNamed:@"left-arrow_3_white"];
    [imgView2 setContentMode:UIViewContentModeScaleAspectFit];
    [imgView2 setUserInteractionEnabled:NO];
    [view addSubview:imgView2];
    
    
    [view addSubview:backspace];
    
    
    
}

-(void)deleteCopiedImage
{
    [self delete];
    //[self delete];
    //[self delete];
    [pasteboard setString:@""];
    [self performSelectorOnMainThread:@selector(advanceToNextInputMode) withObject:nil waitUntilDone:NO];
    
    //[UIPasteboard generalPasteboard].image = SelectedImage;
    //NSData *imagedata = UIImagePNGRepresentation(SelectedImage);
    //pasteboard = [UIPasteboard generalPasteboard];
    //[pasteboard setImage:nil];
    
    //[pasteboard setData:imagedata forPasteboardType:@"public.jpeg"];
}


-(void)delete
{
   [self.textDocumentProxy deleteBackward];
    
}

#pragma mark Add Collection View
-(void) addCollectionView
{
    int cellSize = (self.view.frame.size.width-50)/3;
    UICollectionViewFlowLayout *Layout = [[UICollectionViewFlowLayout alloc]init];
    Layout.itemSize=CGSizeMake(cellSize, cellSize);
    //myCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50) collectionViewLayout:Layout];
    myCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) collectionViewLayout:Layout];
    [myCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    myCollectionView.backgroundColor = [UIColor clearColor];
    myCollectionView.dataSource = self;
    myCollectionView.delegate = self;
    myCollectionView.allowsMultipleSelection = NO;
    [self.view addSubview:myCollectionView];
}

-(void) getData
{
    images = [[NSMutableArray alloc]init];
    NSURL *groupContainerURL = [[NSFileManager defaultManager]containerURLForSecurityApplicationGroupIdentifier:@"group.com.summerinc.MeBoard"];
    NSString *url = [groupContainerURL absoluteString];
    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:[groupContainerURL path]  error:nil];
    NSLog(@"Paths:%@",filePathsArray);
    
    for (NSString *string in filePathsArray)
    {
        if ([string hasSuffix:@".png"])
        {
            NSString *path = [url stringByAppendingPathComponent:string];
            NSData *imageData = [NSData dataWithContentsOfFile:[NSURL URLWithString: path]];
            UIImage *image = [UIImage imageWithData:imageData];
            
             [images addObject:image];
        }
        
    }
}




#pragma mark Collection View Data Source Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return images.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    // remove overlapping of imageviews
    
    
    
    
    
//    for (UIImageView *view in cell.contentView.subviews)
//    {
//        if ([view isKindOfClass:[UIImageView class]])
//        {
//            [view removeFromSuperview];
//        }
//    }
    
    imgview = [[UIImageView alloc]init] ;
    stickerlbl = [[UILabel alloc]init];
    stickerlbl_background = [[UILabel alloc]init];
    
     UIImage *img = [[UIImage alloc]init];
    img = [images objectAtIndex:indexPath.row];
    
//    for(int i =0; i<[images count];i++)
//    {
//        [indexNumber addObject:@"0"];
//
//
//    }
    
    imgview.clipsToBounds = YES;
   //imgview.contentMode = UIViewContentModeScaleToFill;
    imgview.backgroundColor = [UIColor whiteColor];
   //imgview.contentMode = UIViewContentModeCenter;
    
    [imgview setContentMode:UIViewContentModeScaleAspectFit];
    
    imgview.image = img;
    imgview.frame = cell.bounds;
    
    stickerlbl.frame = CGRectMake(imgview.frame.origin.x, imgview.frame.size.height - 20, imgview.frame.size.width, 20);
//    stickerlbl_background.frame = CGRectMake(imgview.frame.origin.x, imgview.frame.size.height - 32, imgview.frame.size.width, 32);
    
    stickerlbl.text = @"Copied, now Paste!";
    [stickerlbl setFont:[UIFont fontWithName:@"Montserrat-Regular" size:9]];
    [stickerlbl setTextAlignment:NSTextAlignmentCenter];
    stickerlbl.textColor = [UIColor whiteColor];
    //[stickerlbl setNumberOfLines:0];
    
    //[stickerlbl sizeToFit];
//    CGPoint tmpCenter = stickerlbl.center;
//    tmpCenter.x = imgview.center.x;
//    [stickerlbl setCenter:tmpCenter];
    
    stickerlbl.backgroundColor = [UIColor colorWithRed:0.78 green:0.88 blue:0.90 alpha:1.0];//[UIColor colorWithRed:0 green:178 blue:254 alpha:1];
    //stickerlbl_background.backgroundColor = [UIColor colorWithRed:0 green:178 blue:254 alpha:1];
    
    stickerlbl.hidden = YES;
    //stickerlbl_background.hidden = YES;
    
    //[imgview addSubview:stickerlbl_background];
    [imgview addSubview:stickerlbl];
    [imgview bringSubviewToFront:stickerlbl];
    
    [cell addSubview:imgview];
    
    if ([indexNumber containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
        myIndexPath = indexPath;
        cell.selected = YES;
        stickerlbl.hidden = NO;
        //stickerlbl_background.hidden = NO;
        
        imgview.backgroundColor = [UIColor whiteColor];
    }else{
        
    }
    
//    if([[indexNumber objectAtIndex:indexPath.row] intValue] == 1)
//    {
//        myIndexPath = indexPath;
//        cell.selected = YES;
//        stickerlbl.hidden = NO;
//        imgview.backgroundColor = [UIColor blackColor];
//    }

    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Selected");
    [collectionView deselectItemAtIndexPath:indexPath animated:true];
    
    imgview.image = [images objectAtIndex:indexPath.row];
    UIImage *SelectedImage = [images objectAtIndex:indexPath.row];


    [UIPasteboard generalPasteboard].image = SelectedImage;
    NSData *imagedata = UIImagePNGRepresentation(SelectedImage);
    pasteboard = [UIPasteboard generalPasteboard];
  
    [pasteboard setData:imagedata forPasteboardType:@"public.jpeg"];

    [indexNumber removeAllObjects];
    [indexNumber addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
//    if([[indexNumber objectAtIndex:indexPath.row] intValue] == 0)
//    {
//
//        [indexNumber replaceObjectAtIndex:indexPath.row withObject:@"1"];
//        imgview.image = [images objectAtIndex:indexPath.row];
//
//        myIndexPath = indexPath;
//        stickerlbl.hidden = NO;
//        imgview.backgroundColor = [UIColor blackColor];
//        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//        cell.selected = YES;
//        [cell addSubview:imgview];
//
//    }
    [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    [myCollectionView reloadData];
    
 
}

//-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    [myCollectionView reloadData];
//    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:myIndexPath];
//
//    if([[indexNumber objectAtIndex:indexPath.row] intValue] == 1)
//    {
//        [indexNumber replaceObjectAtIndex:myIndexPath.row withObject:@"0"];
//        imgview.backgroundColor = [UIColor clearColor];
//        [stickerlbl setHidden:YES];
//    }
//
//
//    cell.selected = NO;
//    [cell addSubview:imgview];
//}

- (void)showMessage:(NSString*)message atPoint:(CGPoint)point
{
    const CGFloat fontSize = 18;  // Or whatever.
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:fontSize];  // Or whatever.
    label.text = message;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    [label sizeToFit];
    
   // label.center = point;
    
    [self.view addSubview:label];
    
    [UIView animateWithDuration:0.8 delay:1 options:0 animations:^{
        label.alpha = 0;
    } completion:^(BOOL finished)
    {
        label.hidden = YES;
        [label removeFromSuperview];
    }];
}

@end
