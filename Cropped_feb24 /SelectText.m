

#define Rgb2UIColor(r, g, b, a)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:a]

#define floatToNumberFloat(a) [NSNumber numberWithFloat:a];

#import "SelectText.h"
#import "UIImage+ThreeD.h"


@interface SelectText ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate>

{
    UIImage *my3dImage;
    NSMutableArray *fonts,*textFormats,*textImages;
    NSArray *colorsArray;
    NSDictionary *selectedTextStyle;
    int selectedIndex;
    MZAppDelegate *delegate;
    BOOL isStateEnd;
    CGRect initialFrame;
    float oldX, oldY, diffrence;
    BOOL dragging;
    int viewBound;
    UIVisualEffectView *effectView;
    float alpha,maxAlpha;
    NSArray * familyNames;
}
@end

@implementation SelectText

- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    familyNames = [NSArray arrayWithObjects:@"AgentOrange",@"BroadcastTitling",@"Montague",@"Wedgie Regular",@"Cantate Beveled",@"From Cartoon Blocks",@"National Cartoon",@"orange juice 2.0",@"planetbe",@"ALBA____",@"ALBAM___",@"ALBAS___",@"STONB___", nil];

     familyNames = [NSArray arrayWithObjects:@"From Cartoon Blocks",@"National Cartoon",@"orange juice 2.0",@"planetbe",@"ALBA____",@"ALBAM___",@"ALBAS___",@"STONB___", nil];
    
    
    UIImageView *image = [[UIImageView alloc]initWithImage:self.backgroundImage];
    image.frame = self.view.frame;
    //image.alpha = 0.5;
    [self.view insertSubview:image atIndex:0];
    
    viewBound = self.view.frame.size.height - self.view.frame.size.height/4;
    // create effect
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    // add effect to an effect view
    effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    effectView.frame = self.view.frame;
    effectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    maxAlpha = 0.8;
    effectView.alpha = maxAlpha;
    // add the effect view to the image view
    [image addSubview:effectView];
    _TextFieldString.layer.borderWidth = 1;
    _TextFieldString.layer.borderColor = [[UIColor colorWithRed:0.0/255.0 green:90.0/255.0 blue:190.0/255.0 alpha:1]CGColor];
    _CollectionView.delegate = self;
    _CollectionView.dataSource = self;
    selectedIndex = 0;
    _TextFieldString.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapMethod:)];
    [_ImageView addGestureRecognizer:tap];
    _ImageView.userInteractionEnabled = YES;
    _darkView.hidden = NO;
    delegate = (MZAppDelegate *)[[UIApplication sharedApplication]delegate];
    textFormats = [[NSMutableArray alloc]initWithArray:delegate.textFormats];
   // textImages = [[NSMutableArray alloc]initWithArray:delegate.textImages];
    
    _ImageView.image = [self createImageTextFromData:[textFormats objectAtIndex:0] indexValue:0 isForCell:NO];
    [_ImageView sizeToFit];
    
    int cellSize = (UIScreen.mainScreen.bounds.size.width-26)/3;
    UICollectionViewFlowLayout *Layout = [[UICollectionViewFlowLayout alloc]init];
    //Layout.itemSize=CGSizeMake(cellSize, cellSize);
    [self.Layout setMinimumLineSpacing:5.0];
    [self.Layout setMinimumInteritemSpacing:5.0];
    [self.Layout setItemSize:CGSizeMake(cellSize, cellSize)];
    [_CollectionView setCollectionViewLayout:self.Layout];
    
    [_CollectionView setShowsHorizontalScrollIndicator:NO];
    [_CollectionView setShowsVerticalScrollIndicator:NO];
    _CollectionView.backgroundColor = [UIColor clearColor];
    _CollectionView.userInteractionEnabled = YES;
    [_CollectionView setScrollEnabled:YES];
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveViewWithGestureRecognizer:)];
    UITapGestureRecognizer *tapOnCollection = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapMethodForCollectionView:)];
    tapOnCollection.delegate = self;
    
    //[_CollectionView addGestureRecognizer:tapOnCollection];
    [_CollectionView addGestureRecognizer:panGestureRecognizer];
    
//    [self.view addGestureRecognizer:tapOnCollection];
//    [self.view addGestureRecognizer:panGestureRecognizer];
//
    initialFrame = _CollectionView.frame;
    
    diffrence = 0;
    alpha = 0;
}

-(void) tapMethod:(UITapGestureRecognizer *)recogniser
{
    NSLog(@"Image  Tapped");
}

-(void) setupInitialView
{
   if(!delegate.textFormats)
   {
    textFormats = [[NSMutableArray alloc]init];
    textImages = [[NSMutableArray alloc]init];
    for (int i=0; i<fonts.count; i++)
    {
        NSString *FontName = [fonts objectAtIndex:i];
        NSArray *ForeGroundColor = [colorsArray objectAtIndex:[self randomIntBetween:0 and:8]];
        NSArray *ShadowColor = [colorsArray objectAtIndex:[self randomIntBetween:0 and:8]];
        NSArray *OutlineColor = [colorsArray objectAtIndex:[self randomIntBetween:0 and:8]];
        int depth = (int)[self randomIntBetween:1 and:20];
        NSDictionary *dict = @{@"FontName":FontName, @"ForeGroundColor":ForeGroundColor,@"ShadowColor":ShadowColor,@"OutlineColor":OutlineColor,@"depth":[NSNumber numberWithInt:depth]};
        [textFormats addObject:dict];
       
        
        UIImage *image =
        [self create3DImageWithText:@"ABC" Font:[UIFont fontWithName:FontName size:180] ForegroundColor:[UIColor colorWithRed:[ForeGroundColor[0] floatValue]/255.f green:[ForeGroundColor[1] floatValue]/255.f blue:[ForeGroundColor[2] floatValue]/255.f alpha:[ForeGroundColor[3] floatValue]] ShadowColor:[self colorFromArray:ShadowColor] outlineColor:[self colorFromArray:OutlineColor] depth:(int)depth useShine:NO];
        UIImage *image3 = [self createImageTextFromData:dict indexValue:i isForCell:NO];
        UIImage *image2 = [self createImageTextFromData:dict indexValue:i isForCell:YES];
        // Tap to style Title
        [textImages addObject:image2];
    }
     //  [MZAppDelegate HideIndicator:self.view];
       delegate.textFormats = [textFormats mutableCopy];
       delegate.textImages = [textImages mutableCopy];
   }
    else
    {
        textFormats = delegate.textFormats;
        textImages = delegate.textImages;
    }
    
}

#pragma mark Get System Fonts
-(void) getSystemFonts
{
    fonts = [[NSMutableArray alloc]init];
    for (id familyName in familyNames)
    {
    @try
        
        {
        NSLog(@"Font is %@",familyName);
        NSString *font = familyName;
            if (fonts.count<30)
            {
                [fonts addObject:font];
            }
        else
        {
            break;
        }
        } @catch (NSException *exception)
        {
           // NSLog(@"Error");
        }
    }
  }

#pragma mark array from colors
-(void) colorArray
{
    NSArray *Red = [self arrayFromRGBAValues_Red:255 Green:0 Blue:0 Alpha:1];
    NSArray *Black = [self arrayFromRGBAValues_Red:0 Green:0 Blue:0 Alpha:0.5];
    NSArray *Green = [self arrayFromRGBAValues_Red:0 Green:128 Blue:0 Alpha:1];
    NSArray *Blue = [self arrayFromRGBAValues_Red:0 Green:0 Blue:255 Alpha:0.8];
    NSArray *Yellow = [self arrayFromRGBAValues_Red:255 Green:255 Blue:0 Alpha:0.4];
    NSArray *Orange = [self arrayFromRGBAValues_Red:255 Green:165 Blue:0 Alpha:0.6];
    NSArray *Brown = [self arrayFromRGBAValues_Red:165 Green:45 Blue:45 Alpha:1];
    NSArray *Gray = [self arrayFromRGBAValues_Red:128 Green:128 Blue:128 Alpha:0.6];
    NSArray *white = [self arrayFromRGBAValues_Red:255 Green:255 Blue:255 Alpha:1];
    colorsArray = @[Red,Black,Green,Blue,Yellow,Orange,Brown,Gray,white];
}

#pragma mark Color From Array
-(UIColor *) colorFromArray:(NSArray *)array
{
    return [UIColor colorWithRed:[array[0] floatValue]/255.f green:[array[1] floatValue]/255.f blue:[array[2] floatValue]/255.f alpha:[array[3] floatValue]];
}

#pragma mark Array from RGB Values
-(NSArray *) arrayFromRGBAValues_Red:(float)red Green:(float)green Blue:(float)blue Alpha:(float)alpha
{
    NSArray *arr = @[[NSNumber numberWithFloat:red],[NSNumber numberWithFloat:green],[NSNumber numberWithFloat:blue],[NSNumber numberWithFloat:alpha]];
    return arr;
}

+ (NSArray *) arrayFromRGBAValues_Red:(float)red Green:(float)green Blue:(float)blue Alpha:(float)alpha
{
    NSArray *arr = @[[NSNumber numberWithFloat:red],[NSNumber numberWithFloat:green],[NSNumber numberWithFloat:blue],[NSNumber numberWithFloat:alpha]];
    return arr;
}

#pragma mark Random Number Generator
-(NSInteger)randomIntBetween:(NSInteger)min and:(NSInteger)max
{
    return (NSInteger)(min + arc4random_uniform(max + 1 - min));
}

#pragma mark Done Action
- (IBAction)doneTextSelection:(UIButton *)sender
{
    NSLog(@"Done Button Tapped");
    NSString *str = _TextFieldString.text;
    if (str.length == 0)
    {
        //str = @"ABC";
        [self backToPreviousViewController:nil];
        return;
    }
    UIImage *image  = [self imagefromImageView:_ImageView];
    NSDictionary *dict = @{@"Image":image,@"String":str};
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TextImage"
     object:dict];
    
   // [screen addImageOnImage:_ImageView.image];
    [self backToPreviousViewController:nil];
}
#pragma mark Back Action
- (IBAction)backToPreviousViewController:(UIButton *)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark Collection View Delegate Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return textFormats.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UILabel *label in cell.contentView.subviews)
    {
        if ([label isKindOfClass:[UILabel class]])
        {
            [label removeFromSuperview];
        }
    }
    
    NSString *FontName = [[textFormats objectAtIndex:indexPath.row] valueForKey:@"FontName"];
    NSArray *ForeGroundColor = [[textFormats objectAtIndex:indexPath.row] valueForKey:@"ForeGroundColor"];
    UIColor *ShadowColor = [self colorFromArray:[[textFormats objectAtIndex:indexPath.row] valueForKey:@"ShadowColor"]];
    UILabel *label = [self newLabel_color:[UIColor colorWithRed:[ForeGroundColor[0] floatValue]/255.f green:[ForeGroundColor[1] floatValue]/255.f blue:[ForeGroundColor[2] floatValue]/255.f alpha:[ForeGroundColor[3] floatValue]] shadowColor:ShadowColor FontStyle:FontName];
    UILabel *labelSimple = [[UILabel alloc]initWithFrame:cell.bounds];
    labelSimple.font = [UIFont fontWithName:FontName size:33];
    labelSimple.text = @"ABC";
    labelSimple.textColor = [UIColor colorWithRed:[ForeGroundColor[0] floatValue]/255.f green:[ForeGroundColor[1] floatValue]/255.f blue:[ForeGroundColor[2] floatValue]/255.f alpha:[ForeGroundColor[3] floatValue]];
    [cell.contentView addSubview:labelSimple];
    [labelSimple setTextAlignment:NSTextAlignmentCenter];
    return cell;

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    NSLog(@"Did Select");
    [self selectedCell:indexPath];
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    int width = collectionView.frame.size.width/4;
//    return CGSizeMake(width, width);
//}

#pragma mark Text Field Delegate Methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *abc = [textField.text stringByReplacingCharactersInRange:range withString:string];
   // NSLog(@"%@",abc);
    if(abc)
    [self createText_String:abc Index:selectedIndex];
    return YES;
}


#pragma mark CreateText
-(void) createText_String:(NSString *)string Index:(int)index
{
    NSDictionary *dictionary = [textFormats objectAtIndex:index];
    NSString *FontName = [dictionary valueForKey:@"FontName"];
    NSLog(@"===================== Selected font name ===============%@",FontName);
    NSArray *ForeGroundColor = [dictionary valueForKey:@"ForeGroundColor"];
    NSArray *ShadowColor = [dictionary valueForKey:@"ShadowColor"];
    NSArray *OutlineColor = [dictionary valueForKey:@"OutlineColor"];
    int depth =[[dictionary valueForKey:@"depth"] intValue];

    UIImage *image =
    [self create3DImageWithText:string Font:[UIFont fontWithName:FontName size:100] ForegroundColor:[UIColor colorWithRed:[ForeGroundColor[0] floatValue]/255.f green:[ForeGroundColor[1] floatValue]/255.f blue:[ForeGroundColor[2] floatValue]/255.f alpha:[ForeGroundColor[3] floatValue]] ShadowColor:[self colorFromArray:ShadowColor] outlineColor:[self colorFromArray:OutlineColor] depth:(int)depth useShine:YES];
    
    _ImageView.image = image;
    NSDictionary *dict = @{@"Index":[NSNumber numberWithInteger:index],@"String":@"ABC"};
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TextImage"
     object:dict];
    [self backToPreviousViewController:nil];

}

-(UIImage *) createImageTextFromData:(NSDictionary *)dict indexValue:(int)index isForCell:(BOOL)cell
{
    int depth  = [[dict valueForKey:@"depth"] intValue];
    int size = 0;
    BOOL shine;
//    if (cell) {
//        size = 40; // for collection view
//        shine = NO;
//        depth = 1;
//    }
//    else
//    {
        size = 180; // For Image View

    NSString *FontName = [dict valueForKey:@"FontName"];
    NSArray *ForeGroundColor = [dict valueForKey:@"ForeGroundColor"];
    NSArray *OutlineColor = [dict valueForKey:@"OutlineColor"];
    NSArray *ShadowColor = [dict valueForKey:@"ShadowColor"];
    
    UIImage *image =
    [self create3DImageWithText:@"ABC" Font:[UIFont fontWithName:FontName size:size] ForegroundColor:[UIColor colorWithRed:[ForeGroundColor[0] floatValue]/255.f green:[ForeGroundColor[1] floatValue]/255.f blue:[ForeGroundColor[2] floatValue]/255.f alpha:[ForeGroundColor[3] floatValue]] ShadowColor:[self colorFromArray:ShadowColor] outlineColor:[self colorFromArray:OutlineColor] depth:depth useShine:shine];
    
    if (cell) {
        // Resize cell image
        return  [self resizeImage:image];
    }
    return image;
}

#pragma mark 3D Image Text Method
- (UIImage *)create3DImageWithText:(NSString *)_text Font:(UIFont*)_font ForegroundColor:(UIColor*)_foregroundColor ShadowColor:(UIColor*)_shadowColor outlineColor:(UIColor*)_outlineColor depth:(int)_depth useShine:(BOOL)_shine {
    //calculate the size we will need for our text
    CGSize expectedSize = [_text sizeWithFont:_font constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
    
    
    //  CGSize expectedSize = CGSizeMake(70.0, 070.0);
    //increase our size, as we will draw in 3d, so we need extra space for 3d depth + shadow with blur
    expectedSize.height+=_depth+5;
    expectedSize.width+=_depth+5;
    
    UIColor *_newColor;
    
    UIGraphicsBeginImageContextWithOptions(expectedSize, NO, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //because we want to do a 3d depth effect, we are going to slightly decrease the color as we move back
    //so here we are going to create a color array that we will use with required depth levels
    NSMutableArray *_colorsArray = [[NSMutableArray alloc] initWithCapacity:_depth];
    
    CGFloat *components =  (CGFloat *)CGColorGetComponents(_foregroundColor.CGColor);
    
    //add as a first color in our array the original color
    [_colorsArray insertObject:_foregroundColor atIndex:0];
    
    //create a gradient of our color (darkening in the depth)
    int _colorStepSize = floor(100/_depth);
    
    for (int i=0; i<_depth; i++) {
        
        for (int k=0; k<3; k++) {
            if (components[k]>(_colorStepSize/255.f)) {
                components[k]-=(_colorStepSize/255.f);
            }
        }
        _newColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:CGColorGetAlpha(_foregroundColor.CGColor)];
        
        //we are inserting always at first index as we want this array of colors to be reversed (darkest color being the last)
        [_colorsArray insertObject:_newColor atIndex:0];
    }
    
    //we will draw repeated copies of our text, with the outline color and foreground color, starting from the deepest
    for (int i=0; i<_depth; i++)
    {
        
        //change color
        _newColor = (UIColor*)[_colorsArray objectAtIndex:i];
        
        //draw the text
        CGContextSaveGState(context);
        
        CGContextSetShouldAntialias(context, YES);
        
        //draw outline if this is the last layer (front one)
        if (i+1==_depth)
        {
            CGContextSetLineWidth(context, 1);
            CGContextSetLineJoin(context, kCGLineJoinRound);
            
            CGContextSetTextDrawingMode(context, kCGTextStroke);
            [_outlineColor set];
            [_text drawAtPoint:CGPointMake(i, i) withFont:_font];
        }
        
        //draw filling
        [_newColor set];
        
        CGContextSetTextDrawingMode(context, kCGTextFill);
        
        //if this is the last layer (first one we draw), add the drop shadow too and the outline
        if (i==0) {
            CGContextSetShadowWithColor(context, CGSizeMake(-2, -2), 4.0f, _shadowColor.CGColor);
        }
        else if (i+1!=_depth){
            //add glow like blur
            CGContextSetShadowWithColor(context, CGSizeMake(-1, -1), 3.0f, _newColor.CGColor);
        }
        
        [_text drawAtPoint:CGPointMake(i, i) withFont:_font];
        CGContextRestoreGState(context);
    }
    
    //if we need to apply the shine
    if (_shine) {
        //create an alpha mask from the top most layer of the image, so we can add a shine effect over it
        CGColorSpaceRef genericRGBColorspace = CGColorSpaceCreateDeviceRGB();
        CGContextRef imageContext = CGBitmapContextCreate(NULL, (int)expectedSize.width, (int)expectedSize.height, 8, (int)expectedSize.width * 4, genericRGBColorspace,  kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
        UIGraphicsPushContext(imageContext);
        CGContextSetTextDrawingMode(imageContext, kCGTextFill);
        [_text drawAtPoint:CGPointMake(_depth-1, _depth-1) withFont:_font];
        CGImageRef alphaMask = CGBitmapContextCreateImage(imageContext);
        CGContextRelease(imageContext);
        UIGraphicsPopContext();
        
        //draw shine effect
        //clip context to the mask we created
        CGRect drawRect = CGRectZero;
        drawRect.size = expectedSize;
        CGContextSaveGState(context);
        CGContextClipToMask(context, drawRect, alphaMask);
        
        CGContextSetBlendMode(context, kCGBlendModeLuminosity);
        
        size_t num_locations = 4;
        CGFloat locations[4] = { 0.0, 0.4, 0.6, 1};
        CGFloat gradientComponents[16] = {
            0.0, 0.0, 0.0, 1.0,
            0.6, 0.6, 0.6, 1.0,
            0.8, 0.8, 0.8, 1.0,
            0.0, 0.0, 0.0, 1.0
        };
        
        CGGradientRef glossGradient = CGGradientCreateWithColorComponents(genericRGBColorspace, gradientComponents, locations, num_locations);
        CGPoint start = CGPointMake(0, 0);
        CGPoint end = CGPointMake(0, expectedSize.height);
        CGContextDrawLinearGradient(context, glossGradient, start, end, 0);
        
        CGColorSpaceRelease(genericRGBColorspace);
        CGGradientRelease(glossGradient);
        CGImageRelease(alphaMask);
    }
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return finalImage;
}



#pragma mark 3D Label Text Method
- (UIImage *)create3DLabelWithText:(NSString *)_text Font:(UIFont*)_font ForegroundColor:(UIColor*)_foregroundColor ShadowColor:(UIColor*)_shadowColor outlineColor:(UIColor*)_outlineColor depth:(int)_depth useShine:(BOOL)_shine
{
    //calculate the size we will need for our text
    CGSize expectedSize = [_text sizeWithFont:_font constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
    
    
    //  CGSize expectedSize = CGSizeMake(70.0, 070.0);
    //increase our size, as we will draw in 3d, so we need extra space for 3d depth + shadow with blur
    expectedSize.height+=_depth+5;
    expectedSize.width+=_depth+5;
    
    UIColor *_newColor;
    
    UIGraphicsBeginImageContextWithOptions(expectedSize, NO, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //because we want to do a 3d depth effect, we are going to slightly decrease the color as we move back
    //so here we are going to create a color array that we will use with required depth levels
    NSMutableArray *_colorsArray = [[NSMutableArray alloc] initWithCapacity:_depth];
    
    CGFloat *components =  (CGFloat *)CGColorGetComponents(_foregroundColor.CGColor);
    
    //add as a first color in our array the original color
    [_colorsArray insertObject:_foregroundColor atIndex:0];
    
    //create a gradient of our color (darkening in the depth)
    int _colorStepSize = floor(100/_depth);
    
    for (int i=0; i<_depth; i++) {
        
        for (int k=0; k<3; k++) {
            if (components[k]>(_colorStepSize/255.f)) {
                components[k]-=(_colorStepSize/255.f);
            }
        }
        _newColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:CGColorGetAlpha(_foregroundColor.CGColor)];
        
        //we are inserting always at first index as we want this array of colors to be reversed (darkest color being the last)
        [_colorsArray insertObject:_newColor atIndex:0];
    }
    
    //we will draw repeated copies of our text, with the outline color and foreground color, starting from the deepest
    for (int i=0; i<_depth; i++)
    {
        
        //change color
        _newColor = (UIColor*)[_colorsArray objectAtIndex:i];
        
        //draw the text
        CGContextSaveGState(context);
        
        CGContextSetShouldAntialias(context, YES);
        
        //draw outline if this is the last layer (front one)
        if (i+1==_depth)
        {
            CGContextSetLineWidth(context, 1);
            CGContextSetLineJoin(context, kCGLineJoinRound);
            
            CGContextSetTextDrawingMode(context, kCGTextStroke);
            [_outlineColor set];
            [_text drawAtPoint:CGPointMake(i, i) withFont:_font];
        }
        
        //draw filling
        [_newColor set];
        
        CGContextSetTextDrawingMode(context, kCGTextFill);
        
        //if this is the last layer (first one we draw), add the drop shadow too and the outline
        if (i==0) {
            CGContextSetShadowWithColor(context, CGSizeMake(-2, -2), 4.0f, _shadowColor.CGColor);
        }
        else if (i+1!=_depth){
            //add glow like blur
            CGContextSetShadowWithColor(context, CGSizeMake(-1, -1), 3.0f, _newColor.CGColor);
        }
        
        [_text drawAtPoint:CGPointMake(i, i) withFont:_font];
        CGContextRestoreGState(context);
    }
    
    //if we need to apply the shine
    if (_shine) {
        //create an alpha mask from the top most layer of the image, so we can add a shine effect over it
        CGColorSpaceRef genericRGBColorspace = CGColorSpaceCreateDeviceRGB();
        CGContextRef imageContext = CGBitmapContextCreate(NULL, (int)expectedSize.width, (int)expectedSize.height, 8, (int)expectedSize.width * 4, genericRGBColorspace,  kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
        UIGraphicsPushContext(imageContext);
        CGContextSetTextDrawingMode(imageContext, kCGTextFill);
        [_text drawAtPoint:CGPointMake(_depth-1, _depth-1) withFont:_font];
        CGImageRef alphaMask = CGBitmapContextCreateImage(imageContext);
        CGContextRelease(imageContext);
        UIGraphicsPopContext();
        
        //draw shine effect
        //clip context to the mask we created
        CGRect drawRect = CGRectZero;
        drawRect.size = expectedSize;
        CGContextSaveGState(context);
        CGContextClipToMask(context, drawRect, alphaMask);
        
        CGContextSetBlendMode(context, kCGBlendModeLuminosity);
        
        size_t num_locations = 4;
        CGFloat locations[4] = { 0.0, 0.4, 0.6, 1};
        CGFloat gradientComponents[16] = {
            0.0, 0.0, 0.0, 1.0,
            0.6, 0.6, 0.6, 1.0,
            0.8, 0.8, 0.8, 1.0,
            0.0, 0.0, 0.0, 1.0
        };
        
        CGGradientRef glossGradient = CGGradientCreateWithColorComponents(genericRGBColorspace, gradientComponents, locations, num_locations);
        CGPoint start = CGPointMake(0, 0);
        CGPoint end = CGPointMake(0, expectedSize.height);
        CGContextDrawLinearGradient(context, glossGradient, start, end, 0);
        
        CGColorSpaceRelease(genericRGBColorspace);
        CGGradientRelease(glossGradient);
        CGImageRelease(alphaMask);
    }
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return finalImage;
}



-(UIImage *) resizeImage:(UIImage *)image
{
    UIImage *tempImage = nil;
    CGSize targetSize = CGSizeMake(100,80);
    UIGraphicsBeginImageContext(targetSize);
    CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
    thumbnailRect.origin = CGPointMake(0.0,0.0);
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    
    [image drawInRect:thumbnailRect];
    
    tempImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return tempImage;
}

#pragma mark Pan Gesture Delegate Method
-(void)moveViewWithGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer{
    
    if (alpha == 0)
    {
        alpha = effectView.alpha;
    }
    CGPoint currentlocation = [panGestureRecognizer locationInView:_CollectionView];
    
    
    BOOL isPointInsideView = [_CollectionView pointInside:currentlocation withEvent:nil];
    
    //NSLog(@"Point in collection: %@", isPointInsideView?@"Yes":@"No");
    
    if (isPointInsideView)
    {
        CGPoint originInSuperview = [self.view convertPoint:currentlocation fromView:_CollectionView];
        NSLog(@"Point y: %f",originInSuperview.y);
        if (diffrence == 0)
        {
            diffrence = originInSuperview.y-_CollectionView.frame.origin.y;
        }
        CGPoint vel = [panGestureRecognizer velocityInView:self.view];
        if (vel.y > 0)
        {
            NSLog(@"Scroll Down in Pan");
            // Collection view scroll down
            if ((originInSuperview.y-diffrence) > initialFrame.origin.y)
            {
            
            _CollectionView.frame = CGRectMake(_CollectionView.frame.origin.x,originInSuperview.y-diffrence, _CollectionView.frame.size.width, _CollectionView.frame.size.height);
                if (alpha > 0.1)
                {
                    effectView.alpha = alpha;
                    alpha = alpha-0.010;
                }
            isStateEnd = NO;
            }
        }
        else
        {
        if (!isStateEnd) {
            // Collection view scroll up
            NSLog(@"Scroll UP in Pan with collection");
            if (_CollectionView.frame.origin.y>=initialFrame.origin.y)
            {
                    _CollectionView.frame = CGRectMake(_CollectionView.frame.origin.x, originInSuperview.y-diffrence, _CollectionView.frame.size.width, _CollectionView.frame.size.height);
                    if (alpha < maxAlpha)
                    {
                        effectView.alpha = alpha;
                        alpha = alpha+0.010;
                    }
                isStateEnd = NO;
            }
                else if(_CollectionView.frame.origin.x == initialFrame.origin.x)
                {
                    // if collection view back to original position
                    _CollectionView.userInteractionEnabled = YES;
                    effectView.alpha = maxAlpha;
                }
            }
            else
            {
                NSLog(@"Scroll UP in Pan");
                if (!_CollectionView.isUserInteractionEnabled)
                {
                    // scroll collection view programatically
                    _CollectionView.userInteractionEnabled = YES;
                    NSArray *visibleItems = [_CollectionView indexPathsForVisibleItems];
                    if(visibleItems.count > 0){
                        NSIndexPath *currentItem = [visibleItems objectAtIndex:0];
//                        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item +(visibleItems.count/2) inSection:currentItem.section];
//                        [_CollectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
                        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
                        [_CollectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
                    }
                    
                }
            }
        }
    }
    if(panGestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        alpha = 0;
        diffrence = 0;
        isStateEnd = YES;
        NSLog(@"End pan");
        if (_CollectionView.frame.origin.y > viewBound) {
            NSLog(@"Go Down");
            _CollectionView.frame = CGRectMake(0,  self.view.frame.size.height+10,  _CollectionView.frame.size.width,  _CollectionView.frame.size.height);
            [self backToPreviousViewController:nil];
            
        }
        else
        {
            NSLog(@"come Back");
            _CollectionView.frame = CGRectMake(initialFrame.origin.x, initialFrame.origin.y, _CollectionView.frame.size.width, _CollectionView.frame.size.height);
            effectView.alpha = maxAlpha;
        }
    }
}

-(void)tapMethodForCollectionView:(UITapGestureRecognizer *)GestureRecognizer
{
    NSLog(@"Tapped on collection view");
    CGPoint location = [GestureRecognizer locationInView:_CollectionView];
    NSIndexPath *IndexPath = [_CollectionView indexPathForItemAtPoint:location];
    NSLog(@"%ld",(long)IndexPath.row);
    [self selectedCell:IndexPath];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"%f",scrollView.contentOffset.y);
//    if (scrollView.contentOffset.y == 0)
//    {
//        NSLog(@"At the top");
//        _CollectionView.userInteractionEnabled = NO;
//    }
    self.lastContentOffset = scrollView.contentOffset.y;
    
    if (self.lastContentOffset < -100){
        [self backToPreviousViewController:nil];
    }
}


#pragma mark Cancel tap in a view
- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (CGRectContainsPoint(self.CollectionView.frame, [touch locationInView:self.view]))
        return YES;
    
    return NO;
}

-(void) selectedCell:(NSIndexPath *)indexPath
{
    NSString *str;
        if (_TextFieldString.text.length == 0)
        {
            str = @"ABC";
        }
        else
            str = _TextFieldString.text;
        selectedIndex = (int)indexPath.row;
        [self createText_String:str Index:(int)indexPath.row];;
}

#pragma mark New Label
-(UILabel *) newLabel_color:(UIColor *)color shadowColor:(UIColor *)shadow FontStyle:(NSString *)style
{
    NSString *text = @"ABC";
    NSString *size1 = @"26";
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    //gettingSizeLabel.font = [UIFont fontWithName:@"YOUR FONT's NAME" size:16];
    gettingSizeLabel.text = text;
    gettingSizeLabel.numberOfLines = 1;
    gettingSizeLabel.textColor = color;
    [gettingSizeLabel setFont:[UIFont fontWithName:style size:[size1 floatValue]]];
    gettingSizeLabel.frame = CGRectMake(0, 0, 320, 200);
    [gettingSizeLabel sizeToFit];
    gettingSizeLabel.shadowColor = shadow;
    gettingSizeLabel.shadowOffset = CGSizeMake(0,1);
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(600, CGFLOAT_MAX);
    
    CGSize expectSize = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    CGRect aRectangle = [text
                         boundingRectWithSize:CGSizeMake(600, 100)
                         options:NSStringDrawingUsesLineFragmentOrigin
                         attributes:@{
                                      NSFontAttributeName : [UIFont systemFontOfSize:[size1 floatValue]]
                                      }
                         context:nil];
    
    CGRect textRect = [text boundingRectWithSize:maximumLabelSize
                                         options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                      attributes:@{NSFontAttributeName:gettingSizeLabel.font}
                                         context:nil];
    
    
    
    //NSLog(@"aRectangle:\n%f %f",aRectangle.size.width,aRectangle.size.height);
    //gettingSizeLabel.frame=textRect;
    // gettingSizeLabel.frame=CGRectMake(image_View.frame.origin.x, image_View.frame.origin.y, textRect.size.width, textRect.size.height);
    
    return gettingSizeLabel;
    
}

#pragma mark Image From Image View
-(UIImage *) imagefromImageView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 4.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UIImageView *img = (UIImageView *)[self.view viewWithTag:255];
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint loc = [touch locationInView:img];
   // UIColor *color = [img colorOfPoint:loc];
    if (loc.x>-1) {
       // pictureTextView.textColor = color;
    }
}



@end
