//
//  Help.h
//  Cropped
//
//  Created by webastral on 04/10/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Help : UIViewController
@property (strong, nonatomic) IBOutlet UIView *bottomView;

@property (strong, nonatomic) IBOutlet UIButton *btn_Organise;
@property (strong, nonatomic) IBOutlet UIButton *btn_Create;
@property (strong, nonatomic) IBOutlet UIButton *btn_Suggestion;
@property (strong, nonatomic) IBOutlet UIButton *btn_Help;
@property (strong, nonatomic) IBOutlet UIView *topView;

@end
