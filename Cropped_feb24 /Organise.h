//
//  Organise.h
//  Cropped
//
//  Created by webastral on 04/10/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface Organise : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UIView *bottomView;
- (IBAction)Goto_Suggestion:(UIButton *)sender;
- (IBAction)goto_Help:(UIButton *)sender;
- (IBAction)goto_Create:(UIButton *)sender;
- (IBAction)goto_Organise:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UIView *darkView;

@property (strong, nonatomic) IBOutlet UIButton *btn_Organise;
@property (strong, nonatomic) IBOutlet UIButton *btn_Create;
@property (strong, nonatomic) IBOutlet UIButton *btn_Suggestion;
@property (strong, nonatomic) IBOutlet UIButton *btn_Help;
@end
