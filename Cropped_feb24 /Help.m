//
//  Help.m
//  Cropped
//
//  Created by webastral on 04/10/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#import "Help.h"
#import "HomeScreen.h"
#import "Suggestions.h"
#import "MZAppDelegate.h"
#import "Organise.h"
#import "Animator.h"

@interface Help ()

@end

@implementation Help

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    // configure send button and top view
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, self.topView.frame.size.height - 1.0f, self.topView.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithRed:0.71 green:0.72 blue:0.73 alpha:1.0].CGColor;
    
    [self.topView.layer addSublayer:bottomBorder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.delegate = self;
    _bottomView.userInteractionEnabled =     NO;
    // Do any additional setup after loading the view.
     //[self topViewButtonsAppearance];
}

#pragma mark Top View Buttons Setting
-(void) topViewButtonsAppearance
{
    int width = self.view.frame.size.width;
    NSLog(@"%f %f",_btn_Organise.frame.size.width,_btn_Organise.frame.size.height);
    int totalSpace = width-240;
    int spaceBetweenButton = totalSpace/5;
    _btn_Organise.frame = CGRectMake(spaceBetweenButton, _btn_Organise.frame.origin.y, _btn_Organise.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Create.frame = CGRectMake(spaceBetweenButton+_btn_Organise.frame.origin.x+_btn_Organise.frame.size.width, _btn_Organise.frame.origin.y, _btn_Create.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Suggestion.frame = CGRectMake(spaceBetweenButton+_btn_Create.frame.origin.x+_btn_Create.frame.size.width, _btn_Organise.frame.origin.y, _btn_Suggestion.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Help.frame = CGRectMake(spaceBetweenButton+_btn_Suggestion.frame.origin.x+_btn_Suggestion.frame.size.width, _btn_Organise.frame.origin.y, _btn_Help.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Organise.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Create.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Suggestion.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Help.translatesAutoresizingMaskIntoConstraints = YES;
    
    [_btn_Help.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [_btn_Organise.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_btn_Suggestion.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_btn_Create.titleLabel setFont:[UIFont systemFontOfSize:15]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Goto View Controllers

- (IBAction)Goto_Suggestion:(UIButton *)sender {
    
    // [MZAppDelegate GotoSuggestion];
    Suggestions *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Suggestions"];
    [self gotoSpecificController:object];;
    
}

- (IBAction)goto_Help:(UIButton *)sender {
//    Help *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
//    [self gotoSpecificController:object];;
}

- (IBAction)goto_Create:(UIButton *)sender {
    HomeScreen *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeScreen"];
    [self gotoSpecificController:object];;
}

- (IBAction)goto_Organise:(UIButton *)sender {
    Organise *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Organise"];
    [self gotoSpecificController:object];;
}

#pragma mark Goto Specific Controller
-(void) gotoSpecificController:(UIViewController *)controller
{
    BOOL isExist = NO;
    controller.transitioningDelegate = self;
    for (UIViewController *object in self.navigationController.viewControllers)
    {
        if ([object isKindOfClass:[controller class]])
        {
            NSLog(@"POP");
            isExist = YES;
            [self.navigationController popToViewController:object  animated:NO];
            
            break;
        }
        
    }
    if (!isExist) {
        NSLog(@"PUSH");
        [self.navigationController pushViewController:controller animated:NO];
    }
}
#pragma mark Navigation Controller Delegate Methods
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController*)fromVC
                                                 toViewController:(UIViewController*)toVC
{
    if (operation == UINavigationControllerOperationPush)
        return [[PushAnimator alloc] init];
    
    if (operation == UINavigationControllerOperationPop)
        return [[PopAnimator alloc] init];
    
    return nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
