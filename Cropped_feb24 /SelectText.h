//
//  SelectText.h
//  Cropped
//
//  Created by webastral on 19/11/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeScreen.h"
#import "MZAppDelegate.h"
@interface SelectText : UIViewController
- (IBAction)doneTextSelection:(UIButton *)sender;
- (IBAction)backToPreviousViewController:(UIButton *)sender;
-(void) setupInitialView;
@property BOOL isFromHomeScreen;
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *Layout;
@property (strong, nonatomic) IBOutlet UIImageView *ImageView;
@property (strong, nonatomic) IBOutlet UITextField *TextFieldString;
@property (strong, nonatomic) IBOutlet UICollectionView *CollectionView;
@property (strong, nonatomic) UIImage *backgroundImage;

@property (strong, nonatomic) MZAppDelegate *delegate1;
- (UIImage *)create3DImageWithText:(NSString *)_text Font:(UIFont*)_font ForegroundColor:(UIColor*)_foregroundColor ShadowColor:(UIColor*)_shadowColor outlineColor:(UIColor*)_outlineColor depth:(int)_depth useShine:(BOOL)_shine;
@property (strong, nonatomic) IBOutlet UIView *darkView;
@property (nonatomic) CGFloat lastContentOffset;
@property (nonatomic) CGFloat lastValueOfY;

@end
