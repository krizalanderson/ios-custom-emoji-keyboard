//
//  Suggestions.h
//  Cropped
//
//  Created by webastral on 03/10/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const kUser_Mail;

@interface Suggestions : UIViewController
- (IBAction)Goto_Suggestion:(UIButton *)sender;
- (IBAction)goto_Help:(UIButton *)sender;
- (IBAction)goto_Create:(UIButton *)sender;
- (IBAction)goto_Organise:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UITextView *txtView_Comments;
@property (strong, nonatomic) IBOutlet UITextField *txtField_email;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Captcha;
@property (strong, nonatomic) IBOutlet UITextField *txtField_Captcha;
- (IBAction)Submit:(UIButton *)sender;
- (IBAction)reload_Captcha:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *bottomView;

@property (strong, nonatomic) IBOutlet UIButton *btn_Organise;
@property (strong, nonatomic) IBOutlet UIButton *btn_Create;
@property (strong, nonatomic) IBOutlet UIButton *btn_Suggestion;
@property (strong, nonatomic) IBOutlet UIButton *btn_Help;

@property (strong, nonatomic) IBOutlet UIButton *sendBtn;
@property (strong, nonatomic) IBOutlet UIView *topView;

@end
