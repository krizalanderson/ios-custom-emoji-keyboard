//
//  Organise.m
//  Cropped
//
//  Created by webastral on 04/10/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#import "Organise.h"
#import "Suggestions.h"
#import "MZAppDelegate.h"
#import "Help.h"
#import "HomeScreen.h"
#import "Animator.h"

@interface Organise ()<UINavigationControllerDelegate>
{
    NSString *documentsDirectory;
    NSMutableArray *imagesData;
    NSMutableArray *imagesNames;
    UICollectionView *_collectionView;
    int selectedImageIndex;
}
@end

@implementation Organise

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    // configure send button and top view
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, self.topView.frame.size.height - 1.0f, self.topView.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithRed:0.71 green:0.72 blue:0.73 alpha:1.0].CGColor;
    
    [self.topView.layer addSublayer:bottomBorder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.delegate= self;
    self.bottomView.userInteractionEnabled = NO;
    
    
//    [self getData];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(buttonTapped:)];
    [_img addGestureRecognizer:tap];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(buttonTapped:)];
    [_darkView addGestureRecognizer:tap2];
    
    _img.userInteractionEnabled = YES;
    _img.hidden = YES;
    [_img setContentMode:UIViewContentModeScaleAspectFit];
    //[imagesData removeAllObjects];
    [self.view addSubview:_darkView];
    [self.view addSubview:_img];
    
    // Do any additional setup after loading the view.
    UISwipeGestureRecognizer *swipeleft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(leftSwipe)];
    swipeleft.direction = UISwipeGestureRecognizerDirectionLeft;
    [_img addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(rightSwipe)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [_img addGestureRecognizer:swipeRight];
    
    [self collectionView];
    
    
    UILongPressGestureRecognizer *lpgr
    = [[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [_collectionView addGestureRecognizer:lpgr];
    
    
    // [self topViewButtonsAppearance];
}

-(void) viewWillAppear:(BOOL)animated
{
    [self getData];
    if (imagesData.count>0) {
        _img.image = [UIImage imageWithData:[imagesData objectAtIndex:0]];
        [_collectionView reloadData];
    }
    else
     {
        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"No Images Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
    }
}

#pragma mark Top View Buttons Setting
-(void) topViewButtonsAppearance
{
    int width = self.view.frame.size.width;
    NSLog(@"%f %f",_btn_Organise.frame.size.width,_btn_Organise.frame.size.height);
    int totalSpace = width-240;
    int spaceBetweenButton = totalSpace/5;
    _btn_Organise.frame = CGRectMake(spaceBetweenButton, _btn_Organise.frame.origin.y, _btn_Organise.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Create.frame = CGRectMake(spaceBetweenButton+_btn_Organise.frame.origin.x+_btn_Organise.frame.size.width, _btn_Organise.frame.origin.y, _btn_Create.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Suggestion.frame = CGRectMake(spaceBetweenButton+_btn_Create.frame.origin.x+_btn_Create.frame.size.width, _btn_Organise.frame.origin.y, _btn_Suggestion.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Help.frame = CGRectMake(spaceBetweenButton+_btn_Suggestion.frame.origin.x+_btn_Suggestion.frame.size.width, _btn_Organise.frame.origin.y, _btn_Help.frame.size.width, _btn_Organise.frame.size.height);
    _btn_Organise.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Create.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Suggestion.translatesAutoresizingMaskIntoConstraints = YES;
    _btn_Help.translatesAutoresizingMaskIntoConstraints = YES;
    
    [_btn_Organise.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [_btn_Help.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_btn_Suggestion.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_btn_Create.titleLabel setFont:[UIFont systemFontOfSize:15]];
}
#pragma mark Gestures Methods
-(void)leftSwipe
{
    if(selectedImageIndex<imagesData.count-1) {
        selectedImageIndex++;
    }
    _img.image = [UIImage imageWithData:imagesData[selectedImageIndex]];
}
-(void)rightSwipe {
    if(selectedImageIndex>0) {
        selectedImageIndex--;
    }
    _img.image = [UIImage imageWithData:imagesData[selectedImageIndex]];
}


-(void) buttonTapped:(UIGestureRecognizer *)recogniser
{
    [MZAppDelegate popZoomOut:_darkView];
    [MZAppDelegate popZoomOut:_img];
//     _darkView.hidden = YES;
   // _img.hidden = YES;
}
-(void) getData
{
    
//    NSURL *groupContainerURL = [[NSFileManager defaultManager]containerURLForSecurityApplicationGroupIdentifier:@"group.com.summerinc.MeBoard"];
//    NSString *sharedDirectory = [groupContainerURL path];
//    NSData *imageData = UIImagePNGRepresentation(image);
//    NSString *filePath = [sharedDirectory stringByAppendingPathComponent:@"image.png"];
//    [imageData writeToFile:filePath atomically:YES];
    
    
    NSURL *groupContainerURL = [[NSFileManager defaultManager]containerURLForSecurityApplicationGroupIdentifier:@"group.com.summerinc.MeBoard"];
    documentsDirectory = [groupContainerURL path];
    
    imagesData = [[NSMutableArray alloc]init];
    imagesNames = [[NSMutableArray alloc]init];
    
//    NSArray *documentDirectoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    documentsDirectory = [documentDirectoryPath objectAtIndex:0];
    NSLog(@" document directory path :%@", documentsDirectory);
    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    NSLog(@"files path %@", filePathsArray);
    
    for (NSString *string in filePathsArray) {
        
        if ([string hasSuffix:@".png"])
        {
            NSString *path = [documentsDirectory stringByAppendingPathComponent:string];
            NSData *imageData = [NSData dataWithContentsOfFile:path];
            [imagesData addObject:imageData];
            [imagesNames addObject:string];
        }
        
        
    }
    
 
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Goto View Controllers

- (IBAction)Goto_Suggestion:(UIButton *)sender {
    
    // [MZAppDelegate GotoSuggestion];
    Suggestions *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Suggestions"];
    [self gotoSpecificController:object];;
    
}

- (IBAction)goto_Help:(UIButton *)sender {
//    [self showAlert:@"View is in under progress and will be available soon"];
    Help *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Help"];
    [self gotoSpecificController:object];
}

- (IBAction)goto_Create:(UIButton *)sender {
    HomeScreen *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeScreen"];
    [self gotoSpecificController:object];;
}

- (IBAction)goto_Organise:(UIButton *)sender {
//    Organise *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Organise"];
//    [self.navigationController pushViewController:object animated:NO];
}

#pragma mark Goto Specific Controller
-(void) gotoSpecificController:(UIViewController *)controller
{
    BOOL isExist = NO;
    for (UIViewController *object in self.navigationController.viewControllers)
    {
        if ([object isKindOfClass:[controller class]])
        {
            NSLog(@"POP");
            //Do not forget to import AnOldViewController.h
            isExist = YES;
            [self.navigationController popToViewController:object  animated:NO];
            break;
        }
        
    }
    if (!isExist) {
        NSLog(@"PUSH");
        [self.navigationController pushViewController:controller animated:NO];
    }
}
#pragma mark Navigation Controller Delegate Methods
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController*)fromVC
                                                 toViewController:(UIViewController*)toVC
{
    if (operation == UINavigationControllerOperationPush)
        return [[PushAnimator alloc] init];
    
    if (operation == UINavigationControllerOperationPop)
        return [[PopAnimator alloc] init];
    
    return nil;
}

#pragma mark Alert View
-(void) showAlert:(NSString *)message
{
    [[[UIAlertView alloc]initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil]show];
    //[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hideAlertView) userInfo:nil repeats:NO];
}

#pragma mark Add Collection View
-(void) collectionView
{
    for (UICollectionView *collection in self.view.subviews) {
        if ([collection isKindOfClass:[UICollectionView class]]) {
            [collection removeFromSuperview];
        }
    }
    CGRect frame = CGRectMake(0, _topView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-(_topView.frame.size.height+_bottomView.frame.size.height));
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    _collectionView=[[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor whiteColor]];
    [self.view insertSubview:_collectionView belowSubview:_darkView];
}


-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:_collectionView];
    
    NSIndexPath *indexPath = [_collectionView indexPathForItemAtPoint:p];
    if (indexPath == nil){
        NSLog(@"couldn't find index path");
    } else {
        // get the cell at indexPath (the one you long pressed)
        UICollectionViewCell* cell =
        [_collectionView cellForItemAtIndexPath:indexPath];
        // do stuff with the cell
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Deleting this sticker will delete it from the app and the keyboard." message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Delete Sticker" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            
            [self removeImage:indexPath.row];
            [self getData];
            [_collectionView reloadData];
            [alert dismissViewControllerAnimated:NO completion:Nil];
        }];
        
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Cancel Delete" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alert dismissViewControllerAnimated:NO completion:Nil];
        }];
        
        
        [alert addAction:action1];
        [alert addAction:action2];
        [self presentViewController:alert animated:YES completion:Nil];
    }
}

- (void)removeImage:(NSInteger )fileNumber
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

    NSURL *groupContainerURL = [fileManager containerURLForSecurityApplicationGroupIdentifier:@"group.com.summerinc.MeBoard"];
    NSString *documentsPath = [groupContainerURL path];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:[imagesNames objectAtIndex:fileNumber]];
    NSError *error;
    
    
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
//        UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//        [removedSuccessFullyAlert show];
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
    
    
}


#pragma mark Collection View Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return imagesData.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIImageView *img in cell.contentView.subviews)
    {
        if ([img isKindOfClass:[UIImageView class]])
        {
            [img removeFromSuperview];
        }
    }
//
//    if (collectionView.tag == 100) {
//        
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:cell.bounds];
        imgView.image = [UIImage imageWithData:[imagesData objectAtIndex:indexPath.row]];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
    selectedImageIndex = (int)indexPath.row;
//        label.text = @"AB";
//        label.textColor = [UIColor whiteColor];
//        [label setFont:[UIFont fontWithName:[fontStyle objectAtIndex:indexPath.row] size:20]];
//        label.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:imgView];
//        cell.backgroundColor = [UIColor blackColor];
//    }
//    else
//    {
//        UILabel *label = [[UILabel alloc]initWithFrame:cell.bounds];
//        label.text = [size objectAtIndex:indexPath.row];
//        label.textColor = [UIColor whiteColor];
//        label.textAlignment = NSTextAlignmentCenter;
//        [cell.contentView addSubview:label];
//        cell.backgroundColor = [UIColor blackColor];
//        
//    }
//    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIImage *image = [UIImage imageWithData:[imagesData objectAtIndex:indexPath.row]];
    _img.image = image;
    _img.hidden = NO;
    _darkView.hidden = NO;
    [_img setContentMode:UIViewContentModeScaleAspectFit];
    
    [MZAppDelegate popUpZoomIn:_darkView];
    [MZAppDelegate popUpZoomIn:_img];
    selectedImageIndex = indexPath.row;
    //_img.hidden = NO;
   
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
