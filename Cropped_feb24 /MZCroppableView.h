//
//  MZCroppableView.h
//  MZCroppableView
//
//  Created by macbook on 30/10/2013.
//  Copyright (c) 2013 macbook. All rights reserved.
//

@class MZCroppableView;
@protocol HideViewDelegate <NSObject>
@required
-(void) hideView;
@end
#import <UIKit/UIKit.h>
#import "Line.h"


@interface MZCroppableView : UIView
{
  CGPoint pts[5];
    uint ctr;
    CGPoint start;
    NSMutableArray * xAxis;
    NSMutableArray * yAxis;
    
    //created to crop image when line connect to at any point
    NSMutableArray *pointArr;
    NSMutableArray *pointArr2;
    
    //Line *line;
    
    int i;
    
    NSMutableArray *pathArray;
    UIBezierPath *myPath;
    NSMutableArray *lines;
    Line *line;
    
}
@property(nonatomic, strong) UIBezierPath *croppingPath;
@property(nonatomic, strong) UIColor *lineColor;
@property(nonatomic, assign) float lineWidth;
@property(nonatomic, strong) id<HideViewDelegate>delegate;
@property(nonatomic,assign) CGPoint intersactPoint;

- (id)initWithImageView:(UIImageView *)imageView;

+ (CGPoint)convertPoint:(CGPoint)point1 fromRect1:(CGSize)rect1 toRect2:(CGSize)rect2;
+ (CGRect)scaleRespectAspectFromRect1:(CGRect)rect1 toRect2:(CGRect)rect2;

- (UIImage *)deleteBackgroundOfImage:(UIImageView *)image;
- (UIImage *) addBorder:(UIImage *)image;
-(UIImage *) drawBorder;

@end
