//
//  HomeScreen.h
//  Cropped
//
//  Created by webastral on 03/10/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MZAppDelegate.h"
#import <QuartzCore/QuartzCore.h>


@interface HomeScreen : UIViewController

-(void)SetupChoosenImage:(UIImage *)image;

@property (strong, nonatomic) IBOutlet UIButton *btn_Gallery
;
@property (strong, nonatomic) IBOutlet UIButton *btn_Camera
;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
- (IBAction)openCamera:(UIButton *)sender;
- (IBAction)open_Gallery:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIImageView *image_View;


@property (nonatomic) BOOL isImageSelectcted;
@property (nonatomic) BOOL isFromPreview;
@property (strong, nonatomic) IBOutlet UIButton *btn_Crop;
- (IBAction)crop_Image:(UIButton *)sender;
- (IBAction)Goto_Suggestion:(UIButton *)sender;
- (IBAction)goto_Help:(UIButton *)sender;
- (IBAction)goto_Create:(UIButton *)sender;
- (IBAction)goto_Organise:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imagePreview;
@property (strong, nonatomic) IBOutlet UIButton *btn_saveImage;
- (IBAction)saveImageIntoDocumentDirectory:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_Cancel;
- (IBAction)resetView:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIButton *btn_editText;
- (IBAction)edit_Text:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *darkView;
- (IBAction)add_Border:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_Border;

- (void) RetunToSpecificViewController:(UIViewController *)Controller;
#pragma mark Input for Text
@property (strong, nonatomic) IBOutlet UIView *view_Prompt;
@property (strong, nonatomic) IBOutlet UITextField *txtField_text;
- (IBAction)select_Color:(UIButton *)sender;
-(UIImage *) imageWithView:(UIView *)view;
- (IBAction)select_Size:(UIButton *)sender;
- (IBAction)done_Prompt:(UIButton *)sender;
-(UIImageView *)addImageOnImage:(UIImage *)image String:(NSString *)string;

@property (strong, nonatomic) IBOutlet UIButton *btn_Size;
@property (strong, nonatomic) IBOutlet UIButton *btn_Color;
@property (strong, nonatomic) IBOutlet UIView *baseView;
@property (strong, nonatomic) IBOutlet UIView *top_View;

// Font Style
@property (strong, nonatomic) IBOutlet UIButton *btn_FontStyle;
- (IBAction)select_Font:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_Organise;
@property (strong, nonatomic) IBOutlet UIButton *btn_Create;
@property (strong, nonatomic) IBOutlet UIButton *btn_Suggestion;
@property (strong, nonatomic) IBOutlet UIButton *btn_Help;
@property (nonatomic) CGAffineTransform transform;



@end
