//
//  Animator.h
//  Cropped
//
//  Created by webastral on 04/10/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animator : NSObject

@end



@interface PushAnimator : NSObject <UIViewControllerAnimatedTransitioning>
@end

@interface PopAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@end



