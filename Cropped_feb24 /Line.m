//
//  Line.m
//  bezierpathDemo
//
//  Created by Hupp Technologies on 12/04/17.
//  Copyright © 2017 Hupp Technologies. All rights reserved.
//

#import "Line.h"

@implementation Line

@synthesize pointA;
@synthesize pointB;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
