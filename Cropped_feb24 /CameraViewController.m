//
//  CameraViewController.m
//  CameraWithAVFoundation
//
//  Created by Gabriel Alvarado on 4/16/14.
//  Copyright (c) 2014 Gabriel Alvarado. All rights reserved.
//

#import "CameraViewController.h"
#import "CameraSessionView.h"
#import "TOCropViewController.h"


@interface CameraViewController () <CACameraSessionDelegate,TOCropViewControllerDelegate>

@property (nonatomic, strong) CameraSessionView *cameraView;
@property (strong, nonatomic) IBOutlet UIImageView *tmpImageView;

@end

@implementation CameraViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self SetupCamera];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self setNeedsStatusBarAppearanceUpdate];
}
-(void)SetupCamera{
    
    //Set white status bar
    
    
    //Instantiate the camera view & assign its frame
    _cameraView = [[CameraSessionView alloc] initWithFrame:self.view.frame];
    
    //Set the camera view's delegate and add it as a subview
    _cameraView.delegate = self;
    
    //Apply animation effect to present the camera view
    CATransition *applicationLoadViewIn =[CATransition animation];
    [applicationLoadViewIn setDuration:0.6];
    [applicationLoadViewIn setType:kCATransitionReveal];
    [applicationLoadViewIn setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [[_cameraView layer]addAnimation:applicationLoadViewIn forKey:kCATransitionReveal];
    
    [self.view addSubview:_cameraView];
    
    //____________________________Example Customization____________________________
    //[_cameraView setTopBarColor:[UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha: 0.64]];
    //[_cameraView hideFlashButton]; //On iPad flash is not present, hence it wont appear.
    //[_cameraView hideCameraToggleButton];
    //[_cameraView hideDismissButton];
    
}
-(void)didCaptureImage:(UIImage *)image {
    NSLog(@"CAPTURED IMAGE");
//    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
//    [self.cameraView removeFromSuperview];
     NSLog(@"%@",image);
    //CGFloat width = [[UIScreen mainScreen] bounds].size.width;
//    UIImage *myResizedImage = [[self class] imageWithImage:image
//                                            scaledToMaxWidth:width*2
//                                                   maxHeight:1024];
//    
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithImage:image];
    cropController.delegate = self;
    cropController.toolbar.cancelTextButtonTitle = @"Retake";
    cropController.toolbar.clampButtonHidden = YES;
    cropController.toolbar.rotateClockwiseButtonHidden = YES;
    cropController.toolbar.rotateCounterclockwiseButtonHidden = YES;
    [cropController setImageCropFrame:CGRectMake(0.0, 0.0, 1200, 1200)];
    [cropController setMinimumAspectRatio:1200];
    
    
    [self presentViewController:cropController animated:YES completion:nil];
}

-(void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle{
    NSLog(@"%@",image);
    if (self.delegate)
        [self.delegate didCroppedImage:image];
    
    [_cameraView removeFromSuperview];
    _cameraView = Nil;
//    [self.tmpImageView setHidden:NO];
//    self.tmpImageView.image = image;
//    [self.view bringSubviewToFront:self.tmpImageView];
    
    
    [cropViewController dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
    //[self dismissViewControllerAnimated:NO completion:nil];
}

//-(void)didCaptureImageWithData:(NSData *)imageData {
//    NSLog(@"CAPTURED IMAGE DATA");
//    //UIImage *image = [[UIImage alloc] initWithData:imageData];
//    //UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
//    //[self.cameraView removeFromSuperview];
//}

-(void)didDismissTapped{
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (BOOL)prefersStatusBarHidden {
   return YES;
    
}

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height {
    CGFloat oldWidth = image.size.width;
    CGFloat oldHeight = image.size.height;
    
    CGFloat scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
    
    CGFloat newHeight = oldHeight * scaleFactor;
    CGFloat newWidth = oldWidth * scaleFactor;
    CGSize newSize = CGSizeMake(newWidth, newHeight);
    
    return [[self class] imageWithImage:image scaledToSize:newSize];
}

@end
