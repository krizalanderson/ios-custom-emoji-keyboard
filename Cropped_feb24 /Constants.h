//
//  Constants.h
//  Cropped
//
//  Created by webastral on 13/10/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#ifndef Constants_h
#define Constants_h



# define NSAlertView(Title,Message) [[[UIAlertView alloc]initWithTitle:Title message:Message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil]show];

# define NSAlertView2(Title,Message) [[[UIAlertView alloc]initWithTitle:Title message:Message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil]show];

#endif /* Constants_h */
