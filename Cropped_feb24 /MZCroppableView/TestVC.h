//
//  TestVC.h
//  Cropped_Example
//
//  Created by Hupp Technologies on 02/02/18.
//  Copyright © 2018 Muhammad Zeeshan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestVC : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end
