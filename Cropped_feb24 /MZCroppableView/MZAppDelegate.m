//
//  MZAppDelegate.m
//  MZCroppableView
//
//  Created by Muhammad Zeeshan on 05/06/2016.
//  Copyright (c) 2016 Muhammad Zeeshan. All rights reserved.
//

#import "MZAppDelegate.h"
#import "Suggestions.h"
#import "HomeScreen.h"
#import "UIImage+ThreeD.h"


@interface MZAppDelegate()
{
   NSArray *colorsArray;
   NSArray * familyNames;
}

@end
@implementation MZAppDelegate
@synthesize textImages,textFormats,fonts;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [self addTextImages];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark Goto View Controllers
+(void) GotoSuggestion
{
    Suggestions *object = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Suggestions"];
    UINavigationController *nav = [[UINavigationController alloc]init];
    [nav pushViewController:object animated:YES];
}

+(UIImage *)changeWhiteColorTransparent: (UIImage *)image
{
    //convert to uncompressed jpg to remove any alpha channels
    //this is a necessary first step when processing images that already have transparency
    image = [UIImage imageWithData:UIImageJPEGRepresentation(image, 1.0)];
    CGImageRef rawImageRef=image.CGImage;
    //RGB color range to mask (make transparent)  R-Low, R-High, G-Low, G-High, B-Low, B-High
    const CGFloat colorMasking[6] = {222, 255, 222, 255, 222, 255};
    
    UIGraphicsBeginImageContext(image.size);
    CGImageRef maskedImageRef=CGImageCreateWithMaskingColors(rawImageRef, colorMasking);
    
    //iPhone translation
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, image.size.height);
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, image.size.width, image.size.height), maskedImageRef);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(maskedImageRef);
    UIGraphicsEndImageContext();
    return result;
}

#pragma mark Zoom pop in out animations
+ (void)popUpZoomIn:(UIView*)img
{
    // UIView *img = _view_Prompt;
    [UIView animateWithDuration:0.3
                     animations:^{
                         img.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                     } completion:^(BOOL finished) {
                         [UIImageView animateWithDuration:0.3/2 animations:^{
                             img.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                         } completion:^(BOOL finished) {
                             [UIImageView animateWithDuration:0.3/2 animations:^{
                                 img.transform = CGAffineTransformIdentity;
                             }];
                         }];
                     }];
}

+ (void)popZoomOut:(UIView *)img{
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         img.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
                     } completion:^(BOOL finished) {
                         img.hidden = TRUE;
                         
                     }];
}

#pragma mark ShowIndicator
+(void)showIndicator:(UIView *)view
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = @"Please Wait...";
    hud.animationType = MBProgressHUDAnimationZoom;
    hud.bezelView.backgroundColor = [UIColor blackColor];
    hud.contentColor = [UIColor whiteColor];
}

+(void) HideIndicator:(UIView *)view
{
    [MBProgressHUD hideHUDForView:view animated:YES];
}

#pragma mark Text Style Images Working In Background
-(void) addTextImages
{
    [self getSystemFonts];
    [self colorArray];
   // [self performSelectorInBackground:@selector(setupInitialView) withObject:nil];
   // [self setupInitialView];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // if(!delegate.textFormats)
        [self setupInitialView];
    });

}

#pragma mark Get System Fonts
-(void) getSystemFonts
{
//    familyNames = [NSArray arrayWithObjects:@"AgentOrange",@"BroadcastTitling",@"Montague",
//                   @"Wedgie Regular",@"CantateBeveled",@"FromCartoonBlocks",            @"NationalCartoon",@"orangejuice",@"PlanetBenson2",@"Alba",@"AlbaMatter",@"AlbaSuper",@"StoneyBilly", nil];
//
    /* removed font name : BroadcastTitling,
     Montague,Wedgie-Regular,FromCartoonBlocks,
     NationalCartoon,orangejuice,PlanetBenson2,
     Alba,AlbaMatter,AlbaSuper,TooneyNoodleNF,
     JandaManateeBubble,BROKENDEPTHdemo,Brutality,Chrysalis,ElectricPickle,JustadreamHollow,MuddyTractor-Bold,KGBlankSpaceSketch
     */
    
    
    familyNames = [NSArray arrayWithObjects:@"AgentOrange",@"StoneyBilly",
                   @"RainyDays",@"MediaBlackoutDEMO",@"JandaManateeSolid"
                   ,@"ComicalCartoon", @"BrutalityExtra",@"ChrysalisFilled",
                   @"ElectricPickleBold",@"JustadreamSolid",
                   @"MuddyTractor",@"KGBlankSpaceSolid",nil];

    
    [self printAllFontFamilyAndFonts];
    for (NSString *fontFamilyName in [UIFont familyNames]) {
        for (NSString *fontName in [UIFont fontNamesForFamilyName:fontFamilyName]) {
            NSLog(@"Family: %@    Font: %@", fontFamilyName, fontName);
        }
    }
    
    fonts = [[NSMutableArray alloc]init];
   
      for ( id familyName in familyNames)
    {
       @try
        {
            for(NSString *fontName in [UIFont fontNamesForFamilyName:familyName])
            {
                NSLog(@"Font Name = %@", fontName);
            }
            
           
           
            NSString *font = familyName;
            [fonts addObject:font];
//            if (fonts.count<13)
//            {
//                // NSLog(@"NAme of Font is %@",font);
//                [fonts addObject:font];
//            }
//            else
//            {
//                break;
//            }
        }
        @catch (NSException *exception)
            {
            // NSLog(@"Error");
        }
    }
    // NSLog(@"Fonts: %@",fonts);
}
//}
- (void)printAllFontFamilyAndFonts
{
    
  
    NSArray *fontNames;
    NSInteger indFamily, indFont;
    NSInteger fontsCount = familyNames.count;
    
    for (indFamily = 0; indFamily < fontsCount; ++indFamily) {
        NSLog (@"Family name: %@", [familyNames objectAtIndex:indFamily]);
        fontNames = [[NSArray alloc] initWithArray:
                     [UIFont fontNamesForFamilyName:
                      [familyNames objectAtIndex:indFamily]]];
        for (indFont = 0; indFont < [fontNames count]; ++indFont) {
            NSLog (@"    Font name: %@", [fontNames objectAtIndex:indFont]);
        }
        
    }
}
#pragma mark array from colors
-(void) colorArray
{
    NSArray *Red = [self arrayFromRGBAValues_Red:0 Green:0 Blue:0 Alpha:0.8];
    NSArray *Black = [self arrayFromRGBAValues_Red:0 Green:0 Blue:0 Alpha:0.8];
    NSArray *Green = [self arrayFromRGBAValues_Red:0 Green:0 Blue:0 Alpha:0.8];
    NSArray *Blue = [self arrayFromRGBAValues_Red:0 Green:0 Blue:0 Alpha:0.8];
    NSArray *Yellow = [self arrayFromRGBAValues_Red:0 Green:0 Blue:0 Alpha:0.8];
    NSArray *Orange = [self arrayFromRGBAValues_Red:0 Green:0 Blue:0 Alpha:0.8];
    NSArray *Brown = [self arrayFromRGBAValues_Red:0 Green:0 Blue:0 Alpha:0.8];
    NSArray *Gray = [self arrayFromRGBAValues_Red:0 Green:0 Blue:0 Alpha:0.8];
    NSArray *white = [self arrayFromRGBAValues_Red:0 Green:0 Blue:0 Alpha:0.8];
    colorsArray = @[Red,Black,Green,Blue,Yellow,Orange,Brown,Gray,white];
}

#pragma mark Array from RGB Values
-(NSArray *) arrayFromRGBAValues_Red:(float)red Green:(float)green Blue:(float)blue Alpha:(float)alpha
{
    NSArray *arr = @[[NSNumber numberWithFloat:red],[NSNumber numberWithFloat:green],[NSNumber numberWithFloat:blue],[NSNumber numberWithFloat:alpha]];
    return arr;
}

#pragma mark Color From Array
-(UIColor *) colorFromArray:(NSArray *)array
{
    return [UIColor colorWithRed:[array[0] floatValue]/255.f green:[array[1] floatValue]/255.f blue:[array[2] floatValue]/255.f alpha:[array[3] floatValue]];
}

#pragma mark Random Number Generator
-(NSInteger)randomIntBetween:(NSInteger)min and:(NSInteger)max
{
    return (NSInteger)(min + arc4random_uniform(max + 1 - min));
}

-(void) setupInitialView
{
        textFormats = [[NSMutableArray alloc]init];
      //  textImages = [[NSMutableArray alloc]init];
        for (int i=0; i<fonts.count; i++)
        {
            NSString *FontName = [fonts objectAtIndex:i];
            NSArray *ForeGroundColor = [colorsArray objectAtIndex:[self randomIntBetween:0 and:8]];
            NSArray *ShadowColor = [colorsArray objectAtIndex:[self randomIntBetween:0 and:8]];
            NSArray *OutlineColor = [colorsArray objectAtIndex:[self randomIntBetween:0 and:8]];
            int depth = (int)[self randomIntBetween:1 and:20];
            NSDictionary *dict = @{@"FontName":FontName, @"ForeGroundColor":ForeGroundColor,@"ShadowColor":ShadowColor,@"OutlineColor":OutlineColor,@"depth":[NSNumber numberWithInt:depth]};
            [textFormats addObject:dict];
     
        }
      
}
    
-(UIImage *) createImageTextFromData:(NSDictionary *)dict indexValue:(int)index isForCell:(BOOL)cell
{
        int depth  = [[dict valueForKey:@"depth"] intValue];
        int size = 0;
        BOOL shine;
    
        size = 180; // For Image View
                
        NSString *FontName = [dict valueForKey:@"FontName"];
        NSArray *ForeGroundColor = [dict valueForKey:@"ForeGroundColor"];
        NSArray *OutlineColor = [dict valueForKey:@"OutlineColor"];
        NSArray *ShadowColor = [dict valueForKey:@"ShadowColor"];
        
        UIImage *image =
        [UIImage create3DImageWithText:@"ABC" Font:[UIFont fontWithName:FontName size:size] ForegroundColor:[UIColor colorWithRed:[ForeGroundColor[0] floatValue]/255.f green:[ForeGroundColor[1] floatValue]/255.f blue:[ForeGroundColor[2] floatValue]/255.f alpha:[ForeGroundColor[3] floatValue]] ShadowColor:[self colorFromArray:ShadowColor] outlineColor:[self colorFromArray:OutlineColor] depth:depth useShine:shine];
        
        if (cell) {
            // Resize cell image
            return  [self resizeImage:image];
        }
    return image;
}

-(UIImage *) resizeImage:(UIImage *)image
{
    UIImage *tempImage = nil;
    CGSize targetSize = CGSizeMake(100,80);
    UIGraphicsBeginImageContext(targetSize);
    CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
    thumbnailRect.origin = CGPointMake(0.0,0.0);
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    
    [image drawInRect:thumbnailRect];
    
    tempImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return tempImage;
}









@end
