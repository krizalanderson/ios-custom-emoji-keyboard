//
//  TestVC.m
//  Cropped_Example
//
//  Created by Hupp Technologies on 02/02/18.
//  Copyright © 2018 Muhammad Zeeshan. All rights reserved.
//

#import "TestVC.h"

@interface TestVC ()
@property (strong, nonatomic) IBOutlet UIImageView *imgView;

@end

@implementation TestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)openBtn:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    });
    
}
# pragma mark Image Picker Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    //UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    //CGRect tmpRect = [[info valueForKey:UIImagePickerControllerCropRect] CGRectValue];
    
    //self.imgView.image = [self imageByScalingAndCroppingForSize:tmpRect.size img:image];
    
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
    CGSize size = image.size;
    
    // crops the crop rect that the user selected.
    CGRect cropRect = [info[@"UIImagePickerControllerCropRect"] CGRectValue];
    
    // creates a graphics context of the correct size.
    UIGraphicsBeginImageContext(cropRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // checks and corrects the image orientation.
    UIImageOrientation orientation = [image imageOrientation];
    if(orientation == UIImageOrientationUp) {
        CGContextTranslateCTM(context, 0, size.height);
        CGContextScaleCTM(context, 1, -1);
        
        cropRect = CGRectMake(cropRect.origin.x,
                              -cropRect.origin.y,
                              cropRect.size.width,
                              cropRect.size.height);
    }
    else if(orientation == UIImageOrientationRight) {
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextRotateCTM(context, -M_PI/2);
        size = CGSizeMake(size.height, size.width);
        
        cropRect = CGRectMake(cropRect.origin.y,
                              cropRect.origin.x,
                              cropRect.size.height,
                              cropRect.size.width);
    }
    else if(orientation == UIImageOrientationDown) {
        CGContextTranslateCTM(context, size.width, 0);
        CGContextScaleCTM(context, -1, 1);
        
        cropRect = CGRectMake(-cropRect.origin.x,
                              cropRect.origin.y,
                              cropRect.size.width,
                              cropRect.size.height);
    }
    
    // draws the image in the correct place.
    CGContextTranslateCTM(context, -cropRect.origin.x, -cropRect.origin.y);
    CGContextDrawImage(context,
                       CGRectMake(0,0, size.width, size.height),
                       image.CGImage);
    
    // and pull out the cropped image
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    
    self.imgView.image = croppedImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize img:(UIImage *)image
{
    CGFloat scaleFactor = 1.0;
    
    //Deciding which factor to use to scale the image (factor = targetSize / imageSize)
    if (image.size.width > targetSize.width || image.size.height > targetSize.height)
        if (!((scaleFactor = (targetSize.width / image.size.width)) > (targetSize.height / image.size.height))) //scale to fit width, or
            scaleFactor = targetSize.height / image.size.height; // scale to fit heigth.
    
    UIGraphicsBeginImageContext(targetSize);
    
    //Creating the rect where the scaled image is drawn in
    CGRect rect = CGRectMake((targetSize.width - image.size.width * scaleFactor) / 2,
                             (targetSize.height -  image.size.height * scaleFactor) / 2,
                             image.size.width * scaleFactor, image.size.height * scaleFactor);
    
    //Draw the image into the rect
    [image drawInRect:rect];
    
    //Saving the image, ending image context
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
    
//    UIImage *sourceImage = image;
//    UIImage *newImage = nil;
//    CGSize imageSize = sourceImage.size;
//    CGFloat width = imageSize.width;
//    CGFloat height = imageSize.height;
//    CGFloat targetWidth = targetSize.width;
//    CGFloat targetHeight = targetSize.height;
//    CGFloat scaleFactor = 0.0;
//    CGFloat scaledWidth = targetWidth;
//    CGFloat scaledHeight = targetHeight;
//    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
//
//    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
//    {
//        CGFloat widthFactor = targetWidth / width;
//        CGFloat heightFactor = targetHeight / height;
//
//        if (widthFactor > heightFactor)
//        {
//            scaleFactor = widthFactor; // scale to fit height
//        }
//        else
//        {
//            scaleFactor = heightFactor; // scale to fit width
//        }
//
//        scaledWidth  = width * scaleFactor;
//        scaledHeight = height * scaleFactor;
//
//        // center the image
//        if (widthFactor > heightFactor)
//        {
//            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
//        }
//        else
//        {
//            if (widthFactor < heightFactor)
//            {
//                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
//            }
//        }
//    }
//
//    UIGraphicsBeginImageContext(targetSize); // this will crop
//
//    CGRect thumbnailRect = CGRectZero;
//    thumbnailRect.origin = thumbnailPoint;
//    thumbnailRect.size.width  = scaledWidth;
//    thumbnailRect.size.height = scaledHeight;
//
//    [sourceImage drawInRect:thumbnailRect];
//
//    newImage = UIGraphicsGetImageFromCurrentImageContext();
//
//    if(newImage == nil)
//    {
//        NSLog(@"could not scale image");
//    }
//
//    //pop the context to get back to the default
//    UIGraphicsEndImageContext();
//
//    return newImage;
}

@end
