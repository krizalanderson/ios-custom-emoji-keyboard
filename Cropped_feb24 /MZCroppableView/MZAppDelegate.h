//
//  MZAppDelegate.h
//  MZCroppableView
//
//  Created by Muhammad Zeeshan on 05/06/2016.
//  Copyright (c) 2016 Muhammad Zeeshan. All rights reserved.
//

@import UIKit;
#import "MBProgressHUD.h"
@interface MZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray *textImages;
@property (strong, nonatomic) NSMutableArray *textFormats;
@property (strong, nonatomic) NSMutableArray *fonts;
@property (nonatomic,strong) NSMutableArray *Alphabets;

+(void) GotoSuggestion;
+(void) GotoHome;
+ (void)popUpZoomIn:(UIView*)img;
+ (void)popZoomOut:(UIView *)img;
+(void) HideIndicator:(UIView *)view;
+(void)showIndicator:(UIView *)view;
+(UIImage *)changeWhiteColorTransparent: (UIImage *)image;
-(NSInteger)randomIntBetween:(NSInteger)min and:(NSInteger)max;
-(UIColor *) colorFromArray:(NSArray *)array;
- (UIStatusBarStyle)preferredStatusBarStyle;


@end
