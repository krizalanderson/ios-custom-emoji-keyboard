//
//  UIView+ColorOfPoint.h
//  Cropped
//
//  Created by webastral on 26/11/16.
//  Copyright © 2016 Muhammad Zeeshan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ColorOfPoint)
- (UIColor *) colorOfPoint:(CGPoint)point;
@end
